import { Typography } from '@material-ui/core';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  container: {
    color: 'white',
  },
  headerTypo: {
    textAlign: 'center',
    fontFamily: 'rokkitt',
  },
  descriptionTypo: {
    textAlign: 'center',
  },
}));

export default function Custom404() {
  const classes = useStyles();

  return (
    <Grid container justifyContent="center" className={classes.container}>
      <Grid item xs={12}>
        <Typography variant="h1" className={classes.headerTypo}>
          Hey you are out of bound !
        </Typography>
      </Grid>
      <Grid item xs={12}>
        <Typography variant="h2" className={classes.descriptionTypo}>
          404 - Page Not Found
        </Typography>
      </Grid>
    </Grid>
  );
}
