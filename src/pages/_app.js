import { makeStyles } from '@material-ui/core/styles';
import AppBar from '../components/AppBar';
import { SSRPersistGate } from '../components/Molecules/SSRPersistGate/SSRPersistGate';
import { useRouter } from 'next/router';
import '../styles/globals.css';
import store from '../redux/store/store';
import { Provider } from 'react-redux';
import Grid from '@material-ui/core/Grid';
import { persistStore } from 'redux-persist';
import { GoogleReCaptchaProvider } from 'react-google-recaptcha-v3';
import Spinner from '../components/Molecules/Spinner/Spinner';
import ErrorBar from '../components/Molecules/Snackbar/ErrorBar';
import SuccessBar from '../components/Molecules/Snackbar/SuccessBar';
import LoginCheck from '../components/Atom/LoginCheck';
import axios from 'axios';

if (typeof window !== 'undefined') {
  axios.defaults.baseURL = process.env.NEXT_PUBLIC_BACKEND_SERVICE_URL;
  axios.defaults.headers.common['Authorization'] =
    localStorage.getItem('persist:root') &&
    'Bearer ' +
      JSON.parse(JSON.parse(localStorage.getItem('persist:root')).auth).token;
  axios.defaults.headers.post['Content-Type'] = 'application/json';
}

const useStyles = makeStyles({
  backgroundColor: {
    backgroundColor: '#383838',
    minHeight: '100vh',
    height: 'auto',
    maxWidth: '100vw',
    width: 'auto',
    margin: 0,
    padding: 0,
  },
});

function MyApp({ Component, pageProps }) {
  const classes = useStyles();
  const router = useRouter();

  let persistor = persistStore(store);

  return (
    <GoogleReCaptchaProvider
      reCaptchaKey={process.env.NEXT_PUBLIC_RECAPTCHA_SECRET_KEY}
      scriptProps={{
        async: false, // optional, default to false,
        defer: false, // optional, default to false
        appendTo: 'head', // optional, default to "head", can be "head" or "body",
        nonce: undefined, // optional, default undefined
      }}
    >
      <Provider store={store}>
        <LoginCheck />
        <SSRPersistGate persistor={persistor}>
          <Spinner />
          <Grid
            container
            direction="column"
            className={classes.backgroundColor}
          >
            {router.pathname !== '/' && router.pathname !== '/summary' ? (
              <AppBar />
            ) : (
              <></>
            )}
            <Component {...pageProps} />
          </Grid>
          <SuccessBar />
          <ErrorBar />
        </SSRPersistGate>
      </Provider>
    </GoogleReCaptchaProvider>
  );
}

export default MyApp;
