import React, { useState, useCallback } from 'react';
import { Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Link from '@material-ui/core/Link';
import Image from 'next/image';
import classNames from 'classnames/bind';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';
import NavigateBeforeIcon from '@material-ui/icons/NavigateBefore';
import IconButton from '@material-ui/core/IconButton';
import { useRouter } from 'next/router';
import { Formik, Form, useField } from 'formik';
import useBreakPoints from '../../helpers/breakpoints';
import * as Yup from 'yup';
import TextareaAutosize from '@material-ui/core/TextareaAutosize';
import InstagramIcon from '@material-ui/icons/Instagram';
import LinkedInIcon from '@material-ui/icons/LinkedIn';
import GitHubIcon from '@material-ui/icons/GitHub';
import Button from '@material-ui/core/Button';
import { RETRIEVE_BLOG, RETRIEVE_PROJECT } from '../../constants/action';
import { fetchData } from '../../hooks/swr/fetchData';
import DOMPurify from 'isomorphic-dompurify';
import { makePostRequest } from '../../redux/slice/loadingSlice';
import { RECAPTCHA_ERROR } from '../../constants/error';
import { SEND_EMAIL_TO_DEV } from '../../constants/action';
import { useGoogleReCaptcha } from 'react-google-recaptcha-v3';
import GooglePolicy from '../../components/Atom/GooglePolicy';
import { onError, onErrorFinish } from '../../redux/slice/errorSlice';
import { onSuccess, onSuccessFinish } from '../../redux/slice/successSlice';
import { useDispatch } from 'react-redux';
import Head from 'next/head';

const useStyles = makeStyles(() => ({
  navButton: {
    color: '#eef703',
  },
  nameTypo: {
    fontSize: '50px',
    fontFamily: 'Rokkitt',
    color: 'white',
  },
  jobTypo: {
    marginTop: '25px',
    fontSize: '25px',
    fontFamily: 'Rokkitt',
    color: '#21EB00',
    lineHeight: 0.8,
  },
  descriptionTypo: {
    fontSize: '18px',
    fontFamily: 'Rokkitt',
    color: 'white',
  },
  quickNav: {
    top: '40px',
    right: '0px',
    position: 'fixed',
    height: (props) => (props.quickNavCollapse ? '200px' : '55px'),
    width: (props) => (props.quickNavCollapse ? '120px' : '50px'),
    backgroundColor: '#21EB00',
    borderRadius: '10px 0px 0px 10px',
    cursor: (props) => (props.quickNavCollapse ? 'default' : 'pointer'),
    transition: '0.3s',
    zIndex: 101,
  },
  imageContainer: {
    cursor: 'pointer',
    borderRadius: '30px',
    boxShadow: '30px 30px 50px 0px rgba(0,0,0,0.50)',
  },
  defaultMargin: {
    margin: '20px 0px 20px 0px',
    borderRadius: '30px',
  },
  image: {
    borderRadius: '30px',
    animation: '$fadingHightlight 3s',
  },
  fadeAnimation: {
    animation: '$fadingHightlight 3s',
  },
  '@keyframes fadingHightlight': {
    from: {
      background: 'red',
      opacity: 0,
    },
    to: {
      opacity: 1,
    },
  },
  blogBackground: {
    position: 'absolute',
    height: '100%',
    width: '100%',
    top: 0,
    zIndex: 100,
    opacity: 1,
    borderRadius: '30px',
    color: 'white',
    fontSize: '50px',
    background:
      'linear-gradient(180deg, rgba(255, 255, 255, 0) 0%, rgba(0, 0, 0, 0.5) 100%);',

    '&:hover ': {
      '& $blogBackgroundHover': {
        bottom: 0,
        transition: '0.9s',
      },
      '& $blogTitle': {
        bottom: 'calc(100% - 60px);',
        transition: '1s',
      },
    },
    overflow: 'hidden',
  },
  blogTitle: {
    position: 'absolute',
    bottom: '10%',
    textAlign: 'center',
  },
  blogBackgroundHover: {
    position: 'absolute',
    height: '100%',
    width: '100%',
    bottom: '-100%',
    opacity: 1,
    borderRadius: '30px',
    color: 'white',
    fontSize: '50px',
    background:
      'linear-gradient(180deg, rgba(255, 255, 255, 0) 0%, rgba(0, 0, 0, 0.5) 0%);',
  },
  blogDescription: {
    textAlign: 'justify',
    color: 'white',
    fontSize: '20px',
    marginTop: '75px',
  },
  formTextField: {
    backgroundColor: '#C4C4C4',
    fontSize: '20px',
    padding: '10px',
    border: 'none',
  },
  formErrorText: {
    color: 'red',
    marginBottom: '15px',
  },
  formLabel: {
    fontFamily: 'Sorts Mill Goudy',
  },
  body: {
    marginTop: '10vh',
    color: 'white',
  },
  socmedTypo: {
    color: 'white',
    marginLeft: '5px',
  },
  formContainer: {
    width: '90%',
  },
  quickNavTypo: {
    padding: '5px',
    fontSize: '18px',
    fontFamily: 'Rokkitt',
    '&:hover': {
      color: 'white',
    },
  },
  quickNavItemTypo: {
    padding: '5px',
    cursor: 'pointer',
    color: '#383838',
    '&:hover': {
      textDecoration: 'none',
      color: 'white',
    },
  },
  quickNavItemContainer: {},
}));

const SlidingCard = ({
  header,
  cardData,
  handleBack,
  handleNext,
  cardType,
  image,
}) => {
  const router = useRouter();
  const classes = useStyles();

  return (
    <>
      <Head>
        <title>Elsen's Summary</title>
        <meta name="description" content="Summary of this website" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Typography className={classes.nameTypo}>{header.title}</Typography>
      <Typography className={classes.jobTypo}>{header.content}</Typography>
      <Grid item container alignItems="center" justifyContent="center">
        <Grid item xs={1} justifyContent="center" container>
          <Grid item>
            <IconButton onClick={() => handleBack()}>
              <NavigateBeforeIcon
                className={classes.navButton}
                fontSize="large"
              />
            </IconButton>
          </Grid>
        </Grid>
        <Grid
          className={classNames(classes.imageContainer, classes.defaultMargin)}
          item
          xs={10}
          onClick={() => {
            router.push(`${cardType}/${cardData._id}`);
          }}
          style={{ position: 'relative' }}
        >
          <Image
            key={cardData._id}
            src={cardData.imagepath || '/'}
            alt="Elsen Yacub"
            layout="responsive"
            width={'100%'}
            height={'50%'}
            className={classes.image}
          />
          <Grid
            container
            justifyContent="center"
            className={classes.blogBackground}
          >
            <Grid
              className={classes.blogBackgroundHover}
              justifyContent="center"
              container
            >
              <Grid item xs={10} className={classes.blogDescription}>
                {cardData.content}
              </Grid>
            </Grid>
            <Grid item className={classes.blogTitle}>
              {cardData.title}
            </Grid>
          </Grid>
        </Grid>
        <Grid item xs={1} justifyContent="center" container>
          <Grid item>
            <IconButton onClick={() => handleNext()}>
              <NavigateNextIcon
                className={classes.navButton}
                fontSize="large"
              />
            </IconButton>
          </Grid>
        </Grid>
      </Grid>
    </>
  );
};

const SummaryCard = () => {
  const classes = useStyles();
  const mdUp = useBreakPoints('mdUp');
  return (
    <Grid item container>
      <Grid item container xs={12} md={5} justifyContent="center">
        <Grid item>
          <div>
            <Image
              src={'/images/Foto Formal.png' || '/'}
              alt="Elsen Yacub"
              width={200}
              height={200}
            />
          </div>
        </Grid>
      </Grid>
      <Grid
        item
        container
        justifyContent={mdUp ? 'flex-start' : 'center'}
        xs={12}
        md={6}
      >
        <Grid
          item
          container
          direction="column"
          alignItems={mdUp ? 'flex-start' : 'center'}
          xs={10}
        >
          <Typography className={classes.nameTypo}>Elsen Yacub</Typography>
          <Typography className={classes.jobTypo}>Software Engineer</Typography>
          <br />
          <br />
          <Typography className={classes.descriptionTypo}>
            It’s story time!
          </Typography>
          <br />
          <br />
          <Typography className={classes.descriptionTypo}>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Since I was a little, I like to
            explore and experiment to make something new, experimenting with
            food/drinks, explore new path for travel, and even making new
            map(mode) for Warcraft III game.
          </Typography>
          <br />
          <Typography className={classes.descriptionTypo}>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;My study on Computer Science was not
            planned, there were a lot of fields that interest me, but finally I
            chose CS because I think its good for future career.
          </Typography>
          <br />
          <Typography className={classes.descriptionTypo}>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Finally, after my start at University,
            I am just grateful that I have chosen Computer Science as my study,
            it just suits to my liking ‘to explore and make something new’
          </Typography>
        </Grid>
      </Grid>
    </Grid>
  );
};

const Contact = () => {
  const classes = useStyles();
  const dispatch = useDispatch();

  const socmedClick = (socmed) => {
    switch (socmed) {
      case 'instagram':
        window.open('https://www.instagram.com/elsenyacub/');
        break;
      case 'linkedin':
        window.open('https://www.linkedin.com/in/elsen-yacub-4b712b1a3/');
        break;
      case 'git':
        window.open('https://gitlab.com/elseny');
        break;
    }
  };

  const { executeRecaptcha } = useGoogleReCaptcha();

  const handleReCaptchaVerify = useCallback(
    async (action) => {
      if (!executeRecaptcha) {
        return null;
      }
      const token = await executeRecaptcha(action);
      return token;
    },
    [executeRecaptcha]
  );

  const sendEmail = async (formData, resetForm) => {
    const recaptchaToken = await handleReCaptchaVerify(SEND_EMAIL_TO_DEV);
    if (!recaptchaToken) {
      dispatch(onError(RECAPTCHA_ERROR));
      setTimeout(() => dispatch(onErrorFinish(RECAPTCHA_ERROR)), 6000);
      return;
    }

    dispatch(
      makePostRequest({
        url: '/mail/send-to-dev',
        data: {
          email: formData.email,
          subject: formData.subject,
          text: formData.text,
          recaptcha_token: recaptchaToken,
        },
        action: SEND_EMAIL_TO_DEV,
      })
    )
      .unwrap()
      .then((res) => {
        // handle result here if (res && res.data && res.data.status === 200) {
        if (res.status === 200) {
          dispatch(onSuccess(res.message));
          setTimeout(() => dispatch(onSuccessFinish(res.message)), 6000);
          resetForm();
        } else {
          dispatch(onError(res.message));
          setTimeout(() => dispatch(onErrorFinish(res.message)), 6000);
        }
      })
      .catch((errorRes) => {
        dispatch(onError(errorRes.message));
        setTimeout(() => dispatch(onErrorFinish(errorRes.message)), 6000);
      });
  };

  return (
    <Grid item container className={classes.body}>
      <Grid item container xs={12} md={6} justifyContent="center">
        <Grid item container xs={10} md={6}>
          <Grid item xs={6} md={12}>
            <IconButton onClick={() => socmedClick('instagram')}>
              <InstagramIcon fontSize="large" />
              <Typography className={classes.socmedTypo}>elsenyacub</Typography>
            </IconButton>
          </Grid>
          <Grid item xs={6} md={12}>
            <IconButton onClick={() => socmedClick('linkedin')}>
              <LinkedInIcon fontSize="large" />
              <Typography className={classes.socmedTypo}>
                Elsen Yacub
              </Typography>
            </IconButton>
          </Grid>
          <Grid item xs={6} md={12}>
            <IconButton onClick={() => socmedClick('git')}>
              <GitHubIcon fontSize="large" />
              <Typography className={classes.socmedTypo}>elseny</Typography>
            </IconButton>
          </Grid>
        </Grid>
      </Grid>
      <Grid item container md={5} xs={12} justifyContent="center">
        <Typography className={classes.jobTypo}>Send Email</Typography>
        <br />
        <br />
        <br />
        <Formik
          initialValues={{
            email: '',
            subject: '',
            text: '',
          }}
          validationSchema={Yup.object({
            email: Yup.string().required('Required').email(),
            subject: Yup.string().required('Required'),
            text: Yup.string().required('Required'),
          })}
          onSubmit={(values, funcs) => sendEmail(values, funcs.resetForm)}
        >
          {({ handleSubmit }) => (
            <Form
              onSubmit={(e) => {
                e.preventDefault();
                handleSubmit();
              }}
              className={classes.formContainer}
            >
              <Grid container direction="column">
                <Grid item container direction="column">
                  <TextInput
                    name="email"
                    label="email (so I can reply your email)"
                    variant="filled"
                  />
                  <TextInput
                    name="subject"
                    label="subject"
                    variant="filled"
                    className={classes.mainTitleTextField}
                  />
                  <TextInput
                    name="text"
                    label="text"
                    variant="filled"
                    inputType="TextArea"
                    className={classes.mainTitleTextField}
                  />
                </Grid>
                <br />
                <GooglePolicy />
                <br />
                <Grid item container justifyContent="center">
                  <Button
                    className={classes.submitButton}
                    type="submit"
                    variant="contained"
                  >
                    Send email
                  </Button>
                </Grid>
              </Grid>
            </Form>
          )}
        </Formik>
      </Grid>
    </Grid>
  );
};

const TextInput = (props) => {
  // useField() returns [formik.getFieldProps(), formik.getFieldMeta()]
  // which we can spread on <input> and alse replace ErrorMessage entirely.
  const classes = useStyles();
  const [field, meta] = useField(props);
  return (
    <>
      <label className={classes.formLabel}>{props.label}</label>
      {props.inputType === 'TextArea' ? (
        <TextareaAutosize
          maxRows={7}
          minRows={7}
          {...field}
          {...props}
          className={classes.formTextField}
          onChange={(e) => {
            field.onChange(e);
          }}
        />
      ) : (
        <input
          {...field}
          {...props}
          className={classes.formTextField}
          onChange={(e) => {
            field.onChange(e);
          }}
        />
      )}
      {meta.touched && meta.error ? (
        <div className={classes.formErrorText}>{meta.error}</div>
      ) : null}
    </>
  );
};

export default function Summary() {
  const [blogShownIdx, setBlogShownIdx] = useState(0);
  const [projectShownIdx, setProjectShownIdx] = useState(0);
  const [openQuickNav, setOpenQuickNav] = useState(false);
  const [blogImage, setBlogImage] = useState([]);
  const classes = useStyles({ quickNavCollapse: openQuickNav });

  const blog = fetchData('/blog/retrieve', RETRIEVE_BLOG, { per_page: 100 });
  const project = fetchData('/project/retrieve', RETRIEVE_PROJECT, {
    per_page: 100,
  });

  const blogData =
    blog.data &&
    blog.data.data.data.map((data) => {
      return {
        ...data,
        content: DOMPurify.sanitize(data.content, { ALLOWED_TAGS: [] }),
      };
    });

  const projectData =
    project.data &&
    project.data.data.data.map((data) => {
      return {
        ...data,
        content: DOMPurify.sanitize(data.content, { ALLOWED_TAGS: [] }),
      };
    });

  // useEffect(() => {
  //   console.log(
  //     '🚀 ~ file: index.jsx ~ line 476 ~ useEffect ~ blogShownIdx',
  //     blogImage
  //   );

  //   if (
  //     blogData &&
  //     blogShownIdx % 5 === 0 &&
  //     blogShownIdx === blogImage.length
  //   ) {
  //     let images = [];
  //     console.log(
  //       '🚀 ~ file: index.jsx ~ line 485 ~ useEffect ~ blogData.length',
  //       blogData.length
  //     );

  //     for (
  //       let i = 0;
  //       i <
  //       (blogData.length - blogShownIdx > 5
  //         ? 5
  //         : blogData.length - blogShownIdx);
  //       i++
  //     ) {
  //       images.push(
  //         <Image
  //           key={blogData[i]._id}
  //           src={blogData[i + blogShownIdx].imagepath}
  //           alt="Elsen Yacub"
  //           layout="responsive"
  //           width={'100%'}
  //           height={'50%'}
  //           className={classes.image}
  //         />
  //       );
  //     }
  //     console.log(
  //       '🚀 ~ file: index.jsx ~ line 476 ~ useEffect ~ images',
  //       images
  //     );
  //     setBlogImage((state) => state.concat(images));
  //     console.log(blogImage);
  //   }
  // }, [blog.data, blogShownIdx]);

  const handleChangeBlogShown = (type) => {
    if (!blogData || !blogData.length) {
      return;
    }

    if (type === 'back') {
      setBlogShownIdx((state) => {
        if (state !== 0) {
          return state - 1;
        } else {
          return blogData.length - 1;
        }
      });
    } else if (type === 'next') {
      setBlogShownIdx((state) => {
        if (state !== blogData.length - 1) {
          return state + 1;
        } else {
          return 0;
        }
      });
    }
  };

  const handleChangeProjectShown = (type) => {
    if (!blogData || !blogData.length) {
      return;
    }

    if (type === 'back') {
      setProjectShownIdx((state) => {
        if (state !== 0) {
          return state - 1;
        } else {
          return projectData.length - 1;
        }
      });
    } else if (type === 'next') {
      setProjectShownIdx((state) => {
        if (state !== projectData.length - 1) {
          return state + 1;
        } else {
          return 0;
        }
      });
    }
  };

  return (
    <Grid container direction="column">
      <Grid
        className={classes.quickNav}
        container
        onClick={() => setOpenQuickNav((state) => !state)}
      >
        <Typography className={classes.quickNavTypo}>Quick Nav</Typography>
        {openQuickNav && (
          <ul className={classes.quickNavItemContainer}>
            <li>
              <Link href="#summary" className={classes.quickNavItemTypo}>
                Summary
              </Link>
            </li>
            <li>
              <Link href="#blog_post" className={classes.quickNavItemTypo}>
                Blog Post
              </Link>
            </li>
            <li>
              <Link href="#project_list" className={classes.quickNavItemTypo}>
                Projects
              </Link>
            </li>
            <li>
              <Link href="#contacts" className={classes.quickNavItemTypo}>
                Contacts
              </Link>
            </li>
            <li>
              <Link href="/" className={classes.quickNavItemTypo}>
                Back to Home
              </Link>
            </li>
          </ul>
        )}
      </Grid>
      <Grid container direction="column" alignItems="center">
        <Grid item id="summary">
          <Link href="/">
            <div>
              <Image
                src={'/favicon.ico' || '/'}
                alt="Elsen Yacub"
                width={102}
                height={51}
              />
            </div>
          </Link>
        </Grid>
        <br />
        <br />
        <Grid item container>
          <SummaryCard />
        </Grid>
      </Grid>
      <br />
      <br />
      <Grid item container justifyContent="center">
        {blog.isError && blog.isLoading && !blog.data ? (
          <></>
        ) : (
          <Grid id="blog_post" item container direction="column" xs={11}>
            <SlidingCard
              header={{
                title: 'Blog Post',
                description: 'Stories I decided to share',
              }}
              cardType={'blog'}
              cardData={
                blogData && blogData[blogShownIdx]
                  ? blogData[blogShownIdx]
                  : {
                      _id: 'no id',
                      imagepath: '/images/water or glass.jpg',
                      title: 'no data!',
                      description: 'no data!',
                    }
              }
              image={blogImage[blogShownIdx]}
              handleBack={() => handleChangeBlogShown('back')}
              handleNext={() => handleChangeBlogShown('next')}
            />
          </Grid>
        )}
      </Grid>
      <br />
      <br />
      <br />
      <br />
      <br />
      <Grid item container justifyContent="center">
        {project.isError && project.isLoading && !project.data ? (
          <></>
        ) : (
          <Grid id="project_list" item container direction="column" xs={11}>
            <SlidingCard
              header={{
                title: 'Projects',
                description: 'My Projects, so far...',
              }}
              cardType="projects"
              cardData={
                projectData && projectData[projectShownIdx]
                  ? projectData[projectShownIdx]
                  : {
                      _id: 'no id',
                      imagepath: '/images/water or glass.jpg',
                      title: 'no data!',
                      description: 'no data!',
                    }
              }
              handleBack={() => handleChangeProjectShown('back')}
              handleNext={() => handleChangeProjectShown('next')}
            />
          </Grid>
        )}
      </Grid>
      <br />
      <br />
      <br />
      <br />
      <br />
      <Grid item container justifyContent="center">
        <Grid item container direction="column" xs={11}>
          <Grid item container direction="column" alignItems="center">
            <Typography className={classes.nameTypo}>Contacts</Typography>
            <Typography className={classes.jobTypo}>
              Reach out to me!
            </Typography>
          </Grid>
          <br />
          <Contact />
          <br />
          <br id="contacts" />
        </Grid>
      </Grid>
    </Grid>
  );
}
