import { useState, useEffect, useCallback } from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import { Formik, Form, useField } from 'formik';
import * as Yup from 'yup';
import Button from '@material-ui/core/Button';
import { login, register } from '../redux/slice/authSlice';
import { useDispatch, useSelector } from 'react-redux';
import { useRouter } from 'next/router';
import Spinner from '../components/Molecules/Spinner/Spinner';
import { makePostRequest } from '../redux/slice/loadingSlice';
import { onSuccess, onSuccessFinish } from '../redux/slice/successSlice';
import { onError, onErrorFinish } from '../redux/slice/errorSlice';
import { RECAPTCHA_ERROR } from '../constants/error';
import { useGoogleReCaptcha } from 'react-google-recaptcha-v3';
import { USER_REGISTER, USER_LOGIN } from '../constants/action';
import axios from 'axios';
import { IconButton } from '@material-ui/core';
import GooglePolicy from '../components/Atom/GooglePolicy';
import VisibilityIcon from '@material-ui/icons/Visibility';
import VisibilityOffIcon from '@material-ui/icons/VisibilityOff';

const useStyles = makeStyles((theme) => ({
  loginContainer: {
    backgroundColor: '#444444',
    width: '400px',
    borderRadius: '15px',
    boxShadow: '10px 10px 50px 0px black',
  },
  loginTypo: {
    fontSize: '45px',
    fontFamily: 'Rokkitt',
    color: '#DBDBDB',
  },
  inputContainer: {
    width: '100%',
    backgroundColor: '#C4C4C4',
    border: 'none',
    fontSize: '20px',
  },
  inputLabel: {
    fontSize: '12px',
    color: '#E9E9E9',
    fontWeight: 'bold',
    fontFamily: 'Arial',
  },
  errorTypo: {
    color: '#E70000',
    fontSize: '15px',
  },
  formContainer: {
    width: '100%',
  },
  submitButton: {
    backgroundImage:
      'linear-gradient(300.89deg, #706A6A 10.12%, rgba(100, 100, 100, 0) 99.31%);',
    filter: 'drop-shadow(0px 4px 4px rgba(0, 0, 0, 0.25));',
    mixBlendMode: 'overlay',
    color: '#FFFFFF',
  },
  noAccountTypo: {
    color: 'white',
    fontSize: '13px',
  },
  registerLink: {
    fontSize: '13px',
    color: '#1BC200',
    '&:hover': {
      textDecoration: 'underline',
      cursor: 'pointer',
    },
  },
  errorMessage: {
    color: 'red',
    textAlign: 'center',
  },
}));

const TextInput = ({ label, ...props }) => {
  // useField() returns [formik.getFieldProps(), formik.getFieldMeta()]
  // which we can spread on <input> and alse replace ErrorMessage entirely.
  const classes = useStyles();
  const [field, meta] = useField(props);
  return (
    <>
      <Grid container direction="column">
        <Grid item>
          <Typography className={classes.inputLabel}>{label}</Typography>
        </Grid>
        <Grid item>
          <input
            className="text-input"
            type={props.type}
            {...field}
            className={classes.inputContainer}
            placeholder={props.placeholder}
          />
        </Grid>
        <Grid item>
          {meta.touched && meta.error ? (
            <Typography className={classes.errorTypo}>{meta.error}</Typography>
          ) : null}
          <br />
        </Grid>
      </Grid>
    </>
  );
};

const AddCommentForm = (props) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const [mode, setMode] = useState('login');
  const [requestError, setRequestError] = useState('');
  const [showPassword, setShowPassword] = useState(false);
  const { executeRecaptcha } = useGoogleReCaptcha();

  const handleReCaptchaVerify = useCallback(
    async (action) => {
      if (!executeRecaptcha) {
        return null;
      }
      const token = await executeRecaptcha(action);
      return token;
    },
    [executeRecaptcha]
  );

  const handleRegister = () => {
    setRequestError('');
    setMode('register');
  };

  const handleLogin = () => {
    setRequestError('');
    setMode('login');
  };

  const loginSubmit = async ({ email, password }, formikProps) => {
    const recaptchaToken = await handleReCaptchaVerify(USER_LOGIN);
    if (!recaptchaToken) {
      dispatch(onError(RECAPTCHA_ERROR));
      setTimeout(() => dispatch(onErrorFinish(RECAPTCHA_ERROR)), 6000);
      return;
    }
    //post method here
    setRequestError('');
    try {
      dispatch(
        makePostRequest({
          url: '/login',
          data: { email, password, recaptcha_token: recaptchaToken },
          action: USER_LOGIN,
        })
      )
        .unwrap()
        .then((res) => {
          // handle result here if (res && res.data && res.data.status === 200) {
          if (res.status === 200) {
            axios.defaults.headers.common['Authorization'] =
              'Bearer ' + res.data.data[0].token;
            dispatch(login(res.data.data[0]));
            dispatch(onSuccess(res.message));
            setTimeout(() => dispatch(onSuccessFinish(res.message)), 6000);
          } else {
            dispatch(onError(res.message));
            setTimeout(() => dispatch(onErrorFinish(res.message)), 6000);
          }
        })
        .catch((errorRes) => {
          dispatch(onError(errorRes.message));
          setTimeout(() => dispatch(onErrorFinish(errorRes.message)), 6000);
        });
      // await axios
      //   .post(`${process.env.NEXT_PUBLIC_BACKEND_SERVICE_URL}/login`, { email, password })
      //   .then((resp) => {
      //     if (resp.data.status !== 200) {
      //       setRequestError(resp.data.message);
      //       return;
      //     }
      //     console.log(resp);
      //     dispatch(login(resp.data.data.data[0]));

      //     axios.defaults.headers.common['Authorization'] =
      //       'Bearer ' + resp.data.data.data[0].token;
      //   });
    } catch (err) {
      console.log(err);
    }
  };

  const registerSubmit = async ({ email, password, name }, formikProps) => {
    const recaptchaToken = await handleReCaptchaVerify(USER_REGISTER);
    if (!recaptchaToken) {
      dispatch(onError(RECAPTCHA_ERROR));
      setTimeout(() => dispatch(onErrorFinish(RECAPTCHA_ERROR)), 6000);
      return;
    }
    //post method here
    setRequestError('');
    try {
      dispatch(
        makePostRequest({
          url: '/signup',
          data: { email, password, name, recaptcha_token: recaptchaToken },
          action: USER_REGISTER,
        })
      )
        .unwrap()
        .then((res) => {
          // handle result here if (res && res.data && res.data.status === 200) {
          if (res.status === 200) {
            loginSubmit({ email, password });
          } else {
            dispatch(onError(res.message));
            setTimeout(() => dispatch(onErrorFinish(res.message)), 6000);
          }
        })
        .catch((errorRes) => {
          dispatch(onError(errorRes.message));
          setTimeout(() => dispatch(onErrorFinish(errorRes.message)), 6000);
        });
      // const signupRes = await axios.post(
      //   `${process.env.NEXT_PUBLIC_BACKEND_SERVICE_URL}/signup`,
      //   { email, password, name }
      // );

      // if (signupRes.status !== 200) {
      //   throw new Error('Failed register');
      // } else {
      //   await axios
      //     .post(`${process.env.NEXT_PUBLIC_BACKEND_SERVICE_URL}/login`, { email, password })
      //     .then((res) => {
      //       if (res.data.status !== 200) {
      //         setRequestError(res.data.message);
      //         return;
      //       }
      //       dispatch(login(res.data));
      //     });
      // }
    } catch (e) {
      console.log(e);
    }
  };

  const registerSchema = {
    name: Yup.string()
      .max(30, 'Name can only be 30 characters maximum')
      .required('Required'),
    email: Yup.string().email().required('Required'),
    password: Yup.string().min(6).required('Required'),
    confirmPassword: Yup.string()
      .when('password', {
        is: (val) => (val && val.length > 0 ? true : false),
        then: Yup.string().oneOf(
          [Yup.ref('password')],
          'Both password need to be the same'
        ),
      })
      .required('required'),
  };

  const loginSchema = {
    email: Yup.string().email().required('Required'),
    password: Yup.string().min(6).required('Required'),
  };

  return (
    <Formik
      initialValues={
        mode === login
          ? {
              email: '',
              password: '',
            }
          : {
              email: '',
              password: '',
              name: '',
              confirmPassword: '',
            }
      }
      validationSchema={Yup.object(
        mode === 'login' ? loginSchema : registerSchema
      )}
      onSubmit={(val, formikProps) => {
        mode === 'login'
          ? loginSubmit(val, formikProps)
          : registerSubmit(val, formikProps);
      }}
    >
      {({ handleSubmit, resetForm }) => (
        <Form
          // role="form"
          onSubmit={(e) => {
            e.preventDefault();
            handleSubmit();
          }}
          className={classes.formContainer}
        >
          <Grid container justifyContent="center">
            <Grid item container xs={12} justifyContent="center">
              <Typography className={classes.loginTypo}>
                {mode === 'login' ? 'Login' : 'Register'}
              </Typography>
            </Grid>
            {requestError.length ? (
              <Grid item container xs={10}>
                <Typography className={classes.errorMessage}>
                  {requestError}
                </Typography>
              </Grid>
            ) : (
              <></>
            )}
            <Grid item xs={12}>
              <br />
            </Grid>
            <Grid item container xs={8}>
              {mode === 'register' && (
                <Grid item xs={10}>
                  <TextInput
                    value=""
                    label="Name"
                    name="name"
                    type="text"
                    placeholder="Jane"
                  />
                </Grid>
              )}
              <Grid item xs={10}>
                <TextInput
                  value=""
                  label="Email"
                  name="email"
                  type="text"
                  placeholder="Jane@gmail.com"
                />
              </Grid>
              <Grid item xs={12} />
              <Grid item container>
                <Grid item xs={10}>
                  <TextInput
                    value=""
                    label="Password"
                    name="password"
                    type={showPassword ? 'text' : 'password'}
                  />
                </Grid>
                <Grid item xs={2}>
                  <IconButton
                    onClick={() => setShowPassword((state) => !state)}
                  >
                    {showPassword ? <VisibilityIcon /> : <VisibilityOffIcon />}
                  </IconButton>
                </Grid>
              </Grid>

              {mode === 'register' && (
                <Grid item xs={10}>
                  <TextInput
                    value=""
                    label="Confirm Password"
                    name="confirmPassword"
                    type="password"
                  />
                </Grid>
              )}
            </Grid>
            <Grid item container xs={8}>
              <Typography className={classes.noAccountTypo}>
                {mode === 'login'
                  ? 'Does not have an account yet?'
                  : 'Already have an Account?'}
                &nbsp;
              </Typography>
              <Typography
                onClick={() => {
                  resetForm();
                  mode === 'login' ? handleRegister() : handleLogin();
                }}
                className={classes.registerLink}
              >
                {mode === 'login' ? 'Register' : 'Login'}
              </Typography>
            </Grid>
            <Grid item xs={12}>
              <br />
            </Grid>
            <Grid item xs={8} container justifyContent="center">
              <Button
                type="submit"
                variant="contained"
                className={classes.submitButton}
              >
                Submit
              </Button>
            </Grid>
            <Grid item xs={12}>
              <br />
            </Grid>
            <Grid item container xs={10}>
              <GooglePolicy />
            </Grid>
          </Grid>
        </Form>
      )}
    </Formik>
  );
};

export default function auth() {
  const classes = useStyles();
  const auth = useSelector((state) => state.auth);
  const router = useRouter();

  useEffect(() => {
    if (auth.isLoggedIn) {
      router.push('/');
    }
  }, [auth.isLoggedIn]);

  return auth.isLoggedIn ? (
    <Spinner />
  ) : (
    <Grid
      container
      direction="column"
      justifyContent="center"
      style={{ flexGrow: 1 }}
    >
      <Grid item xs={12} container justifyContent="center">
        <Grid
          item
          container
          className={classes.loginContainer}
          direction="column"
        >
          <Grid item xs={1} />
          <Grid item container justifyContent="center">
            <AddCommentForm />
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
}
