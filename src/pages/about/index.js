import Grid from '@material-ui/core/Grid';
import Image from 'next/image';
import useBreakpoints from '../../helpers/breakpoints';
import { makeStyles } from '@material-ui/core/styles';
import { Typography } from '@material-ui/core';
import Head from 'next/head';
import classNames from 'classnames/bind';

const useStyles = makeStyles((theme) => ({
  contentTypoHead: {
    fontSize: '80px',
    fontFamily: 'Sorts Mill Goudy',
    textAlign: 'center',
    color: '#FFFFFF',
  },
  contentTypoBody: {
    fontSize: '20px',
    fontFamily: 'Sorts Mill Goudy',
    color: '#D9D9D9',
    textAlign: 'justify',
  },
  imageContainer: {
    borderRadius: '30px',
    boxShadow: '30px 30px 50px 0px rgba(0,0,0,0.50)',
  },
  defaultMargin: {
    margin: '20px',
  },
  image: {
    borderRadius: '30px',
  },
}));

export default function about() {
  const classes = useStyles();
  const smDown = useBreakpoints('smDown');

  return (
    <>
      <Head>
        <title>About Elsen</title>
        <meta name="description" content="Find out more about Elsen Yacub" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <div>
        <Grid container>
          <Grid
            item
            container
            alignItems="center"
            justifyContent="center"
            xs={12}
            md={4}
          >
            <div
              className={classNames(
                classes.imageContainer,
                classes.defaultMargin
              )}
            >
              <Image
                src={'/images/Foto kebun raya.jpg' || '/'}
                alt="Elsen Yacub"
                width={300}
                height={500}
                className={classes.image}
              />
            </div>
          </Grid>
          <Grid
            item
            md={7}
            direction={smDown ? 'row' : 'column'}
            container
            justifyContent="center"
          >
            <Grid
              item
              xs={smDown && 12}
              container
              justifyContent={smDown ? 'center' : 'flex-start'}
            >
              <Typography className={classes.contentTypoHead}>
                {smDown ? 'Above' : 'Left'} is,
              </Typography>
            </Grid>
            <Grid
              item
              xs={smDown && 10}
              container
              justifyContent={smDown ? 'center' : 'flex-start'}
            >
              <Typography className={classes.contentTypoBody}>
                A picture of me at Kebun Raya Bogor (looks cool right? hehe)
              </Typography>
            </Grid>
            <br />
            <Grid
              item
              xs={smDown && 12}
              container
              justifyContent={smDown ? 'center' : 'flex-start'}
            >
              <Typography className={classes.contentTypoHead}>
                About Me
              </Typography>
            </Grid>
            <Grid item xs={smDown && 10}>
              <Typography className={classes.contentTypoBody}>
                Hello, my name is Elsen Yacub, a Software Engineer. I graduate
                as a Computer Science Bachelor on 2021 at Bina Nusantara
                University (3,5 years). Currently my main interest is on
                developing web/mobile application.
              </Typography>
            </Grid>
          </Grid>
        </Grid>
      </div>
    </>
  );
}
