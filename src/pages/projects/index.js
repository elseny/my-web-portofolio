import { useState, useEffect } from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Image from 'next/image';
import Button from '@material-ui/core/Button';
import DOMPurify from 'isomorphic-dompurify';
import Link from 'next/link';
import { RETRIEVE_PROJECT } from '../../constants/action';
import useBreakpoints from '../../helpers/breakpoints';
import Drawer from '@material-ui/core/Drawer';
import classNames from 'classnames';
import { useDispatch, useSelector } from 'react-redux';
import { fetchInfiniteData } from '../../hooks/swr/fetchData';
import { onError, onErrorFinish } from '../../redux/slice/errorSlice';
import Head from 'next/head';

const useStyles = makeStyles((theme) => ({
  contentTypo: {
    color: '#2ad20f',
    fontFamily: 'Sorts Mill Goudy',
    margin: '10px',
  },
  projectTitle: {
    wordBreak: 'break-all',
    textAlign: 'center',
    fontSize: '50px',
    fontWeight: 'bold',
    color: '#FFFFFF',
  },
  projectDescription: {
    color: '#CCCCCC',
    textAlign: 'justify',
  },
  image: {
    borderRadius: '30px',
  },
  titleContainer: {
    backgroundColor: '#202020',
    top: (props) => {
      return props.mdUp ? '64px' : '56px';
    },
    position: 'sticky',
    height: '100vh',
    boxShadow: '30px 30px 50px 30px rgba(0,0,0,0.20)',
  },
  imageItem: {
    cursor: 'pointer',
  },
  projectText: {
    overflow: 'hidden',
    height: '200px',
  },
  detailButton: {
    margin: '20px 20px 0px 0px',
    border: 0,
    backgroundColor: 'green',
    '&:hover': {
      backgroundColor: 'limegreen',
    },
  },
  projectContainer: {
    marginTop: '50px',
  },
  leftProjectTitle: {
    cursor: 'pointer',
    fontSize: '20px',
    margin: '15px',
    color: 'white',
  },
  mobileTitle: {
    position: 'sticky',
    top: (props) => {
      return props.mdUp ? '64px' : '56px';
    },
    backgroundColor: '#202020',
    borderRadius: '0px 0px 10px 0px',
    padding: '10px',
    margin: '0px',
    cursor: 'pointer',
    zIndex: 10,
  },
  showMoreButton: {
    color: 'white',
  },
}));

const ProjectCards = ({ project }) => {
  const classes = useStyles();

  DOMPurify.addHook('afterSanitizeAttributes', function (node) {
    // set all elements owning target to target=_blank
    if ('target' in node) {
      node.setAttribute('target', '_blank');
      node.setAttribute('rel', 'noopener');
    }
  });

  const projectData = project.data.data.map((data) => {
    return {
      ...data,
      content: DOMPurify.sanitize(data.content, { FORBID_TAGS: ['img'] }),
    };
  });

  return (
    <>
      <Head>
        <title>Elsen's Project</title>
        <meta name="description" content="Elsen Yacub's Project" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      {projectData.map((project, idx) => {
        return (
          <Grid item container xs={12} key={project._id}>
            <Link href={`projects/${project._id}`}>
              <Grid
                item
                container
                xs={12}
                md={4}
                justifyContent="center"
                className={classes.imageItem}
              >
                <Grid item container xs={8} md={12}>
                  <Image
                    src={project.imagepath || '/'}
                    width={450}
                    height={300}
                    className={classes.image}
                    layout="fixed"
                  />
                </Grid>
              </Grid>
            </Link>
            <Grid item md={1} />
            <Grid item container xs={12} md={7} justifyContent="center">
              <Grid item container xs={11} justifyContent="center">
                <Grid item xs={10}>
                  <Typography className={classes.projectTitle}>
                    {project.title}
                  </Typography>
                </Grid>
              </Grid>
              <Grid
                item
                container
                justifyContent="center"
                xs={11}
                className={classes.projectText}
              >
                <Grid item>
                  <div
                    dangerouslySetInnerHTML={{
                      __html: DOMPurify.sanitize(project.content),
                    }}
                    className={classes.projectDescription}
                  />
                </Grid>
              </Grid>
              <Link href={`projects/${project._id}`}>
                <Grid
                  item
                  container
                  justifyContent="flex-end"
                  alignItems="flex-end"
                >
                  <Button
                    variant="contained"
                    color="default"
                    className={classes.detailButton}
                    id={
                      idx !== projectData.length - 1
                        ? projectData[idx + 1]._id
                        : `last-button-${projectData[idx]._id}`
                    }
                  >
                    Details {`>>>`}
                  </Button>
                </Grid>
              </Link>
            </Grid>
            <Grid item xs={12}>
              <br />
              <hr className={classes.separateLine} />
              <br />
            </Grid>
          </Grid>
        );
      })}
    </>
  );
};

export default function Projects(props) {
  const mdUp = useBreakpoints('mdUp');
  const classes = useStyles({ mdUp: mdUp });
  const dispatch = useDispatch();

  const project = fetchInfiniteData(
    '/project/retrieve',
    RETRIEVE_PROJECT,
    {
      per_page: 5,
    },
    {
      maxReachedComment: 'No more project!',
    }
  );

  const auth = useSelector((state) => state.auth);
  const [openDrawer, setOpenDrawer] = useState(false);

  const handleToggleDrawer = (val) => {
    setOpenDrawer(val);
  };

  if (!project.data) {
    return <></>;
  }
  return (
    <>
      <div
        style={{
          top: '-100px',
          height: '10px',
          width: '10px',
          position: 'absolute',
        }}
        id={project.data[0].data.data[0]?._id}
      />
      <Grid container>
        <Typography
          variant="h5"
          color="initial"
          className={classNames(classes.contentTypo, classes.mobileTitle)}
          onClick={() => setOpenDrawer(true)}
        >
          Project List
        </Typography>
        {project.isError && project.isLoading && !project.data ? (
          <></>
        ) : (
          <Grid item container justifyContent="center">
            <Grid item container md={11} className={classes.projectContainer}>
              {project.data &&
                project.data.map((projectData, idx) => {
                  return (
                    <ProjectCards
                      project={projectData}
                      key={`projectData.data.last_page_id-${idx}`}
                    />
                  );
                })}
            </Grid>
          </Grid>
        )}
        <Grid item container justifyContent="center">
          <Button
            className={classes.showMoreButton}
            onClick={() => {
              if (!project.maxDataAchieved) {
                project.setSize(project.size + 1);
              } else {
                dispatch(onError('No more Project!'));
                setTimeout(() => {
                  dispatch(onErrorFinish('No more Project!'));
                }, 6000);
              }
            }}
          >
            Show more...
          </Button>
        </Grid>
      </Grid>
      <Drawer
        anchor="left"
        open={openDrawer}
        onClose={() => handleToggleDrawer(false)}
      >
        <div className={classes.titleContainer}>
          <Typography
            variant="h5"
            color="initial"
            className={classes.contentTypo}
          >
            Project List
            {auth.role === 'admin' && (
              <Link href={`/projects/create`}>
                <Button>Create One</Button>
              </Link>
            )}
            <ul>
              {project.data ? (
                project.data.map((projectData, idx) => {
                  let lastBatchId = null;

                  if (idx > 0) {
                    const prevProjectData =
                      idx > 0 && project.data[idx - 1].data.data;
                    lastBatchId =
                      idx > 0 &&
                      prevProjectData[prevProjectData.length - 1]._id;
                  }

                  return projectData.data.data.map(
                    (eachProjectData, childIdx) => {
                      return (
                        <Link
                          href={
                            childIdx === 0 && lastBatchId
                              ? `#last-button-${lastBatchId}`
                              : `#${eachProjectData._id}`
                          }
                          key={`link-${eachProjectData._id}`}
                        >
                          <li onClick={() => handleToggleDrawer(false)}>
                            <Typography className={classes.leftProjectTitle}>
                              {eachProjectData.title}
                            </Typography>
                          </li>
                        </Link>
                      );
                    }
                  );
                })
              ) : (
                <></>
              )}
            </ul>
          </Typography>
        </div>
      </Drawer>
    </>
  );
}
