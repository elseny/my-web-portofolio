import { useState, useCallback } from 'react';
import CustomEditor from '../../components/Organism/CustomEditor/CustomEditor';
import {
  onProjectContentChange,
  onTitleChange,
  onImagepathChange,
} from '../../redux/slice/addProjectSlice';
import { useSelector, useDispatch } from 'react-redux';
import { makePostRequest } from '../../redux/slice/loadingSlice';
import { CREATE_PROJECT } from '../../constants/action';
import { onError, onErrorFinish } from '../../redux/slice/errorSlice';
import { onSuccess, onSuccessFinish } from '../../redux/slice/successSlice';
import { RECAPTCHA_ERROR } from '../../constants/error';
import { useGoogleReCaptcha } from 'react-google-recaptcha-v3';
import * as Yup from 'yup';

export default function CreateProject() {
  const dispatch = useDispatch();
  const addProjectState = useSelector((state) => state.addProject);
  const auth = useSelector((state) => state.auth);

  const [gitUrl, setGitUrl] = useState('');
  const [webUrl, setWebUrl] = useState('');
  const { executeRecaptcha } = useGoogleReCaptcha();

  const additionalProperties = [
    {
      name: 'gitUrl',
      label: 'Git Url',
      value: gitUrl,
      onChange: (e) => setGitUrl(e.target.value),
      validator: Yup.string().nullable(),
    },
    {
      name: 'webUrl',
      label: 'Web Url',
      value: webUrl,
      onChange: (e) => setWebUrl(e.target.value),
      validator: Yup.string().nullable(),
    },
  ];

  const handleReCaptchaVerify = useCallback(
    async (action) => {
      if (!executeRecaptcha) {
        return null;
      }
      const token = await executeRecaptcha(action);
      return token;
    },
    [executeRecaptcha]
  );

  const handleCreateSubmit = async (formData) => {
    const recaptchaToken = await handleReCaptchaVerify(CREATE_PROJECT);
    if (!recaptchaToken) {
      dispatch(onError(RECAPTCHA_ERROR));
      setTimeout(() => dispatch(onErrorFinish(RECAPTCHA_ERROR)), 6000);
      return;
    }

    dispatch(
      makePostRequest({
        url: '/admin/project/create',
        data: {
          content: formData.content,
          imagepath: formData.mainImagepath,
          title: formData.title,
          git_url: formData.gitUrl,
          web_url: formData.webUrl,
          recaptcha_token: recaptchaToken,
        },
        action: CREATE_PROJECT,
      })
    )
      .unwrap()
      .then((res) => {
        // handle result here if (res && res.data && res.data.status === 200) {
        if (res.status === 200) {
          dispatch(onSuccess(res.message));
          setTimeout(() => dispatch(onSuccessFinish(res.message)), 6000);
          dispatch(onProjectContentChange({ content: '' }));
          dispatch(onImagepathChange({ imagepath: '' }));
        } else {
          dispatch(onError(res.message));
          setTimeout(() => dispatch(onErrorFinish(res.message)), 6000);
        }
      })
      .catch((errorRes) => {
        dispatch(onError(errorRes.message));
        setTimeout(() => dispatch(onErrorFinish(errorRes.message)), 6000);
      });
  };

  return auth.role === 'admin' ? (
    <CustomEditor
      mainImagepath={addProjectState.imagepath}
      title={addProjectState.title}
      content={addProjectState.content}
      handleCreateSubmit={handleCreateSubmit}
      onTitleChange={(e) => dispatch(onTitleChange({ title: e.target.value }))}
      onContentChange={(newContent) =>
        dispatch(onProjectContentChange({ content: newContent }))
      }
      onMainImagepathChange={(imagepath) =>
        dispatch(onImagepathChange({ imagepath: imagepath }))
      }
      mode="new"
      additionalProperties={additionalProperties}
    />
  ) : (
    <div>UNAUTHORIZED</div>
  );
}
