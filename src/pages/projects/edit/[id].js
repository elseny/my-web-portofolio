import { useState, useEffect, useCallback } from 'react';
import CustomEditor from '../../../components/Organism/CustomEditor/CustomEditor';
import DOMPurify from 'isomorphic-dompurify';
import { RETRIEVE_PROJECT, EDIT_PROJECT } from '../../../constants/action';
import { fetchData } from '../../../hooks/swr/fetchData';
import { useDispatch, useSelector } from 'react-redux';
import { makePostRequest } from '../../../redux/slice/loadingSlice';
import { useRouter } from 'next/router';
import { onSuccess, onSuccessFinish } from '../../../redux/slice/successSlice';
import { onError, onErrorFinish } from '../../../redux/slice/errorSlice';
import { RECAPTCHA_ERROR } from '../../../constants/error';
import { useGoogleReCaptcha } from 'react-google-recaptcha-v3';
import * as Yup from 'yup';

export default function EditProject() {
  const auth = useSelector((state) => state.auth);
  const dispatch = useDispatch();
  const router = useRouter();
  const project = fetchData('/project/retrieve', RETRIEVE_PROJECT, {
    id: router.query.id,
  });
  const projectData = project.data && project.data.data.data[0];

  const [title, setTitle] = useState('');
  const [mainImagepath, setMainImagepath] = useState('');
  const [content, setContent] = useState('');
  const [gitUrl, setGitUrl] = useState('');
  const [webUrl, setWebUrl] = useState('');
  const { executeRecaptcha } = useGoogleReCaptcha();

  const additionalProperties = [
    {
      name: 'gitUrl',
      label: 'Git Url',
      onChange: (e) => setGitUrl(e.target.value),
      validator: Yup.string().nullable(),
    },
    {
      name: 'webUrl',
      label: 'Web Url',
      onChange: (e) => setWebUrl(e.target.value),
      validator: Yup.string().nullable(),
    },
  ];

  const handleReCaptchaVerify = useCallback(
    async (action) => {
      if (!executeRecaptcha) {
        return null;
      }
      const token = await executeRecaptcha(action);
      return token;
    },
    [executeRecaptcha]
  );

  useEffect(() => {
    if (project.data) {
      setTitle(projectData.title);
      setMainImagepath(projectData.imagepath);
      setContent(projectData.content);
      setGitUrl(projectData.git_url);
      setWebUrl(projectData.web_url);
    }
  }, [project.data]);

  const handleCreateSubmit = async (formData) => {
    const recaptchaToken = await handleReCaptchaVerify(EDIT_PROJECT);
    if (!recaptchaToken) {
      dispatch(onError(RECAPTCHA_ERROR));
      setTimeout(() => dispatch(onErrorFinish(RECAPTCHA_ERROR)), 6000);
      return;
    }

    dispatch(
      makePostRequest({
        url: '/admin/project/edit',
        data: {
          title: formData.title,
          content: formData.content,
          imagepath: formData.mainImagepath,
          git_url: formData.gitUrl,
          web_url: formData.webUrl,
          id: router.query.id,
          recaptcha_token: recaptchaToken,
        },
        action: EDIT_PROJECT,
      })
    )
      .unwrap()
      .then((res) => {
        // handle result here if (res && res.data && res.data.status === 200) {
        if (res.status === 200) {
          dispatch(onSuccess(res.message));
          setTimeout(() => dispatch(onSuccessFinish(res.message)), 6000);
        } else {
          dispatch(onError(res.message));
          setTimeout(() => dispatch(onErrorFinish(res.message)), 6000);
        }
      })
      .catch((errorRes) => {
        dispatch(onError(errorRes.message));
        setTimeout(() => dispatch(onErrorFinish(errorRes.message)), 6000);
      });
  };

  return auth.role === 'admin' ? (
    <CustomEditor
      mainImagepath={mainImagepath}
      title={title}
      content={content}
      gitUrl={gitUrl}
      webUrl={webUrl}
      handleCreateSubmit={handleCreateSubmit}
      onTitleChange={(e) => setTitle(e.target.value)}
      onContentChange={(newContent) => setContent(newContent)}
      onMainImagepathChange={(imagepath) => setMainImagepath(imagepath)}
      additionalProperties={additionalProperties}
      mode="edit"
    />
  ) : (
    <div>UNAUTHORIZED</div>
  );
}
