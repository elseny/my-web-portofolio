import { useState, useCallback } from 'react';
import Grid from '@material-ui/core/Grid';
import Link from 'next/link';
import ValidationDialog from '../../components/Organism/ConfirmDialog/ConfirmDialog';
import Image from 'next/image';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import GitHubIcon from '@material-ui/icons/GitHub';
import Tooltip from '@material-ui/core/Tooltip';
import IconButton from '@material-ui/core/IconButton';
import LanguageIcon from '@material-ui/icons/Language';
import DOMPurify from 'isomorphic-dompurify';
import { RETRIEVE_PROJECT, DELETE_PROJECT } from '../../constants/action';
import { fetchData } from '../../hooks/swr/fetchData';
import { useRouter } from 'next/router';
import { makePostRequest } from '../../redux/slice/loadingSlice';
import { onSuccess, onSuccessFinish } from '../../redux/slice/successSlice';
import { onError, onErrorFinish } from '../../redux/slice/errorSlice';
import { useDispatch, useSelector } from 'react-redux';
import { getFormattedDate } from '../../helpers/dateHelper';
import { RECAPTCHA_ERROR } from '../../constants/error';
import { useGoogleReCaptcha } from 'react-google-recaptcha-v3';
import Head from 'next/head';

const useStyles = makeStyles((theme) => ({
  defaultMargin: {
    margin: '20px',
  },
  icons: {
    fontSize: '35px',
    color: 'wheat',
  },
  bodyDescription: {
    textAlign: 'justify',
    color: '#D0D0D0',
    fontFamily: 'Sorts Mill Goudy',
  },
  titleTypo: {
    textAlign: 'center',
    color: '#D0D0D0',
    fontFamily: 'Arial Black',
    wordBreak: 'break-word',
    marginBottom: '20px',
  },
  dateTypo: {
    color: '#2ad70d',
  },
}));

export default function ProjectItem(props) {
  const classes = useStyles();
  const router = useRouter();
  const dispatch = useDispatch();
  const [openValDeleteDialog, setOpenValDeleteDialog] = useState(false);
  const projectData = fetchData('/project/retrieve', RETRIEVE_PROJECT, {
    id: router.query.id,
  });
  const auth = useSelector((state) => state.auth);
  const { executeRecaptcha } = useGoogleReCaptcha();

  const handleReCaptchaVerify = useCallback(
    async (action) => {
      if (!executeRecaptcha) {
        return null;
      }
      const token = await executeRecaptcha(action);
      return token;
    },
    [executeRecaptcha]
  );

  if (!projectData.data && (projectData.isError || projectData.isLoading)) {
    return <></>;
  }

  const submitDelete = async () => {
    if (!router.query.id) {
      dispatch(onError('Project Not Found!'));
      return;
    }

    const recaptchaToken = await handleReCaptchaVerify(DELETE_PROJECT);
    if (!recaptchaToken) {
      dispatch(onError(RECAPTCHA_ERROR));
      setTimeout(() => dispatch(onErrorFinish(RECAPTCHA_ERROR)), 6000);
      return;
    }

    dispatch(
      makePostRequest({
        url: '/admin/project/delete',
        data: {
          id: router.query.id,
          recaptcha_token: recaptchaToken,
        },
        action: DELETE_PROJECT,
      })
    )
      .unwrap()
      .then((res) => {
        // handle result here if (res && res.data && res.data.status === 200) {
        if (res.status === 200) {
          dispatch(onSuccess(res.message));
          setTimeout(() => dispatch(onSuccessFinish(res.message)), 6000);
          router.push('/projects');
        } else {
          dispatch(onError(res.message));
          setTimeout(() => dispatch(onErrorFinish(res.message)), 6000);
        }
      })
      .catch((errorRes) => {
        dispatch(onError(errorRes.message));
        setTimeout(() => dispatch(onErrorFinish(errorRes.message)), 6000);
      });
  };

  const redirectNewWindow = (url) => {
    window.open(url);
    return;
  };

  DOMPurify.addHook('afterSanitizeAttributes', function (node) {
    // set all elements owning target to target=_blank
    if ('target' in node) {
      node.setAttribute('target', '_blank');
      node.setAttribute('rel', 'noopener');
    }
  });

  const sanitizedProjectContent = DOMPurify.sanitize(
    projectData.data.data.data[0].content
  );

  return (
    <>
      <Head>
        <title>
          {projectData.data.data.data[0]?.title || 'Blog not found'}
        </title>
        <meta
          name="description"
          content={projectData.data.data.data[0]?.title || 'Blog not found'}
        />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Grid container>
        <Grid item container md={4} xs={12}>
          <Grid item container direction="column" alignItems="center">
            <Typography className={classes.dateTypo} variant="h5">
              {getFormattedDate(
                new Date(projectData.data.data.data[0].created_at),
                'dd MM yyyy hh:ii'
              )}
            </Typography>
            {auth.role === 'admin' && (
              <Grid item container justifyContent="space-around">
                <Grid item>
                  <Link
                    href={{
                      pathname: `/projects/edit/${projectData.data.data.data[0]._id}`,
                    }}
                  >
                    <Button className={classes.editButton} variant="contained">
                      EDIT
                    </Button>
                  </Link>
                </Grid>
                <Grid item>
                  <Button
                    className={classes.deleteButton}
                    variant="contained"
                    onClick={() => setOpenValDeleteDialog(true)}
                  >
                    DELETE
                  </Button>
                </Grid>
              </Grid>
            )}
          </Grid>
          <Grid
            item
            container
            justifyContent="center"
            xs={12}
            className={classes.defaultMargin}
          >
            <Image
              src={projectData.data.data.data[0].imagepath || '/'}
              alt="Elsen Yacub"
              width={300}
              height={300}
            />
          </Grid>
          <Grid item container justifyContent="center">
            {projectData.data.data.data[0].git_url && (
              <Grid item>
                <Tooltip title="The Git url">
                  <IconButton
                    onClick={() =>
                      redirectNewWindow(projectData.data.data.data[0].git_url)
                    }
                  >
                    <GitHubIcon className={classes.icons} />
                  </IconButton>
                </Tooltip>
              </Grid>
            )}
            {projectData.data.data.data[0].web_url && (
              <Grid item>
                <Tooltip title="The app url">
                  <IconButton
                    onClick={() =>
                      redirectNewWindow(projectData.data.data.data[0].web_url)
                    }
                  >
                    <LanguageIcon className={classes.icons} />
                  </IconButton>
                </Tooltip>
              </Grid>
            )}
          </Grid>
        </Grid>
        <Grid item container md={8} xs={12} justifyContent="center">
          <Grid item container xs={11} justifyContent="center">
            <Typography variant="h1" className={classes.titleTypo}>
              {projectData.data.data.data[0].title}
            </Typography>
          </Grid>
          <Grid item container xs={11}>
            <div
              dangerouslySetInnerHTML={{
                __html: sanitizedProjectContent,
              }}
              className={classes.bodyDescription}
            />
          </Grid>
        </Grid>
        <ValidationDialog
          title={`Delete Project "${projectData.data.data.data[0].title}"`}
          description="Are you sure want to delete this blog? the action can not be undone!"
          open={openValDeleteDialog}
          onClose={() => setOpenValDeleteDialog(false)}
          onNo={() => setOpenValDeleteDialog(false)}
          onYes={() => {
            submitDelete();
          }}
        />
      </Grid>
    </>
  );
}
