import { useCallback } from 'react';
import CustomEditor from '../../components/Organism/CustomEditor/CustomEditor';
import {
  onBlogContentChange,
  onTitleChange,
  onImagepathChange,
} from '../../redux/slice/addBlogSlice';
import { useSelector, useDispatch } from 'react-redux';
import { makePostRequest } from '../../redux/slice/loadingSlice';
import { CREATE_BLOG } from '../../constants/permissions';
import { onError, onErrorFinish } from '../../redux/slice/errorSlice';
import { onSuccess, onSuccessFinish } from '../../redux/slice/successSlice';
import { RECAPTCHA_ERROR } from '../../constants/error';
import { useGoogleReCaptcha } from 'react-google-recaptcha-v3';

export default function CreateBlog() {
  const dispatch = useDispatch();
  const addBlogState = useSelector((state) => state.addBlog);
  const auth = useSelector((state) => state.auth);
  const { executeRecaptcha } = useGoogleReCaptcha();

  const handleReCaptchaVerify = useCallback(
    async (action) => {
      if (!executeRecaptcha) {
        return null;
      }
      const token = await executeRecaptcha(action);
      return token;
    },
    [executeRecaptcha]
  );

  const handleCreateSubmit = async (formData) => {
    const recaptchaToken = await handleReCaptchaVerify(CREATE_BLOG);
    if (!recaptchaToken) {
      dispatch(onError(RECAPTCHA_ERROR));
      setTimeout(() => dispatch(onErrorFinish(RECAPTCHA_ERROR)), 6000);
      return;
    }

    dispatch(
      makePostRequest({
        url: '/admin/blog/create',
        data: {
          content: formData.content,
          imagepath: formData.mainImagepath,
          title: formData.title,
          recaptcha_token: recaptchaToken,
        },
        action: CREATE_BLOG,
      })
    )
      .unwrap()
      .then((res) => {
        // handle result here if (res && res.data && res.data.status === 200) {
        if (res.status === 200) {
          dispatch(onSuccess(res.message));
          setTimeout(() => dispatch(onSuccessFinish(res.message)), 6000);
          dispatch(onBlogContentChange({ content: '' }));
          dispatch(onImagepathChange({ imagepath: '' }));
        } else {
          dispatch(onError(res.message));
          setTimeout(() => dispatch(onErrorFinish(res.message)), 6000);
        }
      })
      .catch((errorRes) => {
        dispatch(onError(errorRes.message));
        setTimeout(() => dispatch(onErrorFinish(errorRes.message)), 6000);
      });
  };

  return auth.role === 'admin' ? (
    <CustomEditor
      mainImagepath={addBlogState.imagepath}
      title={addBlogState.title}
      content={addBlogState.content}
      handleCreateSubmit={handleCreateSubmit}
      onTitleChange={(e) => dispatch(onTitleChange({ title: e.target.value }))}
      onContentChange={(newContent) =>
        dispatch(onBlogContentChange({ content: newContent }))
      }
      onMainImagepathChange={(imagepath) =>
        dispatch(onImagepathChange({ imagepath: imagepath }))
      }
      mode="new"
    />
  ) : (
    <div>UNAUTHORIZED!</div>
  );
}
