import { useState, useCallback } from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import { useRouter } from 'next/router';
import { useDispatch } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import CommentList from '../../components/Templates/CommentList/CommentList';
import ValidationDialog from '../../components/Organism/ConfirmDialog/ConfirmDialog';
import Image from 'next/image';
import Link from 'next/link';
import { fetchData } from '../../hooks/swr/fetchData';
import { makePostRequest } from '../../redux/slice/loadingSlice';
import { onSuccess, onSuccessFinish } from '../../redux/slice/successSlice';
import { onError, onErrorFinish } from '../../redux/slice/errorSlice';
import { getFormattedDate } from '../../helpers/dateHelper';
import { DELETE_BLOG } from '../../constants/action';
import DOMPurify from 'isomorphic-dompurify';
import { useSelector } from 'react-redux';
import { RECAPTCHA_ERROR } from '../../constants/error';
import { useGoogleReCaptcha } from 'react-google-recaptcha-v3';
import Head from 'next/head';

const useStyles = makeStyles((theme) => ({
  bodyDescription: {
    textAlign: 'justify',
    color: '#D0D0D0',
  },
  dateTypo: {
    color: '#2AD70D',
    margin: '0px 15px',
  },
  editButton: {
    backgroundColor: '#ffa000',
    '&:hover': {
      backgroundColor: '#ffa000',
    },
  },
  deleteButton: {
    backgroundColor: '#F32013',
    '&:hover': {
      backgroundColor: '#F32013',
    },
  },
  titleTypo: {
    color: '#D0D0D0',
    fontFamily: 'Arial Black',
    wordBreak: 'break-word',
    marginBottom: '20px',
    textAlign: 'center',
  },
  commentContainer: {
    backgroundColor: '#9B9B9B',
    padding: '15px',
    margin: '10px',
    boxShadow: '30px 30px 50px 0px rgba(0,0,0,0.50)',
  },
  commentHeaderText: {
    color: 'white',
    fontFamily: 'Sorts Mill Goudy',
    marginTop: '40px',
  },
  showNextCommentIcon: {
    color: 'white',
    width: '100px',
    fontWeight: 'bold',
  },
}));

export default function BlogDetail() {
  const classes = useStyles();
  const dispatch = useDispatch();
  const router = useRouter();
  const blogRawData = fetchData('/blog/retrieve', 'FETCH_BLOG', {
    id: router.query.id,
  });
  const auth = useSelector((state) => state.auth);
  const [openValDeleteDialog, setOpenValDeleteDialog] = useState(false);
  const { executeRecaptcha } = useGoogleReCaptcha();

  const handleReCaptchaVerify = useCallback(
    async (action) => {
      if (!executeRecaptcha) {
        return null;
      }
      const token = await executeRecaptcha(action);
      return token;
    },
    [executeRecaptcha]
  );

  if (!blogRawData.data && (blogRawData.isError || blogRawData.isLoading)) {
    return <></>;
  }

  const blogData = blogRawData.data.data.data;

  const submitDelete = async () => {
    if (!router.query.id) {
      dispatch(onError('Blog Not Found!'));
      return;
    }

    const recaptchaToken = await handleReCaptchaVerify(DELETE_BLOG);

    if (!recaptchaToken) {
      dispatch(onError(RECAPTCHA_ERROR));
      setTimeout(() => dispatch(onErrorFinish(RECAPTCHA_ERROR)), 6000);
      return;
    }

    dispatch(
      makePostRequest({
        url: '/admin/blog/delete',
        data: {
          id: router.query.id,
          recaptcha_token: recaptchaToken,
        },
        action: DELETE_BLOG,
      })
    )
      .unwrap()
      .then((res) => {
        // handle result here if (res && res.data && res.data.status === 200) {
        if (res.status === 200) {
          dispatch(onSuccess(res.message));
          setTimeout(() => dispatch(onSuccessFinish(res.message)), 6000);
          router.push('/blog');
        } else {
          dispatch(onError(res.message));
          setTimeout(() => dispatch(onErrorFinish(res.message)), 6000);
        }
      })
      .catch((errorRes) => {
        dispatch(onError(errorRes.message));
        setTimeout(() => dispatch(onErrorFinish(errorRes.message)), 6000);
      });
  };

  DOMPurify.addHook('afterSanitizeAttributes', function (node) {
    // set all elements owning target to target=_blank
    if ('target' in node) {
      node.setAttribute('target', '_blank');
      node.setAttribute('rel', 'noopener');
    }
  });

  const sanitizedBlogContent =
    blogData[0] && DOMPurify.sanitize(blogData[0].content);

  return (
    <>
      <Head>
        <title>{blogData[0]?.title || 'Blog not found'}</title>
        <meta
          name="description"
          content={blogData[0]?.title || 'Blog not found'}
        />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      {blogData[0] && router.query.id ? (
        <Grid item container direction="column">
          <br />
          <Grid item container justifyContent="center">
            <Grid item container justifyContent="center" xs={12}>
              <Typography className={classes.dateTypo} variant="h5">
                {getFormattedDate(
                  new Date(blogData[0].created_at),
                  'dd MM yyyy hh:ii'
                )}
              </Typography>
              {auth.role === 'admin' && (
                <>
                  <Link href={`/blog/edit/${blogData[0]._id}`}>
                    <Button className={classes.editButton} variant="contained">
                      EDIT
                    </Button>
                  </Link>
                  <Button
                    className={classes.deleteButton}
                    variant="contained"
                    onClick={() => setOpenValDeleteDialog(true)}
                  >
                    DELETE
                  </Button>
                </>
              )}
            </Grid>
            <Grid item container justifyContent="center" xs={12}>
              <Typography variant="h1" className={classes.titleTypo}>
                {blogData[0].title}
              </Typography>
            </Grid>
            <Grid xs={10} item container justifyContent="center">
              <Image
                src={blogData[0].imagepath || '/'}
                alt="Image suposed to be here"
                height={300}
                width={500}
              />
            </Grid>
          </Grid>
          <br />
          <Grid item container justifyContent="center">
            <Grid item xs={11}>
              <div
                dangerouslySetInnerHTML={{
                  __html: sanitizedBlogContent,
                }}
                className={classes.bodyDescription}
              />
            </Grid>
          </Grid>
          <Grid item container justifyContent="center">
            <CommentList />
          </Grid>
          <ValidationDialog
            title={`Delete Blog "${blogData[0].title}"`}
            description="Are you sure want to delete this blog? the action can not be undone!"
            open={openValDeleteDialog}
            onClose={() => setOpenValDeleteDialog(false)}
            onNo={() => setOpenValDeleteDialog(false)}
            onYes={() => {
              setOpenValDeleteDialog(false);
              submitDelete();
            }}
          />
        </Grid>
      ) : (
        <div>"Blog Not Found!"</div>
      )}
    </>
  );
}
