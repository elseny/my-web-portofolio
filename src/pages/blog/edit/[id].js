import { useEffect, useState, useCallback } from 'react';
import CustomEditor from '../../../components/Organism/CustomEditor/CustomEditor';
import DOMPurify from 'isomorphic-dompurify';
import { RETRIEVE_BLOG, EDIT_BLOG } from '../../../constants/action';
import { useDispatch, useSelector } from 'react-redux';
import { fetchData } from '../../../hooks/swr/fetchData';
import { useRouter } from 'next/router';
import { makePostRequest } from '../../../redux/slice/loadingSlice';
import { onSuccess, onSuccessFinish } from '../../../redux/slice/successSlice';
import { onError, onErrorFinish } from '../../../redux/slice/errorSlice';
import { RECAPTCHA_ERROR } from '../../../constants/error';
import { useGoogleReCaptcha } from 'react-google-recaptcha-v3';

export default function EditBlog(props) {
  const auth = useSelector((state) => state.auth);
  const router = useRouter();
  const blog = fetchData('/blog/retrieve', RETRIEVE_BLOG, {
    id: router.query.id,
  });
  const blogData = blog.data && blog.data.data.data[0];
  const dispatch = useDispatch();
  const { executeRecaptcha } = useGoogleReCaptcha();

  const handleReCaptchaVerify = useCallback(
    async (action) => {
      if (!executeRecaptcha) {
        return null;
      }
      const token = await executeRecaptcha(action);
      return token;
    },
    [executeRecaptcha]
  );

  const handleCreateSubmit = async (formData) => {
    console.log(formData);
    const recaptchaToken = await handleReCaptchaVerify(EDIT_BLOG);
    if (!recaptchaToken) {
      dispatch(onError(RECAPTCHA_ERROR));
      setTimeout(() => dispatch(onErrorFinish(RECAPTCHA_ERROR)), 6000);
      return;
    }

    dispatch(
      makePostRequest({
        url: '/admin/blog/edit',
        data: {
          title: formData.title,
          content: formData.content,
          imagepath: formData.mainImagepath,
          id: router.query.id,
          recaptcha_token: recaptchaToken,
        },
        action: EDIT_BLOG,
      })
    )
      .unwrap()
      .then((res) => {
        // handle result here if (res && res.data && res.data.status === 200) {
        if (res.status === 200) {
          // setComment(editedComment);
          // props.setSelectedEdit(null);
          dispatch(onSuccess(res.message));
          setTimeout(() => dispatch(onSuccessFinish(res.message)), 6000);
        } else {
          dispatch(onError(res.message));
          setTimeout(() => dispatch(onErrorFinish(res.message)), 6000);
        }
      })
      .catch((errorRes) => {
        dispatch(onError(errorRes.message));
        setTimeout(() => dispatch(onErrorFinish(errorRes.message)), 6000);
      });
  };

  const [title, setTitle] = useState('');
  const [mainImagepath, setMainImagepath] = useState('');
  const [content, setContent] = useState('');

  useEffect(() => {
    if (blog.data) {
      setTitle(blogData.title);
      setMainImagepath(blogData.imagepath);
      setContent(blogData.content);
    }
  }, [blog.data]);

  return auth.role === 'admin' && blog.data ? (
    <CustomEditor
      mainImagepath={mainImagepath}
      title={title}
      content={content}
      handleCreateSubmit={handleCreateSubmit}
      onTitleChange={(e) => setTitle(e.target.value)}
      onContentChange={(newContent) => setContent(newContent)}
      onMainImagepathChange={(imagepath) => setMainImagepath(imagepath)}
      mode="edit"
    />
  ) : (
    <div>UNAUTHORIZED!</div>
  );
}
