import { useCallback, useState, useEffect } from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Card from '../../components/Molecules/Card/Card';
import useBreakpoints from '../../helpers/breakpoints';
import Button from '@material-ui/core/Button';
import { useRouter } from 'next/router';
import DOMPurify from 'isomorphic-dompurify';
import { fetchInfiniteData } from '../../hooks/swr/fetchData';
import { useSelector, useDispatch } from 'react-redux';
import { onError, onErrorFinish } from '../../redux/slice/errorSlice';
import { RETRIEVE_BLOG } from '../../constants/action';
import Head from 'next/head';

const useStyles = makeStyles((theme) => ({
  mainHeaderTypo: {
    fontSize: '80px',
    fontFamily: 'Sorts Mill Goudy',
    color: '#FFFFFF',
    textAlign: 'center',
  },
  descriptionTypo: {
    fontSize: '30px',
    fontFamily: 'Sorts Mill Goudy',
    color: '#FFFFFF',
    textAlign: 'center',
  },
  cardMargin: {
    marginBottom: '30px',
  },
  showMoreButton: {
    color: 'white',
  },
}));

const Cards = ({ currentBreakpoint, perPage, blogRawData }) => {
  // const blogRawData = fetchData('/blog/retrieve', RETRIEVE_BLOG, {
  //   per_page: perPage,
  // });
  const classes = useStyles();
  if (!currentBreakpoint) {
    return <></>;
  }
  let cardCollections = [];

  const blogData = blogRawData.data.data.map((data) => {
    return {
      ...data,
      content: DOMPurify.sanitize(data.content, { FORBID_TAGS: ['img'] }),
    };
  });

  const breakpoints = ['xs', 'sm', 'md'];
  const incrementor = breakpoints.indexOf(currentBreakpoint) + 1;
  for (let i = 0; i < blogData.length; i += incrementor) {
    const dummyArray = new Array(incrementor);
    dummyArray.fill('');
    const theCards = dummyArray.map((val, idx) => {
      return (
        idx + i < blogData.length && (
          <Grid
            item
            container
            justifyContent="center"
            xs={12 / incrementor}
            key={blogData[i + idx]._id}
          >
            <Card blogData={blogData[i + idx]} />
          </Grid>
        )
      );
    });

    cardCollections.push(
      <Grid item container key={i} className={classes.cardMargin}>
        {theCards}
      </Grid>
    );
  }
  return <Grid container>{cardCollections}</Grid>;
};

export default function Blog() {
  const classes = useStyles();
  const router = useRouter();
  const dispatch = useDispatch();

  const auth = useSelector((state) => state.auth);

  const blogData = fetchInfiniteData(
    '/blog/retrieve',
    RETRIEVE_BLOG,
    {
      per_page: 6,
    },
    {
      maxReachedComment: 'No more blog!',
    }
  );

  const [currentBreakpoint, setCurrentBreakpoint] = useState();
  const mdUp = useBreakpoints('mdUp');
  const md = useBreakpoints('md');
  const sm = useBreakpoints('sm');
  const xs = useBreakpoints('xs');

  useEffect(() => {
    if (md) {
      setCurrentBreakpoint('md');
    } else if (sm) {
      setCurrentBreakpoint('sm');
    } else if (xs) {
      setCurrentBreakpoint('xs');
    } else {
      setCurrentBreakpoint('md');
    }
  }, [md, sm, xs, mdUp]);

  return (
    <>
      <Head>
        <title>Elsen's Blog Post</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>
      {auth.role === 'admin' && (
        <Button
          onClick={() => {
            router.push('/blog/create');
          }}
        >
          Create Blog
        </Button>
      )}
      <br />
      <br />
      <Grid container direction="column">
        <Grid item container justifyContent="center">
          <Typography
            variant="h1"
            color="initial"
            className={classes.mainHeaderTypo}
          >
            My Stories
          </Typography>
        </Grid>
        <br />
        <Grid item container justifyContent="center">
          <Grid item xs={10}>
            <Typography
              variant="h1"
              color="initial"
              className={classes.descriptionTypo}
            >
              These are some stories in my life that I want to share, feel free
              to read them
            </Typography>
          </Grid>
        </Grid>
        <br />
        <Grid item container direction="column">
          {blogData.isError && blogData.isLoading && !blogData.data ? (
            <></>
          ) : (
            blogData.data &&
            blogData.data.map((blogDataEach, idx) => {
              return (
                <Cards
                  key={`blogDataEach.data.last_page_id-${idx}`}
                  currentBreakpoint={currentBreakpoint}
                  blogRawData={blogDataEach}
                />
              );
            })
          )}
          {/* <Cards currentBreakpoint={currentBreakpoint} /> */}
        </Grid>
        <Grid item container justifyContent="center">
          <Button
            className={classes.showMoreButton}
            onClick={() => {
              if (!blogData.maxDataAchieved) {
                blogData.setSize(blogData.size + 1);
              } else {
                dispatch(onError('No more blog!'));
                setTimeout(() => {
                  dispatch(onErrorFinish('No more blog!'));
                }, 6000);
              }
            }}
          >
            Show more...
          </Button>
        </Grid>
      </Grid>
    </>
  );
}
