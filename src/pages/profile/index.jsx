import Image from 'next/image';
import Link from 'next/link';
import { ProfileImage } from '../../constants/profile-image';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import EditIcon from '@material-ui/icons/Edit';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import Button from '@material-ui/core/Button';
import { getFormattedDate } from '../../helpers/dateHelper';
import { useEffect, useState, useCallback } from 'react';
import TextField from '@material-ui/core/TextField';
import CheckIcon from '@material-ui/icons/Check';
import CloseIcon from '@material-ui/icons/Close';
import classNames from 'classnames';
import { fetchData, fetchInfiniteData } from '../../hooks/swr/fetchData';
import { useDispatch } from 'react-redux';
import { makePostRequest } from '../../redux/slice/loadingSlice';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import { onError, onErrorFinish } from '../../redux/slice/errorSlice';
import { onSuccess, onSuccessFinish } from '../../redux/slice/successSlice';
import { setClickedComment } from '../../redux/slice/blogCommentFormSlice';
import { EDIT_USER, EDIT_USER_IMAGE } from '../../constants/action';
import useBreakPoints from '../../helpers/breakpoints';
import { RECAPTCHA_ERROR } from '../../constants/error';
import { useGoogleReCaptcha } from 'react-google-recaptcha-v3';

const useStyles = makeStyles((theme) => ({
  credits: {
    fontWeight: 'bold',
    color: 'white',
  },
  showNextCommentIcon: {
    color: 'white',
  },
  selectedProfile: {
    border: '2px solid #bb0f10',
  },
  userName: {
    wordBreak: 'break-all',
    fontSize: '64px',
    color: '#2ad20f',
  },
  userEmail: {
    color: 'white',
  },
  iconButton: {
    color: '#bb0f10',
  },
  dialog: {
    backgroundColor: '#383838',
  },
  confirmButton: {
    color: '#2ad20f',
  },
  cancelButton: {
    color: '#df0000',
  },
  profileIcon: {
    marginTop: '-75px',
    marginLeft: '-30px',
  },
  yourCommentTypo: {
    fontWeight: 400,
    color: '#c9ced0',
  },
  commentContainer: {
    borderRadius: '8px',
    backgroundColor: '#9B9B9B',
    padding: '15px',
    margin: '10px',
    boxShadow: '30px 30px 50px 0px rgba(0,0,0,0.50)',
    wordBreak: 'break-word',
  },
  blogLink: {
    fontWeight: 'bold',
    cursor: 'pointer',
    '&:hover': {
      textDecoration: 'underline',
    },
  },
  dateTypo: {
    color: '#bb0f10',
  },
}));

// const DUMMY_COMMENTS = [
//   {
//     _id: '6153383dff3db5f70c6dfe9e',
//     comment:
//       'amsdmzxncamsdmzxncamsdmzxncamsdmzxncamsdmzxncamsdmzxncamsdmzxncamsdmzxncamsdmzxncamsdmzxncamsdmzxncamsdmzxncamsdmzxncamsdmzxncamsdmzxncamsdmzxncamsdmzxncamsdmzxncamsdmzxncamsdmzxncamsdmzxncamsdmzxncamsdmzxncamsdmzxncamsdmzxncamsdmzxncamsdmzxncamsdmzxncamsdmzxncamsdmzxncamsdmzxncamsdmzxncamsdmzxncamsdmzxncamsdmzxncamsdmzxncamsdmzxncamsdmzxncamsdmzxncamsdmzxncamsdmzxncamsdmzxncamsdmzxncamsdmzxncamsdmzxnc',
//     blog_id: 'tadf2',
//     user_id: '6151f2f3d07a41e7be0e74b2',
//     created_at: '2021-10-04T14:37:43.250Z',
//     updatedAt: '2021-10-04T14:37:43.250Z',
//     __v: 0,
//   },
//   {
//     _id: '615b11b7c1a96ba82751685a',
//     comment: 'AZNXCV',
//     blog_id: 'tadf1',
//     user_id: '6151f2f3d07a41e7be0e74b2',
//     created_at: '2021-10-04T14:37:43.250Z',
//     updatedAt: '2021-10-04T14:37:43.250Z',
//     __v: 0,
//   },
// ];

// const DUMMY_BLOGS = [
//   {
//     _id: 'tadf1',
//     title: 'my beautiful website',
//   },
//   {
//     _id: 'tadf2',
//     title: 'not so beautiful :(',
//   },
// ];

export default function Profile(props) {
  const [openProfileDialog, setOpenProfileDialog] = useState(false);

  return (
    <Grid container direction="column" alignItems="center">
      <br />
      <br />
      <ProfileDesc
        openProfileDialog={openProfileDialog}
        setOpenProfileDialog={setOpenProfileDialog}
      />
      <Grid item container xs={10}>
        <UserComments />
      </Grid>
    </Grid>
  );
}

const UserComments = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const userComments = fetchInfiniteData(
    `/blog-comment/retrieve-user-comment`,
    'FETCH_USER_COMMENTS',
    {},
    { maxReachedComment: 'No more comment' }
  );

  return (
    <Grid item container direction="column">
      <br />
      <br />
      <br />
      <br />
      <Typography variant="h2" className={classes.yourCommentTypo}>
        Your Comments
      </Typography>
      {userComments.data &&
        userComments.data.map((commentData) => {
          return commentData.data.data.map((comment, idx) => {
            return (
              <Grid
                item
                container
                direction="column"
                className={classes.commentContainer}
                key={comment._id}
              >
                <Typography variant="body2">
                  Commented on{' '}
                  <Link
                    href={{
                      pathname: `/blog/${comment.blog_id._id}`,
                      query: {
                        comment_id: comment._id,
                      },
                    }}
                    onClick={() =>
                      dispatch(setClickedComment({ comment: comment }))
                    }
                    passHref
                  >
                    <span className={classes.blogLink}>
                      {comment.blog_id.title}
                    </span>
                  </Link>{' '}
                  at{' '}
                  <span className={classes.dateTypo}>
                    {getFormattedDate(
                      new Date(comment.created_at),
                      'dd MM yyyy hh:ii'
                    )}{' '}
                  </span>
                </Typography>
                <br />
                <Typography variant="h6" color="initial">
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{comment.comment}
                </Typography>
                <br />
                <Grid item container justifyContent="flex-end">
                  <Typography variant="body2">
                    Last edited at{' '}
                    <span className={classes.dateTypo}>
                      {getFormattedDate(
                        new Date(comment.updatedAt),
                        'dd MM yyyy hh:ii'
                      )}
                    </span>
                  </Typography>
                </Grid>
              </Grid>
            );
          });
        })}
      <Grid item container justifyContent="center">
        <IconButton
          onClick={() => {
            if (!userComments.maxDataAchieved) {
              userComments.setSize(userComments.size + 1);
            } else {
              dispatch(onError('No more comment!'));
              setTimeout(() => {
                dispatch(onErrorFinish('No more comment!'));
              }, 6000);
            }
          }}
        >
          <KeyboardArrowDownIcon
            className={classes.showNextCommentIcon}
            fontSize="large"
          />
        </IconButton>
      </Grid>
    </Grid>
  );
};

const ProfileDesc = ({ openProfileDialog, setOpenProfileDialog }) => {
  const classes = useStyles();
  const dispatch = useDispatch();

  const mdUp = useBreakPoints('mdUp');

  const user = fetchData('/user/retrieve-detail', 'FETCH_USER');
  const userData = user.data && user.data.data.data[0];
  const [editNameMode, setEditNameMode] = useState(false);
  const [editedName, setEditedName] = useState(
    userData ? userData.name : 'loading...'
  );
  const [name, setName] = useState(userData ? userData.name : 'loading...');
  const [imagepath, setImagepath] = useState(userData ? userData.imagepath : 1);
  const [email, setEmail] = useState(userData ? userData.email : 'loading...');
  const { executeRecaptcha } = useGoogleReCaptcha();

  const handleReCaptchaVerify = useCallback(
    async (action) => {
      if (!executeRecaptcha) {
        return null;
      }
      const token = await executeRecaptcha(action);
      return token;
    },
    [executeRecaptcha]
  );

  useEffect(() => {
    if (userData) {
      setEditedName(userData.name);
      setName(userData.name);
      setImagepath(userData.imagepath);
      setEmail(userData.email);
    } else if (user.isError) {
      setEditedName('');
      setName('');
      setImagepath(10);
      setEmail('');
    } else {
      setEditedName('loading...');
      setName('loading...');
      setImagepath(1);
      setEmail('loading...');
    }
  }, [userData, user.isError]);

  const submitEditName = async () => {
    const recaptchaToken = await handleReCaptchaVerify(EDIT_USER);
    if (!recaptchaToken) {
      dispatch(onError(RECAPTCHA_ERROR));
      setTimeout(() => dispatch(onErrorFinish(RECAPTCHA_ERROR)), 6000);
      return;
    }

    dispatch(
      makePostRequest({
        url: '/user/edit',
        data: {
          name: editedName,
          recaptcha_token: recaptchaToken,
        },
        action: EDIT_USER,
      })
    )
      .unwrap()
      .then((res) => {
        // handle result here if (res && res.data && res.data.status === 200) {
        if (res.status === 200) {
          setName(editedName);
          dispatch(onSuccess(res.message));
          setTimeout(() => dispatch(onSuccessFinish(res.message)), 6000);
        } else {
          dispatch(onError(res.message));
          setTimeout(() => dispatch(onErrorFinish(res.message)), 6000);
        }
      })
      .catch((errorRes) => {
        dispatch(onError(errorRes.message));
        setTimeout(() => dispatch(onErrorFinish(errorRes.message)), 6000);
      });
  };

  const submitEditProfileImage = async (imagepath) => {
    const recaptchaToken = await handleReCaptchaVerify(EDIT_USER_IMAGE);
    if (!recaptchaToken) {
      dispatch(onError(RECAPTCHA_ERROR));
      setTimeout(() => dispatch(onErrorFinish(RECAPTCHA_ERROR)), 6000);
      return;
    }

    dispatch(
      makePostRequest({
        url: '/user/edit',
        data: {
          imagepath: imagepath + 1,
          recaptcha_token: recaptchaToken,
        },
        action: EDIT_USER_IMAGE,
      })
    )
      .unwrap()
      .then((res) => {
        // handle result here if (res && res.data && res.data.status === 200) {
        if (res.status === 200) {
          dispatch(onSuccess(res.message));
          setTimeout(() => dispatch(onSuccessFinish(res.message)), 6000);
          setImagepath(imagepath + 1);
        } else {
          dispatch(onError(res.message));
          setTimeout(() => dispatch(onErrorFinish(res.message)), 6000);
        }
      })
      .catch((errorRes) => {
        dispatch(onError(errorRes.message));
        setTimeout(() => dispatch(onErrorFinish(errorRes.message)), 6000);
      });
  };

  return (
    <>
      <Grid item container alignItems="center">
        <Grid item container direction="column" md={6} alignItems="center">
          <Grid item>
            <div>
              <Image
                src={`${ProfileImage[imagepath - 1].imagepath}` || '/'}
                alt="Profile Image"
                width={300}
                height={300}
              />
              <IconButton
                className={classNames(classes.iconButton, classes.profileIcon)}
                onClick={() => {
                  setOpenProfileDialog(true);
                }}
              >
                <EditIcon />
              </IconButton>
            </div>
          </Grid>
        </Grid>
        <Grid
          item
          md={6}
          container
          justifyContent={mdUp ? 'flex-start' : 'center'}
        >
          <Grid
            item
            container
            xs={11}
            alignItems="center"
            justifyContent={mdUp ? 'flex-start' : 'center'}
          >
            {editNameMode ? (
              <>
                <Grid item>
                  <TextField
                    value={editedName}
                    inputProps={{
                      className: classes.userName,
                    }}
                    onChange={(e) => setEditedName(e.target.value)}
                  />
                </Grid>
                <Grid item>
                  <IconButton
                    className={classes.iconButton}
                    onClick={() => {
                      submitEditName();
                      setEditNameMode(false);
                    }}
                  >
                    <CheckIcon />
                  </IconButton>
                  <IconButton
                    className={classes.iconButton}
                    onClick={() => {
                      setEditNameMode(false);
                    }}
                  >
                    <CloseIcon />
                  </IconButton>
                </Grid>
              </>
            ) : (
              <Typography color="initial" className={classes.userName}>
                {name}
                <IconButton
                  className={classes.iconButton}
                  onClick={() => {
                    setEditNameMode(true);
                  }}
                >
                  <EditIcon />
                </IconButton>
              </Typography>
            )}
          </Grid>
          <Grid
            item
            container
            md={11}
            justifyContent={mdUp ? 'flex-start' : 'center'}
          >
            <Typography
              variant="body1"
              color="initial"
              className={classes.userEmail}
            >
              {email}
            </Typography>
          </Grid>
        </Grid>
      </Grid>
      <ProfileDialog
        openProfileDialog={openProfileDialog}
        setOpenProfileDialog={setOpenProfileDialog}
        userData={userData}
        submitEditProfileImage={submitEditProfileImage}
      />
    </>
  );
};

const ProfileDialog = ({
  userData,
  openProfileDialog,
  setOpenProfileDialog,
  submitEditProfileImage,
}) => {
  const classes = useStyles();

  const [selectedProfile, setSelectedProfile] = useState(
    userData ? userData.imagepath - 1 : 1
  );

  return (
    <Dialog
      PaperProps={{
        className: classes.dialog,
      }}
      open={openProfileDialog}
      onClose={() => {
        setOpenProfileDialog(false);
        setSelectedProfile(userData ? userData.imagepath - 1 : 1);
      }}
    >
      <DialogTitle className={classes.credits}>Select new profile</DialogTitle>
      <DialogContent>
        <Grid container>
          {ProfileImage.map((image, idx) => {
            return (
              <Grid item xs={4} key={`profile-${image.imagepath}`}>
                <div
                  className={
                    idx === selectedProfile ? classes.selectedProfile : ''
                  }
                  onClick={() => setSelectedProfile(idx)}
                >
                  <Image
                    src={`${image.imagepath}` || '/'}
                    alt="Profile Image"
                    width={175}
                    height={175}
                  />
                </div>
              </Grid>
            );
          })}
        </Grid>
        <div className={classes.credits}>
          Icons made by{' '}
          <a
            href="https://www.flaticon.com/authors/darius-dan"
            title="Darius Dan"
          >
            Darius Dan
          </a>{' '}
          from{' '}
          <a href="https://www.flaticon.com/" title="Flaticon">
            www.flaticon.com
          </a>
        </div>
      </DialogContent>
      <DialogActions>
        <Button
          className={classes.confirmButton}
          onClick={() => {
            submitEditProfileImage(selectedProfile);
            setOpenProfileDialog(false);
          }}
          color="default"
        >
          Confirm
        </Button>
        <Button
          className={classes.cancelButton}
          onClick={() => {
            setOpenProfileDialog(false);
            setSelectedProfile(userData ? userData.imagepath - 1 : 1);
          }}
          color="default"
        >
          Cancel
        </Button>
      </DialogActions>
    </Dialog>
  );
};
