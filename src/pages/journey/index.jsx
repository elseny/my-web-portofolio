import { useState, useEffect, useCallback, useRef } from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Image from 'next/image';
import useBreakPoints from '../../helpers/breakpoints';
import Head from 'next/head';

const useStyles = makeStyles((theme) => ({
  headerText: {
    fontFamily: 'Rock Salt',
    color: '#FFFFFF',
    textAlign: 'center',
    fontSize: '64px',
  },
  header: {
    color: '#C4C4C4',
    fontFamily: 'Rokkitt',
    borderBottom: '10px solid #C4C4C4',
    width: 'fit-content',
    paddingLeft: '150px',
    transition: '0.5s',
  },
  instituteName: {
    color: '#FFFFFF',
    fontFamily: 'Rokkitt',
    fontSize: '35px',
    textAlign: (props) => !props.mdUp && 'center',
  },
  degree: {
    color: '#E22424',
    fontFamily: 'Rokkitt',
    fontSize: '22px',
    textAlign: (props) => !props.mdUp && 'center',
  },
  period: {
    color: '#21EB00',
    fontFamily: 'Rokkitt',
    fontSize: '22px',
    textAlign: (props) => !props.mdUp && 'center',
  },
  stickyTitle: {
    position: 'sticky',
    zIndex: 10,
    top: 0,
    paddingTop: '65px',
    transition: '0.5s',
  },
  testing: {
    top: '20px',
  },
  stickyDummy: {
    position: 'absolute',
  },
}));

const journeyItems = [
  {
    title: 'Study',
    items: [
      {
        name: 'Almasih Christian School',
        degree: 'Kindergarten',
        period: '2004 - 2005',
        image: {
          path: '/images/Almasih Logo.jpeg',
          width: '200px',
          height: '200px',
        },
      },
      {
        name: 'Almasih Christian School',
        degree: 'Elementary',
        period: '2006 - 2011',
        image: {
          path: '/images/Almasih Logo.jpeg',
          width: '200px',
          height: '200px',
        },
      },
      {
        name: 'Almasih Christian School',
        degree: 'Junior high',
        period: '2011 - 2014',
        image: {
          path: '/images/Almasih Logo.jpeg',
          width: '200px',
          height: '200px',
        },
      },
      {
        name: 'Ipeka Christian School',
        degree: 'High School',
        period: '2014 - 2017',
        image: {
          path: '/images/Ipeka Logo.png',
          width: '200px',
          height: '150px',
        },
      },
      {
        name: 'Binus University',
        degree: 'Computer Science Undegraduate Program',
        period: '2017 - 2021',
        image: {
          path: '/images/Binus Logo.png',
          width: '350px',
          height: '180px',
        },
      },
    ],
  },
  {
    title: 'Career',
    items: [
      {
        name: 'Astra Graphia Information Technology',
        degree: 'Front End Developer Intern',
        period: '2020 - 2021',
        image: {
          path: '/images/Agit Logo.png',
          width: '200px',
          height: '150px',
        },
      },
      {
        name: 'Five Jack',
        degree: 'Software Engineer',
        period: '2021 - now',
        image: {
          path: '/images/Five Jack Logo.png',
          width: '300px',
          height: '100px',
        },
      },
    ],
  },
];

export default function Journey() {
  const stickyNow = useRef(-1);

  const mdUp = useBreakPoints('mdUp');
  const classes = useStyles({ mdUp: mdUp });

  const isInViewport = useCallback((el) => {
    if (!el) {
      return;
    }
    const rect = el.getBoundingClientRect();
    return (
      rect.top >= 0 &&
      rect.left >= 0 &&
      rect.bottom <=
        (window.innerHeight || document.documentElement.clientHeight) &&
      rect.right <= (window.innerWidth || document.documentElement.clientWidth)
    );
  }, []);

  useEffect(() => {
    const onMouseScroll = () => {
      if (
        stickyNow.current !== -1 &&
        isInViewport(document.getElementById(`header-${stickyNow.current}`)) &&
        document.getElementById(`header-${stickyNow.current}`).offsetTop >
          document.getElementById(`container-${stickyNow.current}`).offsetTop
      ) {
        return;
      }

      for (let i = 0; i < journeyItems.length; i++) {
        const currHeader = document.getElementById(`header-${i}`);
        const currContainer = document.getElementById(`container-${i}`);
        const selectedChildren = currHeader.children[0];
        if (
          isInViewport(currHeader) &&
          currHeader.offsetTop > currContainer.offsetTop
        ) {
          stickyNow.current = i;
          currHeader = currHeader;
          currHeader.style.paddingLeft = `calc(100% - ${selectedChildren.offsetWidth}px - 15px)`;
          currHeader.style.backgroundColor = 'black';
          selectedChildren.style.borderBottom = 'none';
          break;
        } else {
          stickyNow.current = -1;
          currHeader.style.paddingLeft = 0;
          currHeader.style.width = '100%';
          currHeader.style.backgroundColor = 'transparent';
          selectedChildren.style.borderBottom = '10px solid #C4C4C4';
        }
      }
    };

    window.addEventListener('scroll', onMouseScroll);

    return () => window.removeEventListener('scroll', onMouseScroll);
  }, []);

  // old way, using intersection observer
  // useEffect(() => {
  //   let currSticky = -1;

  //   console.log(window.pageYOffset);
  //   console.log(document.getElementById('container-1-0').offsetTop);
  //   for (
  //     let i = 0;
  //     document.getElementById(`container-${i}-0`) &&
  //     document.getElementById(`container-${i}-0`).offsetTop <
  //       window.pageYOffset;
  //     i++
  //   ) {
  //     currSticky = i;
  //   }
  //   console.log(currSticky);
  //   if (currSticky !== -1) {
  //     setIsTitleSticky((state) => {
  //       const newState = { ...state };
  //       if (currSticky === 0) {
  //         newState[`header-0`] = 1;
  //       } else {
  //         newState[`header-${currSticky}`] = 2;
  //         document.getElementById(`header-${currSticky}`).style.paddingTop =
  //           '150px';
  //         document.getElementById(
  //           `header-${currSticky}`
  //         ).children[0].style.paddingLeft = 0;
  //       }

  //       return newState;
  //     });
  //   }

  //   const observer = new IntersectionObserver(
  //     ([e]) => {
  //       console.log(e.target.id.split('container-')[1].split('-')[0]);
  //       const el = document.getElementById(
  //         'header-' + e.target.id.split('container-')[1].split('-')[0]
  //       );
  //       setIsTitleSticky((state) => {
  //         console.log(e.target.id);

  //         const newState = { ...state };
  //         console.log(newState[el.id]);
  //         if (newState[el.id] === 0) {
  //           console.log(0);
  //           newState[el.id] = 1;
  //         } else if (newState[el.id] === 1) {
  //           console.log(1);
  //           el.style.paddingTop = '150px';
  //           el.children[0].style.paddingLeft = '0px';
  //           newState[el.id] = 2;
  //         } else if (newState[el.id] === 2) {
  //           console.log(2);
  //           el.style.top = '-50px';
  //           el.style.paddingTop = '50px';
  //           el.children[0].style.paddingLeft = '150px';
  //           newState[el.id] = 1;
  //         }
  //         console.log(newState);
  //         return newState;
  //       });
  //     },
  //     { threshold: [1] }
  //   );

  //   for (let i = 0; i < journeyItems.length; i++) {
  //     const stickyElm = document.getElementById(`container-${i}-0`);

  //     observer.observe(stickyElm);
  //   }

  //   return () => {
  //     for (let i = 0; i < journeyItems.length; i++) {
  //       const stickyElm = document.getElementById(`container-${i}-0`);

  //       observer.unobserve(stickyElm);
  //     }
  //   };
  // }, []);

  return (
    <>
      <Head>
        <title>Elsen's Journey</title>
        <meta name="description" content="My academic and career journey" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Grid container direction="column">
        <br />
        <Grid item>
          <Typography className={classes.headerText}>
            My Life Journey
          </Typography>
        </Grid>
        {journeyItems.map((item, idx) => {
          return (
            <Grid item container key={item.title} direction="column">
              <Grid item className={classes.stickyTitle} id={`header-${idx}`}>
                <Typography
                  variant="h3"
                  className={classes.header}
                  color="initial"
                >
                  {item.title}
                </Typography>
              </Grid>
              <Grid item container direction="column">
                {item.items.map((pos) => {
                  return (
                    <Grid
                      item
                      container
                      key={pos.period}
                      direction="column"
                      id={`container-${idx}`}
                    >
                      <br />
                      <br />
                      <Grid item container>
                        <Grid
                          item
                          container
                          justifyContent="center"
                          xs={12}
                          md={6}
                        >
                          <div>
                            <Image
                              src={pos.image.path || '/'}
                              alt="Elsen Yacub"
                              width={pos.image.width}
                              height={pos.image.height}
                            />
                          </div>
                        </Grid>
                        <Grid
                          item
                          container
                          justifyContent="center"
                          xs={12}
                          md={6}
                        >
                          <Grid
                            item
                            container
                            direction="column"
                            xs={10}
                            md={12}
                            justifyContent="center"
                            alignItems={mdUp ? 'flex-start' : 'center'}
                          >
                            <Grid item className={classes.instituteName}>
                              {pos.name}
                            </Grid>
                            <Grid item className={classes.degree}>
                              {pos.degree}
                            </Grid>
                            <Grid item className={classes.period}>
                              {pos.period}
                            </Grid>
                          </Grid>
                        </Grid>
                      </Grid>
                      <br />
                      <br />
                      <br />
                    </Grid>
                  );
                })}
              </Grid>
            </Grid>
          );
        })}
      </Grid>
    </>
  );
}
