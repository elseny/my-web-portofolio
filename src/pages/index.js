import Head from 'next/head';
import Image from 'next/image';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import { useRouter } from 'next/router';
import Link from 'next/link';
import MuiLink from '@material-ui/core/Link';
import { useSelector } from 'react-redux';
import useBreakpoints from '../helpers/breakpoints';

const useStyles = makeStyles((theme) => ({
  headerText: {
    fontFamily: 'Rock Salt',
    color: '#FFFFFF',
    textAlign: 'center',
    fontSize: '64px',
  },
  bodyText: {
    fontFamily: 'Rokkitt',
    fontSize: '60px',
    color: '#FFFFFF',
    textAlign: 'center',
    [theme.breakpoints.down('sm')]: {
      fontSize: '30px',
    },
  },
  menuContainer: {
    height: '100%',
    width: '100%',
    position: 'absolute',
  },
  menuLink: {
    color: '#383838',
    fontSize: '64px',
    fontFamily: 'Rokkitt',
    cursor: 'pointer',
    '&:hover': {
      color: '#FFFFFF',
    },
  },
  notSureTypo: {
    fontFamily: 'Sail',
    fontSize: '35px',
    color: '#FFFFFF',
  },
  tryRandomTypo: {
    fontFamily: 'Ruluko',
    fontSize: '25px',
    color: '#E70000',
    '&:hover': {
      textDecoration: 'underline',
    },
    cursor: 'pointer',
    [theme.breakpoints.down('sm')]: {
      fontSize: '15px',
    },
  },
  summaryTypo: {
    fontFamily: 'Ruluko',
    fontSize: '25px',
    color: '#1BC200',
    '&:hover': {
      textDecoration: 'underline',
    },
    cursor: 'pointer',
    [theme.breakpoints.down('sm')]: {
      fontSize: '15px',
    },
  },
  loginTypo: {
    fontFamily: 'Ruluko',
    fontSize: '25px',
    color: '#FFFF00',
    '&:hover': {
      textDecoration: 'underline',
    },
    cursor: 'pointer',
    [theme.breakpoints.down('sm')]: {
      fontSize: '15px',
    },
  },
}));

export default function Home() {
  const classes = useStyles();
  const router = useRouter();
  const smDown = useBreakpoints('smDown');
  const auth = useSelector((state) => state.auth);

  function getRandomInt(max) {
    return Math.floor(Math.random() * max);
  }

  const randomHandler = (e) => {
    e.preventDefault();
    const items = [
      'about',
      'blog',
      'projects',
      'contacts',
      'journey',
      'summary',
    ];
    const randomInt = getRandomInt(items.length);
    router.push(`/${items[randomInt]}`);
    return;
  };

  return (
    <div>
      <Head>
        <title>Elsen Yacub</title>
        <meta
          name="description"
          content="Landing Page of Elsen Yacub Website"
        />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Grid container justifyContent={smDown ? 'center' : 'flex-start'}>
        <Grid item>
          <Image
            src={'/favicon.ico' || '/'}
            alt="Elsen Yacub"
            width={204}
            height={102}
            layout="intrinsic"
          />
        </Grid>
      </Grid>
      <Grid container justifyContent="center">
        <Grid item container xs={10} justifyContent="center">
          <Typography className={classes.headerText}>Hi, I am Elsen</Typography>
        </Grid>
        {smDown && (
          <Grid item container xs={12}>
            <br style={{ border: '10px solid white', width: '100%' }} />
          </Grid>
        )}
        <Grid item container xs={10} justifyContent="center">
          <Typography className={classes.bodyText}>
            {'Welcome, and what are you looking for?'}
          </Typography>
        </Grid>
        <Grid item>
          <svg width="100vw" height="100%" viewBox="0 0 1440 242" fill="none">
            <path
              style={{ position: 'absolute' }}
              d="M53.0275 12.4025L-28.4082 62.4341C-46.1745 73.3491 -57 92.7053 -57 113.557V148.331C-57 192.342 -11.1892 221.377 28.6111 202.59L53.4793 190.852C68.1742 183.916 85.0541 183.237 100.259 188.97L197.699 225.709C209.411 230.126 222.212 230.768 234.307 227.547L366.907 192.235C376.124 189.78 385.794 189.559 395.115 191.589L617.304 239.976C626.433 241.965 635.901 241.793 644.953 239.476L822.192 194.098C836.287 190.49 851.214 192.126 864.193 198.702L913.084 223.475C924.646 229.334 937.794 231.291 950.561 229.053L1160 192.347C1174.16 189.865 1188.74 192.551 1201.09 199.916L1235 220.139C1248.3 228.071 1264.15 230.554 1279.24 227.069L1436.5 190.74C1463.72 184.453 1483 160.214 1483 132.28V72.1844C1483 33.997 1447.8 5.52627 1410.46 13.5105L1282.98 40.7645C1265.7 44.4602 1247.66 40.346 1233.69 29.5197L1214.26 14.4662C1200.74 3.99048 1183.39 -0.217175 1166.58 2.90048L952.211 42.6407C938.436 45.1945 924.201 42.8471 911.975 36.0054L865.967 10.2602C851.95 2.41622 835.373 0.520269 819.947 4.99667L653.262 53.365C643.402 56.226 632.973 56.5093 622.973 54.1878L397.016 1.73368C386.489 -0.710019 375.498 -0.265294 365.203 3.02086L246.947 40.7687C235.078 44.5571 222.325 44.5571 210.456 40.7687L102.681 6.3665C86.0402 1.05476 67.9109 3.25862 53.0275 12.4025Z"
              fill="#2AD70D"
            />
            <foreignObject x="0" y="0" width="100%" height="100%">
              <Grid
                className={classes.menuContainer}
                container
                alignItems="center"
                justifyContent="center"
              >
                <Grid item container justifyContent="center" xs={3}>
                  <Link href="/about" passHref>
                    <MuiLink underline="none" className={classes.menuLink}>
                      About Me
                    </MuiLink>
                  </Link>
                </Grid>
                <Grid item container justifyContent="center" xs={3}>
                  <Link href="/blog" passHref>
                    <MuiLink underline="none" className={classes.menuLink}>
                      Blog
                    </MuiLink>
                  </Link>
                </Grid>
                <Grid item container justifyContent="center" xs={3}>
                  <Link href="/projects" passHref>
                    <MuiLink underline="none" className={classes.menuLink}>
                      Projects
                    </MuiLink>
                  </Link>
                </Grid>
                <Grid item container justifyContent="center" xs={3}>
                  <Link href="/journey" passHref>
                    <MuiLink underline="none" className={classes.menuLink}>
                      Journey
                    </MuiLink>
                  </Link>
                </Grid>
              </Grid>
            </foreignObject>
          </svg>
        </Grid>
        <Grid item container direction="column">
          <Grid item container>
            <Grid item xs={1} />
            <Typography className={classes.notSureTypo}>
              not sure which one?
            </Typography>
          </Grid>
          <Grid item container>
            <Grid item xs={1} />
            <Grid item>
              <Typography
                onClick={(e) => randomHandler(e)}
                className={classes.tryRandomTypo}
              >
                Try Random
              </Typography>
            </Grid>
            <Grid item xs={1} />
            <Grid item>
              <Link href="/summary">
                <Typography className={classes.summaryTypo}>
                  Summary of All
                </Typography>
              </Link>
            </Grid>
            <Grid item xs={1} />
            {!auth.isLoggedIn ? (
              <Grid item>
                <Link href="/auth">
                  <Typography className={classes.loginTypo}>
                    Login First
                  </Typography>
                </Link>
              </Grid>
            ) : (
              <></>
            )}
          </Grid>
        </Grid>
      </Grid>
    </div>
  );
}

export async function getStaticProps(context) {
  return {
    props: {}, // will be passed to the page component as props
  };
}
