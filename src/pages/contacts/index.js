import { useCallback } from 'react';
import Grid from '@material-ui/core/Grid';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import TextareaAutosize from '@material-ui/core/TextareaAutosize';
import { makeStyles } from '@material-ui/core/styles';
import InstagramIcon from '@material-ui/icons/Instagram';
import LinkedInIcon from '@material-ui/icons/LinkedIn';
import GitHubIcon from '@material-ui/icons/GitHub';
import { Formik, Form, useField } from 'formik';
import * as Yup from 'yup';
import { makePostRequest } from '../../redux/slice/loadingSlice';
import { RECAPTCHA_ERROR } from '../../constants/error';
import { SEND_EMAIL_TO_DEV } from '../../constants/action';
import { useGoogleReCaptcha } from 'react-google-recaptcha-v3';
import { onError, onErrorFinish } from '../../redux/slice/errorSlice';
import { onSuccess, onSuccessFinish } from '../../redux/slice/successSlice';
import GooglePolicy from '../../components/Atom/GooglePolicy';
import { useDispatch } from 'react-redux';

const useStyles = makeStyles((theme) => ({
  headerTypo: {
    marginTop: '10vh',
    fontFamily: 'Sorts Mill Goudy',
    color: 'white',
  },
  body: {
    marginTop: '10vh',
    color: 'white',
  },
  socmedTypo: {
    color: 'white',
    marginLeft: '5px',
  },
  formTextField: {
    backgroundColor: '#C4C4C4',
    fontSize: '20px',
    padding: '10px',
    border: 'none',
  },
  formErrorText: {
    color: 'red',
    marginBottom: '15px',
  },
  formLabel: {
    fontFamily: 'Sorts Mill Goudy',
  },
  formContainer: {
    width: '90%',
  },
  submitButton: {
    margin: '10px 0px',
  },
}));

export default function Contacts(props) {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { executeRecaptcha } = useGoogleReCaptcha();

  const socmedClick = (socmed) => {
    switch (socmed) {
      case 'instagram':
        window.open('https://www.instagram.com/elsenyacub/');
        break;
      case 'linkedin':
        window.open('https://www.linkedin.com/in/elsen-yacub-4b712b1a3/');
        break;
      case 'git':
        window.open('https://gitlab.com/elseny');
        break;
    }
  };

  const TextInput = useCallback((props) => {
    // useField() returns [formik.getFieldProps(), formik.getFieldMeta()]
    // which we can spread on <input> and alse replace ErrorMessage entirely.
    const [field, meta] = useField(props);
    return (
      <>
        <label className={classes.formLabel}>{props.label}</label>
        {props.inputType === 'TextArea' ? (
          <TextareaAutosize
            maxRows={7}
            minRows={7}
            {...field}
            {...props}
            className={classes.formTextField}
            onChange={(e) => {
              field.onChange(e);
            }}
          />
        ) : (
          <input
            {...field}
            {...props}
            className={classes.formTextField}
            onChange={(e) => {
              field.onChange(e);
            }}
          />
        )}
        {meta.touched && meta.error ? (
          <div className={classes.formErrorText}>{meta.error}</div>
        ) : null}
      </>
    );
  }, []);

  const handleReCaptchaVerify = useCallback(
    async (action) => {
      if (!executeRecaptcha) {
        return null;
      }
      const token = await executeRecaptcha(action);
      return token;
    },
    [executeRecaptcha]
  );

  const sendEmail = async (formData, resetForm) => {
    const recaptchaToken = await handleReCaptchaVerify(SEND_EMAIL_TO_DEV);
    if (!recaptchaToken) {
      dispatch(onError(RECAPTCHA_ERROR));
      setTimeout(() => dispatch(onErrorFinish(RECAPTCHA_ERROR)), 6000);
      return;
    }

    dispatch(
      makePostRequest({
        url: '/mail/send-to-dev',
        data: {
          email: formData.email,
          subject: formData.subject,
          text: formData.text,
          recaptcha_token: recaptchaToken,
        },
        action: SEND_EMAIL_TO_DEV,
      })
    )
      .unwrap()
      .then((res) => {
        // handle result here if (res && res.data && res.data.status === 200) {
        if (res.status === 200) {
          dispatch(onSuccess(res.message));
          setTimeout(() => dispatch(onSuccessFinish(res.message)), 6000);
          resetForm();
        } else {
          dispatch(onError(res.message));
          setTimeout(() => dispatch(onErrorFinish(res.message)), 6000);
        }
      })
      .catch((errorRes) => {
        dispatch(onError(errorRes.message));
        setTimeout(() => dispatch(onErrorFinish(errorRes.message)), 6000);
      });
  };

  return (
    <Grid container direction="row">
      <Grid item container justifyContent="center">
        <Typography variant="h3" className={classes.headerTypo}>
          Need More Info?
        </Typography>
      </Grid>
      <Grid item container className={classes.body}>
        <Grid item container md={6} justifyContent="center">
          <Grid item container xs={10} md={8}>
            <Grid item container justifyContent="center">
              <Typography>Have something you want to talk to me?</Typography>
            </Grid>
            <Grid item xs={6} md={12}>
              <IconButton onClick={() => socmedClick('instagram')}>
                <InstagramIcon fontSize="large" />
                <Typography className={classes.socmedTypo}>
                  elsenyacub
                </Typography>
              </IconButton>
            </Grid>
            <Grid item xs={6} md={12}>
              <IconButton onClick={() => socmedClick('linkedin')}>
                <LinkedInIcon fontSize="large" />
                <Typography className={classes.socmedTypo}>
                  Elsen Yacub
                </Typography>
              </IconButton>
            </Grid>
            <Grid item xs={6} md={12}>
              <IconButton onClick={() => socmedClick('git')}>
                <GitHubIcon fontSize="large" />
                <Typography className={classes.socmedTypo}>elseny</Typography>
              </IconButton>
            </Grid>
          </Grid>
        </Grid>
        <Grid item container md={6} justifyContent="center">
          <Formik
            initialValues={{
              email: '',
              subject: '',
              text: '',
            }}
            validationSchema={Yup.object({
              email: Yup.string().required('Required').email(),
              subject: Yup.string().required('Required'),
              text: Yup.string().required('Required'),
            })}
            onSubmit={(values, funcs) => sendEmail(values, funcs.resetForm)}
          >
            {({ handleSubmit }) => (
              <Form
                onSubmit={(e) => {
                  e.preventDefault();
                  handleSubmit();
                }}
                className={classes.formContainer}
              >
                <Grid container direction="column">
                  <TextInput
                    name="email"
                    label="email"
                    variant="filled"
                    className={classes.mainTitleTextField}
                    InputLabelProps={{
                      classes: {
                        root: classes.titleLabel,
                        focused: classes.titleTextFieldFocused,
                      },
                    }}
                    InputProps={{
                      classes: {
                        root: classes.titleTextField,
                        focused: classes.titleTextFieldFocused,
                        notchedOutline: classes.titleNotchedOutline,
                      },
                      inputMode: 'numeric',
                    }}
                  />
                  <TextInput
                    name="subject"
                    label="subject"
                    variant="filled"
                    className={classes.mainTitleTextField}
                    InputLabelProps={{
                      classes: {
                        root: classes.titleLabel,
                        focused: classes.titleTextFieldFocused,
                      },
                    }}
                    InputProps={{
                      classes: {
                        root: classes.titleTextField,
                        focused: classes.titleTextFieldFocused,
                        notchedOutline: classes.titleNotchedOutline,
                      },
                      inputMode: 'numeric',
                    }}
                  />
                  <TextInput
                    name="text"
                    label="text"
                    variant="filled"
                    inputType="TextArea"
                    className={classes.mainTitleTextField}
                    InputLabelProps={{
                      classes: {
                        root: classes.titleLabel,
                        focused: classes.titleTextFieldFocused,
                      },
                    }}
                    InputProps={{
                      classes: {
                        root: classes.titleTextField,
                        focused: classes.titleTextFieldFocused,
                        notchedOutline: classes.titleNotchedOutline,
                      },
                      inputMode: 'numeric',
                    }}
                  />
                </Grid>
                <GooglePolicy />
                <Button
                  className={classes.submitButton}
                  type="submit"
                  variant="contained"
                >
                  Send email
                </Button>
              </Form>
            )}
          </Formik>
        </Grid>
      </Grid>
    </Grid>
  );
}
