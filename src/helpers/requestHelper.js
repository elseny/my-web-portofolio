import axios from 'axios';
import { useDispatch } from 'react-redux';
import { onLoading, onLoadingFinish } from '../redux/slice/loadingSlice';

export async function postRequest(url, data, action) {
  try {
    const res = await axios.post(url, data);
    return res;
  } catch (err) {
    console.log(err);
  }
}
