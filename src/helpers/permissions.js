import store from '../redux/store/store';
import { useSelector } from 'react-redux';

export const CheckPermission = (neededPermission) => {
  const stateData =
    useSelector((state) => state.auth.permissions) ||
    (process.browser &&
      JSON.parse(localStorage.getItem('persist:root') || null)?.auth
        .permissions);

  if (!stateData) {
    return false;
  }

  for (let i = 0; i < neededPermission.length; i++) {
    if (!stateData[neededPermission[i]]) {
      return false;
    }
  }

  return true;
};
