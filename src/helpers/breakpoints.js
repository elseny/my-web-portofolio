import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';

export default function useBreakPoints(breakPoint) {
  const theme = useTheme();
  switch (breakPoint) {
    case 'xs':
      return useMediaQuery(theme.breakpoints.only('xs'));
    case 'sm':
      return useMediaQuery(theme.breakpoints.only('sm'));
    case 'md':
      return useMediaQuery(theme.breakpoints.only('md'));
    case 'lg':
      return useMediaQuery(theme.breakpoints.only('lg'));
    case 'xl':
      return useMediaQuery(theme.breakpoints.only('xl'));
    case 'xxl':
      return useMediaQuery('(min-width:1920px)');
    case 'xxxl':
      return useMediaQuery('(min-width:2560px)');
    case 'xsUp':
      return useMediaQuery(theme.breakpoints.up('xs'));
    case 'smUp':
      return useMediaQuery(theme.breakpoints.up('sm'));
    case 'mdUp':
      return useMediaQuery(theme.breakpoints.up('md'));
    case 'lgUp':
      return useMediaQuery(theme.breakpoints.up('lg'));
    case 'xlUp':
      return useMediaQuery(theme.breakpoints.up('xl'));
    case 'xxlUp':
      return useMediaQuery('(min-width:1920px)');
    case 'xxxlUp':
      return useMediaQuery('(min-width:2560px)');
    case 'xsDown':
      return useMediaQuery(theme.breakpoints.down('xs'));
    case 'smDown':
      return useMediaQuery(theme.breakpoints.down('sm'));
    case 'mdDown':
      return useMediaQuery(theme.breakpoints.down('md'));
    case 'lgDown':
      return useMediaQuery(theme.breakpoints.down('lg'));
    case 'xlDown':
      return useMediaQuery(theme.breakpoints.down('xl'));
    case 'xxlDown':
      return useMediaQuery('(min-width:1920px)');
    case 'xxxlDown':
      return useMediaQuery('(min-width:2560px)');
    default:
      return;
  }
}
