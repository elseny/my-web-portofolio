import { createSlice } from '@reduxjs/toolkit';

export const addBlogSlice = createSlice({
  name: 'addBlog',
  initialState: {
    title: '',
    imagepath: '',
    content: '',
  },
  reducers: {
    onBlogContentChange: (state, action) => {
      state.content = action.payload.content;
    },
    onTitleChange: (state, action) => {
      state.title = action.payload.title;
    },
    onImagepathChange: (state, action) => {
      state.imagepath = action.payload.imagepath;
    },
  },
});

// Action creators are generated for each case reducer function
export const { onBlogContentChange, onTitleChange, onImagepathChange } =
  addBlogSlice.actions;

export default addBlogSlice.reducer;
