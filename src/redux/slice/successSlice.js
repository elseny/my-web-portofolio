import { createSlice } from '@reduxjs/toolkit';

export const SuccessSlice = createSlice({
  name: 'success',
  initialState: {
    isSuccess: false,
    message: {},
  },
  reducers: {
    onSuccess: (state, action) => {
      state.isSuccess = true;
      state.message = { ...state.message, [action.payload]: true };
    },
    onSuccessFinish: (state, action) => {
      delete state.message[action.payload];
      Object.keys(state.message).length <= 0
        ? (state.isSuccess = false)
        : (state.isSuccess = true);
    },
    clearSuccess: (state) => {
      state.isSuccess = false;
      state.message = {};
    },
  },
});

// Action creators are generated for each case reducer function
export const { onSuccess, onSuccessFinish, clearSuccess } =
  SuccessSlice.actions;

export default SuccessSlice.reducer;
