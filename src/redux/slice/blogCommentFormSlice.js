import { createSlice } from '@reduxjs/toolkit';
import { useSelector } from 'react-redux';

export const BlogCommentFormSlice = createSlice({
  name: 'blogCommentForm',
  initialState: {
    userName: '',
    comment: {},
    clickedComment: {},
  },
  reducers: {
    onBlogCommentChange: (state, action) => {
      state.comment[action.payload.blogId] = action.payload.comment;
    },
    onBlogUserNameChange: (state, action) => {
      state.userName = action.payload.userName;
    },
    setClickedComment: (state, action) => {
      state.clickedComment = action.payload.comment;
    },
  },
});

// Action creators are generated for each case reducer function
export const { onBlogCommentChange, onBlogUserNameChange, setClickedComment } =
  BlogCommentFormSlice.actions;

export default BlogCommentFormSlice.reducer;
