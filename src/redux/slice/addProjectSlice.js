import { createSlice } from '@reduxjs/toolkit';

export const addProjectSlice = createSlice({
  name: 'addProject',
  initialState: {
    title: '',
    imagepath: '',
    content: '',
  },
  reducers: {
    onProjectContentChange: (state, action) => {
      state.content = action.payload.content;
    },
    onTitleChange: (state, action) => {
      state.title = action.payload.title;
    },
    onImagepathChange: (state, action) => {
      state.imagepath = action.payload.imagepath;
    },
  },
});

// Action creators are generated for each case reducer function
export const { onProjectContentChange, onTitleChange, onImagepathChange } =
  addProjectSlice.actions;

export default addProjectSlice.reducer;
