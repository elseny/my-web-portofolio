import { createSlice } from '@reduxjs/toolkit';
import {
  CREATE_BLOG,
  EDIT_BLOG,
  CREATE_PROJECT,
  EDIT_PROJECT,
} from '../../constants/permissions';
import axios from 'axios';

export const authSlice = createSlice({
  name: 'login',
  initialState: {
    isLoggedIn: false,
    token: null,
    userName: '',
    role: '',
  },
  reducers: {
    login: (state, action) => {
      // Redux Toolkit allows us to write "mutating" logic in reducers. It
      // doesn't actually mutate the state because it uses the Immer library,
      // which detects changes to a "draft state" and produces a brand new
      // immutable state based off those changes
      const userData = action.payload;
      state.isLoggedIn = true;
      state.userName = userData.name;
      state.userId = userData.userId;
      state.role = userData.role;
      state.token = userData.token;
    },
    logout: (state) => {
      state.isLoggedIn = false;
      state.userName = '';
      state.userId = '';
      state.role = '';
      state.token = '';
      axios.defaults.headers.common['Authorization'] = '';
    },
  },
});

// Action creators are generated for each case reducer function
export const { login, logout } = authSlice.actions;
export default authSlice.reducer;
