import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import axios from 'axios';

export const makePostRequest = createAsyncThunk(
  'post',
  async (reqData, thunkAPI) => {
    try {
      const response = await axios.post(reqData.url, reqData.data);
      return response.data;
    } catch (err) {
      return thunkAPI.rejectWithValue(err.response);
    }
  }
);

export const LoadingSlice = createSlice({
  name: 'loading',
  initialState: {
    isLoading: false,
    action: {},
  },
  reducers: {
    onLoading: (state, action) => {
      state.isLoading = true;
      state.action = { ...state.action, [action.payload]: true };
    },
    onLoadingFinish: (state, action) => {
      state.isLoading = false;
      delete state.action[action.payload];
      Object.keys(state.action).length <= 0
        ? (state.isLoading = false)
        : (state.isLoading = true);
    },
  },
  extraReducers: (builder) => {
    // Add reducers for additional action types here, and handle loading state as needed
    builder.addCase(makePostRequest.pending, (state, action) => {
      // Add user to the state array
      if (action.meta.arg.action) {
        state.isLoading = true;
        state.action = { ...state.action, [action.meta.arg.action]: true };
      }
    });
    builder.addCase(makePostRequest.fulfilled, (state, action) => {
      // Add user to the state array
      state.isLoading = false;
      delete state.action[action.meta.arg.action];
      Object.keys(state.action).length <= 0
        ? (state.isLoading = false)
        : (state.isLoading = true);

      action.meta.arg.onSuccess && action.meta.arg.onSuccess();
    });
    builder.addCase(makePostRequest.rejected, (state, action) => {
      // Add user to the state array
      state.isLoading = false;
      delete state.action[action.meta.arg.action];
      Object.keys(state.action).length <= 0
        ? (state.isLoading = false)
        : (state.isLoading = true);

      action.meta.arg.onError && action.meta.arg.onError();
    });
  },
});

// Action creators are generated for each case reducer function
export const { onLoading, onLoadingFinish } = LoadingSlice.actions;

export default LoadingSlice.reducer;
