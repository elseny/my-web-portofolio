import { createSlice } from '@reduxjs/toolkit';

export const ErrorSlice = createSlice({
  name: 'error',
  initialState: {
    isError: false,
    message: {},
  },
  reducers: {
    onError: (state, action) => {
      state.isError = true;
      state.message = { ...state.message, [action.payload]: true };
    },
    onErrorFinish: (state, action) => {
      delete state.message[action.payload];
      Object.keys(state.message).length <= 0
        ? (state.isError = false)
        : (state.isError = true);
    },
    clearError: (state) => {
      state.isError = false;
      state.message = {};
    },
  },
});

// Action creators are generated for each case reducer function
export const { onError, onErrorFinish, clearError } = ErrorSlice.actions;

export default ErrorSlice.reducer;
