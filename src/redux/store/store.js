import { configureStore } from '@reduxjs/toolkit';
import authSlice from '../slice/authSlice';
import blogCommentFormSlice from '../slice/blogCommentFormSlice';
import addBlogSlice from '../slice/addBlogSlice';
import addProjectSlice from '../slice/addProjectSlice';
import loadingSlice from '../slice/loadingSlice';
import errorSlice from '../slice/errorSlice';
import successSlice from '../slice/successSlice';
import { combineReducers } from 'redux';
import {
  persistReducer,
  FLUSH,
  REHYDRATE,
  PAUSE,
  PERSIST,
  PURGE,
  REGISTER,
} from 'redux-persist';
import storage from 'redux-persist/lib/storage'; // defaults to localStorage for web

const persistConfig = {
  key: 'root',
  storage,
  blacklist: ['loading', 'error', 'success'],
};

const rootReducer = combineReducers({
  auth: authSlice,
  blogCommentForm: blogCommentFormSlice,
  addBlog: addBlogSlice,
  addProject: addProjectSlice,
  loading: loadingSlice,
  error: errorSlice,
  success: successSlice,
});

const persistedReducer = persistReducer(persistConfig, rootReducer);

export default configureStore({
  reducer: persistedReducer,
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: {
        ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER],
      },
    }),
});
