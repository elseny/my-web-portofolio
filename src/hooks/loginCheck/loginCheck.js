import { useEffect, useRef } from 'react';
import { CHECK_LOGIN } from '../../constants/action';
import { onError, onErrorFinish } from '../../redux/slice/errorSlice';
import { makePostRequest } from '../../redux/slice/loadingSlice';
import { logout } from '../../redux/slice/authSlice';
import { useDispatch, useSelector } from 'react-redux';

export function loginCheck() {
  const dispatch = useDispatch();
  const auth = useSelector((state) => state.auth);
  const loginRef = useRef(true);

  useEffect(() => {
    if (auth.token && auth.token.length > 0) {
      dispatch(
        makePostRequest({
          url: '/user/check-login',
        })
      )
        .unwrap()
        .then((res) => {
          // handle result here if (res && res.data && res.data.status === 200) {
          if (res.status !== 200) {
            loginRef.current = false;
            dispatch(logout());
            dispatch(onError(res.message));
            setTimeout(() => dispatch(onErrorFinish(res.message)), 6000);
          }
        })
        .catch((errorRes) => {
          dispatch(onError(errorRes.message));
          setTimeout(() => dispatch(onErrorFinish(errorRes.message)), 6000);
        });
    } else {
      loginRef.current = false;
    }
  }, [auth]);

  return loginRef.current;
}
