import { useDispatch } from 'react-redux';

export default async function postRequest() {
  dispatch(onLoading('EDIT_USER_NAME'));
  const res = await postRequest('/user/edit', {
    name: editedName,
  });
  dispatch(onLoadingFinish('EDIT_USER_NAME'));
  if (res && res.data && res.data.status === 200) {
    dispatch(onSuccess(res.data.message));
    setTimeout(() => dispatch(onSuccessFinish(res.data.message)), 6000);
    setName(editedName);
  } else if (res) {
    dispatch(onError(res.data.message));
    setTimeout(() => dispatch(onErrorFinish(res.data.message)), 6000);
  } else {
    let connectionError = 'Something is wrong with the connection!';
    dispatch(onError(connectionError));

    setTimeout(() => dispatch(onErrorFinish(connectionError)), 6000);
  }
}
