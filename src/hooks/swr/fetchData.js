import { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { onLoading, onLoadingFinish } from '../../redux/slice/loadingSlice';
import { onError, onErrorFinish } from '../../redux/slice/errorSlice';
import useSWR from 'swr';
import useSWRInfinite from 'swr/infinite';
import axios from 'axios';

const fetcher = async (url) => {
  const res = await axios
    .get(url)
    .then((res) => res.data)
    .catch((err) => {
      throw err;
    });

  return res;
};

export function fetchData(url, action, queries) {
  let queryString = '?';

  if (queries) {
    for (const [key, value] of Object.entries(queries)) {
      if (value) {
        queryString += `${key}=${value}&`;
      }
    }
  }

  const { data, error, mutate } = useSWR(url + queryString, fetcher);
  const isLoading = !error && !data;
  const dispatch = useDispatch();
  const [errorState, setErrorState] = useState(error);

  useEffect(() => {
    if (isLoading) {
      dispatch(onLoading(action));
    } else {
      dispatch(onLoadingFinish(action));
    }
  }, [isLoading]);

  useEffect(() => {
    if (error) {
      dispatch(
        onError(error.response ? error.response.statusText : error.toString())
      );
      setErrorState(
        error.response ? error.response.statusText : error.toString()
      );
    } else {
      dispatch(onErrorFinish(errorState));
      setErrorState(error);
    }
  }, [error]);

  return {
    data: data,
    isLoading: isLoading,
    isError: error,
    mutate: mutate,
  };
}

export function fetchInfiniteData(url, action, queries = {}, options = {}) {
  const getKey = (pageIndex, previousPageData) => {
    let queryString = '?';

    const newQueries = { ...queries };
    // delete the initial last_page_id after on the >1st fetch
    if (previousPageData && queries.last_page_id) {
      delete newQueries.last_page_id;
      delete newQueries.include_last_page;
    }

    if (newQueries) {
      for (const [key, value] of Object.entries(newQueries)) {
        if (value) {
          queryString += `${key}=${value}&`;
        }
      }
    }

    if (previousPageData && !previousPageData.data.data.length) {
      return null;
    }

    let lastPageId = null;
    if (previousPageData) {
      lastPageId =
        previousPageData.data.data[previousPageData.data.data.length - 1]._id;
    }

    const finalUrlString = !previousPageData
      ? url + queryString // to retrieve initial data, sometimes using last_page_id too (case: blog comments)
      : `${url + queryString}last_page_id=${lastPageId}`;

    return finalUrlString;
  };

  const { data, error, mutate, size, setSize, isValidating } = useSWRInfinite(
    getKey,
    fetcher
  );
  const [maxDataAchieved, setMaxDataAchieved] = useState(false);
  const isLoading =
    (!error && !data) || (data && data.length < size && !maxDataAchieved);
  const dispatch = useDispatch();
  const [errorState, setErrorState] = useState(error);

  useEffect(() => {
    if (isLoading) {
      dispatch(onLoading(action));
    } else {
      dispatch(onLoadingFinish(action));
    }
  }, [isLoading]);

  useEffect(() => {
    if (error) {
      dispatch(
        onError(error.response ? error.response.statusText : error.toString())
      );
      setErrorState(
        error.response ? error.response.statusText : error.toString()
      );
    } else {
      dispatch(onErrorFinish(errorState));
      setErrorState(error);
    }
  }, [error]);

  useEffect(() => {
    if (data && !data[data.length - 1].data.data.length && size > 1) {
      setMaxDataAchieved(true);
      dispatch(onError(options.maxReachedComment));
      setTimeout(() => {
        dispatch(onErrorFinish(options.maxReachedComment));
      }, 6000);
    }
  }, [data && data.length]);

  return {
    data: data,
    isLoading: isLoading,
    isError: error,
    maxDataAchieved,
    mutate,
    setSize,
    size,
    isValidating,
    setMaxDataAchieved,
  };
}
