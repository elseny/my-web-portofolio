import React, { useState, useCallback } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Image from 'next/image';
import Grid from '@material-ui/core/Grid';
import Link from 'next/link';
import MuiLink from '@material-ui/core/Link';
import { useRouter } from 'next/router';
import useBreakpoints from '../helpers/breakpoints';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Drawer from '@material-ui/core/Drawer';
import Typography from '@material-ui/core/Typography';
import { useSelector, useDispatch } from 'react-redux';
import { logout } from '../redux/slice/authSlice';
import Button from '@material-ui/core/Button';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import { useSWRConfig } from 'swr';
import axios from 'axios';

const useStyles = makeStyles((theme) => ({
  container: {
    position: 'sticky',
    top: '0px',
    zIndex: '11',
  },
  menuButton: {
    color: '#1BC200',
    marginRight: theme.spacing(2),
  },
  linkTypo: {
    fontSize: '30px',
    color: '#383838',
    fontFamily: 'Rokkitt',
    fontWeight: 'bold',
    color: '#FFFFFF',
    cursor: 'pointer',
    '&:hover': {
      color: '#2AD70D',
    },
  },
  linkTypoActive: {
    fontSize: '30px',
    fontWeight: 'bold',
    color: '#383838',
    fontFamily: 'Rokkitt',
    color: '#2AD70D',
    cursor: 'pointer',
    '&:hover': {
      color: '#FFFFFF',
    },
  },
  drawerGrid: {
    padding: '30px',
    width: '200px',
    height: '100%',
    backgroundColor: '#383838',
    flexWrap: 'nowrap',
    overflow: 'auto',
  },
  loginContainer: {
    maxWidth: '100%',
  },
  loginTypo: {
    color: '#E70000',
    '&:hover': {
      color: '#2AD70D',
    },
  },
  loginSeparator: {
    border: '2px solid white',
    width: '100%',
  },
  logoLink: {
    cursor: 'pointer',
  },
  sideLoginTypo: {
    color: '#2AD70D',
    '&:hover': {
      opacity: 0.7,
    },
  },
  sideLogoutTypo: {
    color: '#E70000',
    '&:hover': {
      opacity: 0.7,
    },
  },
  helloTypo: {
    color: '#2AD70D',
  },
  backButtonStyle: {
    color: 'white',
  },
  sideBarMenuItem: {
    margin: '8px 0px 8px 0px',
  },
  profileTypo: {
    margin: '0px 0px 8px 0px',
    fontSize: '15px',
    color: 'white',
  },
}));

export default function NewAppBar() {
  const router = useRouter();
  const dispatch = useDispatch();
  const pathName = router.pathname;
  const classes = useStyles();
  const smDown = useBreakpoints('smDown');
  const auth = useSelector((state) => state.auth);
  const [openDrawer, setOpenDrawer] = useState(false);

  const toggleDrawer = useCallback((val) => {
    setOpenDrawer(val);
  });

  const menuRoute = [
    {
      pathname: '/about',
      text: 'About Me',
    },
    {
      pathname: '/blog',
      text: 'Blog',
    },
    {
      pathname: '/projects',
      text: 'Projects',
    },
    {
      pathname: '/journey',
      text: 'Journey',
    },
    {
      pathname: '/summary',
      text: 'Summary',
    },
  ];

  const sideMenuRoute = [
    {
      pathname: '/about',
      text: 'About Me',
    },
    {
      pathname: '/blog',
      text: 'Blog',
    },
    {
      pathname: '/projects',
      text: 'Projects',
    },
    {
      pathname: '/journey',
      text: 'Journey',
    },
    {
      pathname: '/summary',
      text: 'Summary',
    },
    {
      pathname: '/contacts',
      text: 'Contacts',
    },
  ];

  const menuItems = () => {
    return menuRoute.map((item) => {
      return (
        <Grid item key={item.pathname}>
          <Link href={item.pathname} passHref>
            <MuiLink
              underline="none"
              className={
                pathName.includes(item.pathname)
                  ? classes.linkTypoActive
                  : classes.linkTypo
              }
            >
              {item.text}
            </MuiLink>
          </Link>
        </Grid>
      );
    });
  };

  const sideBarMenu = () => {
    const { cache, mutate } = useSWRConfig();
    return (
      <>
        {sideMenuRoute.map((item) => {
          return (
            <Grid item key={item.pathname} className={classes.sideBarMenuItem}>
              <Link href={item.pathname} passHref>
                <MuiLink
                  underline="none"
                  onClick={() => toggleDrawer(false)}
                  className={
                    pathName.includes(item.pathname)
                      ? classes.linkTypoActive
                      : classes.linkTypo
                  }
                >
                  {item.text}
                </MuiLink>
              </Link>
            </Grid>
          );
        })}
        <Grid item>
          {auth.isLoggedIn && (
            <Grid item>
              <Button
                variant="text"
                className={classes.sideLogoutTypo}
                onClick={() => {
                  toggleDrawer(false);
                  mutate('/user/retrieve-detail', {}, false);
                  dispatch(logout());
                }}
              >
                Logout
              </Button>
            </Grid>
          )}
        </Grid>
      </>
    );
  };

  return (
    <Grid container className={classes.container}>
      <AppBar
        position="static"
        style={{
          backgroundColor: '#202020',
        }}
      >
        <Toolbar>
          <Grid container>
            <Grid item container xs={7} md={2}>
              {smDown && (
                <Grid item>
                  <IconButton
                    onClick={() => {
                      router.back();
                    }}
                  >
                    <ArrowBackIcon className={classes.backButtonStyle} />
                  </IconButton>
                </Grid>
              )}
              <Grid item>
                <Link href="/">
                  <div className={classes.logoLink}>
                    <Image
                      src={'/favicon.ico' || '/'}
                      alt="Elsen Yacub Icon"
                      width={102}
                      height={51}
                    />
                  </div>
                </Link>
              </Grid>
            </Grid>
            <Grid item container xs={5} md={10} justifyContent="flex-end">
              {!smDown && (
                <Grid
                  item
                  container
                  direction="row"
                  alignItems="center"
                  sm={10}
                  justifyContent="space-around"
                >
                  {menuItems()}
                </Grid>
              )}
              <Grid item>
                <IconButton
                  className={classes.menuButton}
                  onClick={() => toggleDrawer(true)}
                >
                  <MenuIcon />
                </IconButton>
              </Grid>
            </Grid>
          </Grid>
        </Toolbar>
      </AppBar>
      <Drawer
        anchor="right"
        open={openDrawer}
        onClose={() => toggleDrawer(false)}
      >
        <Grid
          container
          direction="column"
          className={classes.drawerGrid}
          alignItems="flex-end"
        >
          <Grid
            item
            container
            justifyContent="flex-end"
            alignItems="flex-start"
            className={classes.loginContainer}
          >
            <Grid
              item
              container
              justifyContent="flex-end"
              alignItems="flex-start"
            >
              {auth.isLoggedIn ? (
                <>
                  <Typography variant="h5" className={classes.helloTypo}>
                    Hello, {auth.userName}
                  </Typography>
                  <Grid className={classes.profileTypo}>
                    <Link href={'/profile'} passHref>
                      <MuiLink
                        underline="none"
                        onClick={() => toggleDrawer(false)}
                        className={classes.profileTypo}
                      >
                        your profile
                      </MuiLink>
                    </Link>
                  </Grid>
                </>
              ) : (
                <Link
                  href="/auth"
                  className={classes.loginTypo}
                  underline="none"
                >
                  <Button
                    className={classes.sideLoginTypo}
                    onClick={() => toggleDrawer(false)}
                  >
                    Login
                  </Button>
                </Link>
              )}
            </Grid>
            <Grid item className={classes.loginSeparator}></Grid>
          </Grid>
          <Grid item container justifyContent="flex-end">
            {sideBarMenu()}
          </Grid>
        </Grid>
      </Drawer>
    </Grid>
  );
}
