import { useRouter } from 'next/router';
import FromMiddleComment from '../../Organism/FromMiddleComment/FromMiddleComment';
import DefaultComment from '../../Organism/DefaultComment/DefaultComment';
import CommentOwn from '../../Organism/CommentOwn/CommentOwn';
import { makeStyles } from '@material-ui/core/styles';
import { Grid, Typography, Button } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  commentHeaderText: {
    color: 'white',
    fontFamily: 'Sorts Mill Goudy',
    marginTop: '40px',
  },
  fadeAnimation: {
    animation: '$fadingHightlight 3s',
  },
  '@keyframes fadingHightlight': {
    from: {
      background: 'red',
      opacity: 0,
    },
    to: {
      opacity: 1,
    },
  },
  commentContainer: {
    backgroundColor: '#9B9B9B',
    padding: '15px',
    margin: '75px 0px 25px 0px',
    boxShadow: '30px 30px 50px 0px rgba(0,0,0,0.50)',
  },
}));

export default function CommentList() {
  const router = useRouter();
  const classes = useStyles();

  return (
    <>
      <Grid item>
        <Typography
          className={classes.commentHeaderText}
          variant="h2"
          color="initial"
        >
          Comments
        </Typography>
      </Grid>
      {router.query.comment_id ? (
        <FromMiddleComment
          url="/blog-comment/retrieve"
          blogId={router.query.id}
          commentId={router.query.comment_id}
          perPage={10}
        />
      ) : (
        <DefaultComment
          url="/blog-comment/retrieve"
          blogId={router.query.id}
          perPage={10}
        />
      )}
      <br />
      <br />
      <br />
      {/* {newCommentData.length ? (
        <Grid item container xs={7} className={classes.commentContainer}>
          {newCommentData.map((commentData) => {
            return (
              <Comment
                key={`new-comment-${commentData._id}`}
                commentData={commentData}
                onDeleteClick={() => setOpenDeleteDialog(true)}
              />
            );
          })}
        </Grid>
      ) : (
        <></>
      )} */}
      {/* <Grid item container xs={10}>
        <CommentOwn
          blogId={router.query.id}
          submitComment={(values) => {
            submitComment(values);
          }}
        />
      </Grid> */}
    </>
  );
}
