import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles((theme) => ({
  googlePolicy: {
    fontSize: '12px',
    color: 'white',
    marginBottom: '20px',
  },
  googleLink: {
    color: '#2196f3',
    fontWeight: 'bold',
  },
}));

export default function GooglePolicy() {
  const classes = useStyles();

  return (
    <Typography className={classes.googlePolicy}>
      This site is protected by reCAPTCHA and the Google&nbsp;
      <a
        href="https://policies.google.com/privacy"
        className={classes.googleLink}
      >
        Privacy Policy
      </a>{' '}
      and&nbsp;
      <a
        href="https://policies.google.com/terms"
        className={classes.googleLink}
      >
        Terms of Service
      </a>{' '}
      apply.
    </Typography>
  );
}
