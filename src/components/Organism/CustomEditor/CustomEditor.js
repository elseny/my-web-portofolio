import React, { useState, useRef, useCallback, useEffect } from 'react';
import dynamic from 'next/dynamic';
import { makeStyles } from '@material-ui/core/styles';
import { useSelector } from 'react-redux';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import { Formik, Form, useField } from 'formik';
import ImageUploadDialog from '../ImageUploadDialog/ImageUploadDialog';
import Image from 'next/image';
import DOMPurify from 'isomorphic-dompurify';
import * as Yup from 'yup';

const importJodit = () => import('jodit-react');
const JoditEditor = dynamic(importJodit, {
  ssr: false,
});

const useStyles = makeStyles(() => ({
  headerStyle: {
    color: 'white',
  },
  editorStyle: {
    backgroundColor: 'white',
    height: '400px',
    maxHeight: '400px',
  },
  titleTextField: {
    color: 'white',
    '&:hover $titleNotchedOutline': {
      border: '1px solid green',
    },
    '&$titleTextFieldFocused $titleNotchedOutline': {
      border: '1px solid #2ad70d',
    },
    '&$titleTextFieldFocused $titleLabel': {
      color: 'yellow',
    },
  },
  titleTextFieldFocused: {},
  titleNotchedOutline: {
    border: '1px solid white',
  },
  titleLabel: {
    color: 'white',
    '&$titleTextFieldFocused': {
      color: 'green',
    },
  },
  mainTitleTextField: {
    margin: '10px',
  },
  editorContainer: {
    margin: '20px',
  },
  imagePreviewContainer: {
    width: '50vw',
    margin: '15px 0',
  },
  mainImageButton: {
    backgroundColor: 'yellowgreen',
    '&:hover': {
      backgroundColor: 'yellowgreen',
    },
  },
  imagePreviewTypo: {
    textAlign: 'center',
    color: 'limegreen',
    fontStyle: 'italic',
    fontFamily: 'Sail',
    margin: '10px 0',
  },
  errorText: {
    color: 'red',
  },
  submitButton: {
    backgroundColor: '#2a70d',
    '&:hover': {
      backgroundColor: '#2a70d',
    },
  },
  headerTypo: {
    textAlign: 'center',
  },
}));

export default function CreateBlog(props) {
  const auth = useSelector((state) => state.auth);
  const config = {
    readonly: false, // all options from https://xdsoft.net/jodit/doc/
    style: {
      background: '#383838',
      color: '#d0d0d0',
    },
  };
  const configRef = useRef(config);
  const uploadImageButtonRef = useRef(0);
  const classes = useStyles();

  const [openImagepathDialog, setOpenImagepathDialog] = useState(false);

  const TextInput = useCallback((props) => {
    // useField() returns [formik.getFieldProps(), formik.getFieldMeta()]
    // which we can spread on <input> and alse replace ErrorMessage entirely.
    const [field, meta, helpers] = useField(props);

    useEffect(() => {
      helpers.setValue(props.value);
    }, [props.value]);

    return (
      <TextField
        {...field}
        name={props.name}
        InputProps={props.InputProps}
        InputLabelProps={props.InputLabelProps}
        value={props.value}
        onChange={(e) => {
          helpers.setValue(e.target.value);
          props.onChange(e);
        }}
        variant={props.variant}
        label={props.label}
        className={props.className}
        error={meta.touched && meta.error ? true : false}
        helperText={meta.touched && meta.error ? meta.error : ''}
      />
    );
  }, []);

  const ImageDialogButton = useCallback(
    (props) => {
      const [field, meta, helpers] = useField(props);
      if (
        uploadImageButtonRef.current !== 0 &&
        field &&
        field.value !== props.value
      ) {
        helpers.setValue(props.value);
      }
      uploadImageButtonRef.current++;
      return (
        <Button
          {...field}
          value={props.mainImagepath}
          onClick={() => {
            props.onClick(true);
          }}
          variant="contained"
          className={props.className}
          style={{ backgroundColor: meta.error && meta.touched ? 'red' : '' }}
        >
          Enter the main image's path
        </Button>
      );
    },
    [props.mainImagepath]
  );

  const JoditFormik = useCallback((props) => {
    const [field, meta, helpers] = useField(props);

    useEffect(() => {
      helpers.setValue(props.value);
    }, [props.value]);

    return (
      <div className={classes.editorContainer}>
        {meta.touched && meta.error ? (
          <div className={classes.errorText}>{meta.error}</div>
        ) : null}
        <JoditEditor
          {...field}
          onBlur={() => helpers.setTouched(true)}
          value={props.value || ''}
          config={props.config}
          tabIndex={props.tabIndex} // tabIndex of textarea
          onChange={(newContent) => {
            helpers.setValue(newContent);
            props.onChange(newContent);
          }}
        />
      </div>
    );
  }, []);

  const additionalInitVal = {};
  const additionalValidator = {};
  props.additionalProperties?.map((property) => {
    additionalInitVal[property.name] = props[property.name];
    additionalValidator[property.name] = property.validator;
  });

  return (
    <React.Fragment>
      <Grid container direction="column">
        <Grid item container justifyContent="center">
          <Typography variant="h2" className={classes.headerTypo}>
            Hello {auth.userName}, lets{' '}
            {props.mode === 'new'
              ? 'make a good post!'
              : `lets edit ${props.title} post`}
          </Typography>
        </Grid>
        <Formik
          initialValues={{
            title: props.title,
            mainImagepath: props.mainImagepath,
            content: props.content,
            ...additionalInitVal,
          }}
          validationSchema={Yup.object({
            title: Yup.string().required('Required'),
            mainImagepath: Yup.string().required('Required'),
            content: Yup.string().required('Required'),
            ...additionalValidator,
          })}
          onSubmit={(formData) => {
            props.handleCreateSubmit(formData);
          }}
        >
          {({ handleSubmit, resetForm }) => (
            <Form
              onSubmit={(e) => {
                e.preventDefault();
                handleSubmit();
              }}
            >
              <Grid container direction="column" alignItems="center">
                <TextInput
                  name="title"
                  label="Title"
                  variant="outlined"
                  value={props.title}
                  onChange={
                    (e) => props.onTitleChange(e)
                    //dispatch(onTitleChange({ title: e.target.value }))
                  }
                  className={classes.mainTitleTextField}
                  InputLabelProps={{
                    classes: {
                      root: classes.titleLabel,
                      focused: classes.titleTextFieldFocused,
                    },
                  }}
                  InputProps={{
                    classes: {
                      root: classes.titleTextField,
                      focused: classes.titleTextFieldFocused,
                      notchedOutline: classes.titleNotchedOutline,
                    },
                    inputMode: 'numeric',
                  }}
                />
                <ImageDialogButton
                  name="mainImagepath"
                  onClick={() => {
                    setOpenImagepathDialog(true);
                  }}
                  value={props.mainImagepath}
                  variant="contained"
                  className={classes.mainImageButton}
                >
                  Enter the main image's path
                </ImageDialogButton>
                {props.mainImagepath?.length ? (
                  <>
                    <Typography
                      variant="h6"
                      className={classes.imagePreviewTypo}
                    >
                      Image Preview of : {props.mainImagepath}
                    </Typography>

                    <Grid item className={classes.imagePreviewContainer}>
                      <Image
                        layout="responsive"
                        width="100%"
                        height="100%"
                        alt="Image supposed to be here"
                        src={props.mainImagepath || '/'}
                      />
                    </Grid>
                  </>
                ) : (
                  <></>
                )}
                {props.additionalProperties ? (
                  props.additionalProperties.map((property) => {
                    return (
                      <TextInput
                        key={property.name}
                        name={property.name}
                        label={property.label}
                        variant="outlined"
                        value={props[property.name]}
                        onChange={(e) => property.onChange(e)}
                        className={classes.mainTitleTextField}
                        InputLabelProps={{
                          classes: {
                            root: classes.titleLabel,
                            focused: classes.titleTextFieldFocused,
                          },
                        }}
                      />
                    );
                  })
                ) : (
                  <></>
                )}
                {/* {props.fromProject && (
                  <TextInput
                    name="gitUrl"
                    label="Git Url"
                    variant="outlined"
                    value={props.gitUrl}
                    onChange={(e) => props.additionalProperties(e)}
                    className={classes.mainTitleTextField}
                    InputLabelProps={{
                      classes: {
                        root: classes.titleLabel,
                        focused: classes.titleTextFieldFocused,
                      },
                    }}
                  />
                )} */}
              </Grid>
              <JoditFormik
                name="content"
                value={props.content}
                config={configRef.current}
                tabIndex={1} // tabIndex of textarea
                onChange={(newContent) => {
                  props.onContentChange(newContent);
                  // dispatch(onBlogContentChange({ content: newContent }));
                }}
              />
              <Grid item container justifyContent="center">
                <Button
                  variant="contained"
                  type="submit"
                  className={classes.submitButton}
                >
                  Submit
                </Button>
              </Grid>
            </Form>
          )}
        </Formik>
      </Grid>
      <ImageUploadDialog
        open={openImagepathDialog}
        onClose={() => setOpenImagepathDialog(false)}
        imagepath={props.mainImagepath}
        onImagepathChange={
          (imagepath) => props.onMainImagepathChange(imagepath)
          // dispatch(onImagepathChange({ imagepath: imagepath }))
        }
      />
    </React.Fragment>
  );
}
