import { useState } from 'react';
import Grid from '@material-ui/core/Grid';
import FormControl from '@material-ui/core/FormControl';
import Button from '@material-ui/core/Button';
import TextareaAutosize from '@material-ui/core/TextareaAutosize';
import { useRouter } from 'next/router';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import { Formik, Form, useField } from 'formik';
import { useSelector, useDispatch } from 'react-redux';
import {
  onBlogCommentChange,
  onBlogUserNameChange,
} from '../../../redux/slice/blogCommentFormSlice';
import ValidationDialog from '../../Organism/ConfirmDialog/ConfirmDialog';
import GooglePolicy from '../../Atom/GooglePolicy';
import * as Yup from 'yup';

const useStyles = makeStyles((theme) => ({
  textArea: {
    width: '100%',
    backgroundColor: '#e4e4e4',
    resize: 'none',
    fontSize: '20px',
    border: '2px solid #d0d0d0',
  },
  clearButton: {
    backgroundColor: '#E10D00',
    borderRadius: 0,
    boxShadow: '30px 30px 50px 0px rgba(0,0,0,0.50)',
    color: 'white',
    '&:hover': {
      backgroundColor: '#E93000',
    },
  },
  submitButton: {
    backgroundColor: '#2AD70D',
    boxShadow: '30px 30px 50px 0px rgba(0,0,0,0.50)',
    borderRadius: 0,
    '&:hover': {
      backgroundColor: '#2AD10A',
    },
  },
  formTitle: { color: 'white' },
  errorText: {
    color: 'red',
  },
  postAsTypo: {
    color: '#E70000',
  },
  noteTypo: {
    color: '#FFFF00',
  },
  commentFormMargin: {
    marginBottom: '20px',
  },
}));

export default function CommentOwn(props) {
  const dispatch = useDispatch();
  const router = useRouter();
  const blogId = router.query.id;
  const auth = useSelector((state) => state.auth);
  const [openPostCommentDialog, setOpenPostCommentDialog] = useState(false);
  const [formData, setFormData] = useState({});

  const TextInput = ({ label, ...props }) => {
    // useField() returns [formik.getFieldProps(), formik.getFieldMeta()]
    // which we can spread on <input> and alse replace ErrorMessage entirely.
    const [field, meta] = useField(props);
    return (
      <>
        <label htmlFor={props.id || props.name} className={classes.postAsTypo}>
          {label}
        </label>
        <input
          {...field}
          {...props}
          onChange={(e) => {
            dispatch(onBlogUserNameChange({ userName: e.target.value }));
            field.onChange(e);
          }}
        />
        {meta.touched && meta.error ? (
          <div className={classes.errorText}>{meta.error}</div>
        ) : null}
      </>
    );
  };

  const CommentInput = ({ ...props }) => {
    const classes = useStyles();
    const [field, meta] = useField(props);
    return (
      <>
        <TextareaAutosize
          maxRows={7}
          minRows={7}
          className={props.className}
          {...field}
          onChange={(e) => {
            dispatch(
              onBlogCommentChange({
                blogId: blogId,
                comment: e.target.value,
              })
            );
            field.onChange(e);
          }}
        />
        {meta.touched && meta.error ? (
          <div className={classes.errorText}>{meta.error}</div>
        ) : null}
      </>
    );
  };

  const onSubmit = (values) => {
    if (auth.isLoggedIn || openPostCommentDialog === true) {
      props.submitComment(values);
      setOpenPostCommentDialog(false);
    } else {
      setFormData(values);
      setOpenPostCommentDialog(true);
    }
  };

  const authSchema = {
    comment: Yup.string()
      .max(200, 'comment can only have 200 characters max')
      .required('Required'),
  };

  const unAuthSchema = {
    name: Yup.string()
      .max(30, 'Name can only be 30 characters maximum')
      .required('Required'),
    comment: Yup.string()
      .max(200, 'comment can only have 200 characters max')
      .required('Required'),
  };

  const AddCommentForm = (props) => {
    return (
      <Formik
        initialValues={{
          name: useSelector((state) => state.blogCommentForm.userName) || '',
          comment:
            useSelector(
              (state) =>
                state.blogCommentForm.comment[blogId] &&
                state.blogCommentForm.comment[blogId]
            ) || '',
        }}
        validationSchema={Yup.object(
          auth.isLoggedIn ? authSchema : unAuthSchema
        )}
        onSubmit={(values, actions) => {
          onSubmit(values);
        }}
      >
        {({ handleSubmit, setFieldValue }) => (
          <Form
            // role="form"
            onSubmit={(e) => {
              handleSubmit();
              e.preventDefault();
            }}
          >
            <Typography className={classes.noteTypo}>
              Note : Comment can only has maximum 200 characters
            </Typography>
            {auth.isLoggedIn ? (
              <>
                <Typography className={classes.postAsTypo}>
                  Post as {auth.userName}
                </Typography>
              </>
            ) : (
              <TextInput
                label="Your Name "
                name="name"
                type="text"
                placeholder="Jane"
              />
            )}
            <CommentInput
              label="Last Name"
              name="comment"
              type="text"
              className={props.textAreaStyle}
              placeholder="Doe"
            />
            <GooglePolicy />
            <Grid item container justifyContent="flex-end">
              <Button
                type="submit"
                variant="contained"
                className={props.submitButtonStyle}
              >
                Submit
              </Button>
              <Grid item xs={1} />
              <Button
                variant="contained"
                className={props.clearButtonStyle}
                onClick={(e) => {
                  dispatch(onBlogCommentChange({ comment: '' }));
                  setFieldValue('comment', '');
                }}
              >
                Clear
              </Button>
            </Grid>
          </Form>
        )}
      </Formik>
    );
  };

  const classes = useStyles();

  return (
    <Grid container>
      <Grid item container direction="column" xs={12}>
        <FormControl>
          <Typography variant="h3" className={classes.formTitle}>
            Any Comment?
          </Typography>
          <Grid item className={classes.commentFormMargin}>
            <AddCommentForm
              onSubmit={(e) => {
                e.preventDefault();
              }}
              textAreaStyle={classes.textArea}
              submitButtonStyle={classes.submitButton}
              clearButtonStyle={classes.clearButton}
            />
          </Grid>
        </FormControl>
      </Grid>
      <ValidationDialog
        open={openPostCommentDialog}
        onClose={() => setOpenPostCommentDialog(false)}
        title="Confirm comment post"
        description="You are not logged in, once you post this comment, you can't edit or delete it. continue?"
        onYes={() => {
          onSubmit(formData);
        }}
        onNo={() => setOpenPostCommentDialog(false)}
      />
    </Grid>
  );
}
