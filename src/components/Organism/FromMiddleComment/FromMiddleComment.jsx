import { useDispatch, useSelector } from 'react-redux';
import { fetchInfiniteData } from '../../../hooks/swr/fetchData';
import { makeStyles } from '@material-ui/core/styles';
import {
  RETRIEVE_BLOG_COMMENT,
  CREATE_BLOG_COMMENT,
  DELETE_BLOG_COMMENT,
} from '../../../constants/action';
import { onError, onErrorFinish } from '../../../redux/slice/errorSlice';
import { onSuccess, onSuccessFinish } from '../../../redux/slice/successSlice';
import { Grid, IconButton } from '@material-ui/core';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import Comment from '../../Molecules/Comment/Comment';
import NewComment from '../../Molecules/NewComment/NewComment';
import ValidationDialog from '../../Organism/ConfirmDialog/ConfirmDialog';
import CommentOwn from '../../Organism/CommentOwn/CommentOwn';
import { useEffect, useRef, useState, useCallback } from 'react';
import { useRouter } from 'next/router';
import { makePostRequest } from '../../../redux/slice/loadingSlice';
import { onBlogCommentChange } from '../../../redux/slice/blogCommentFormSlice';
import { RECAPTCHA_ERROR } from '../../../constants/error';
import { useGoogleReCaptcha } from 'react-google-recaptcha-v3';

const useStyles = makeStyles(() => ({
  commentContainer: {
    backgroundColor: '#9B9B9B',
    boxShadow: '30px 30px 50px 0px rgba(0,0,0,0.50)',
  },
  newCommentContainer: {
    padding: '15px',
    margin: '10px',
    backgroundColor: '#9B9B9B',
    boxShadow: '30px 30px 50px 0px rgba(0,0,0,0.50)',
  },
  showNextCommentIcon: {
    color: 'white',
    width: '100px',
    fontWeight: 'bold',
  },
}));

export default function FromMiddleComment({ url, blogId, commentId, perPage }) {
  const classes = useStyles();
  const dispatch = useDispatch();
  const newCommentRef = useRef(false);
  const [submitted, setSubmitted] = useState(false);
  const [newCommentData, setNewCommentData] = useState([]);
  const [openDeleteDialog, setOpenDeleteDialog] = useState(false);
  const [selectedCommentId, setSelectedCommentId] = useState(-1);
  const [selectedEdit, setSelectedEdit] = useState(null);
  const auth = useSelector((state) => state.auth);
  const router = useRouter();
  const { executeRecaptcha } = useGoogleReCaptcha();

  if (!commentId) {
    return <></>;
  }

  const handleReCaptchaVerify = useCallback(
    async (action) => {
      if (!executeRecaptcha) {
        return null;
      }
      const token = await executeRecaptcha(action);
      return token;
    },
    [executeRecaptcha]
  );

  const commentRawDataDown = fetchInfiniteData(
    url,
    RETRIEVE_BLOG_COMMENT,
    {
      blog_id: blogId,
      last_page_id: commentId,
      per_page: perPage,
    },
    {
      maxReachedComment: 'No more newest comment',
    }
  );

  const commentRawDataUp = fetchInfiniteData(
    url,
    RETRIEVE_BLOG_COMMENT,
    {
      blog_id: blogId,
      last_page_id: commentId,
      per_page: perPage,
      retrieve_direction: 'up',
      include_last_page: 1,
    },
    {
      maxReachedComment: 'No more older comment',
    }
  );

  useEffect(() => {
    if (
      !commentRawDataDown ||
      !commentRawDataDown.data ||
      !commentRawDataDown.data.length
    ) {
      return;
    }
    let newState = [...newCommentData];
    const commentRawIds = commentRawDataDown.data[
      commentRawDataDown.data.length - 1
    ].data?.data.map((commentData) => {
      return commentData._id;
    });

    newState = newCommentData.filter(
      (commentData) => !commentRawIds.includes(commentData._id)
    );
    setNewCommentData(newState);

    if (newState.length > 0) {
      commentRawDataDown.setMaxDataAchieved(false);
    }
  }, [
    commentRawDataDown.data &&
      commentRawDataDown.data[commentRawDataDown.data.length - 1].data?.data,
  ]);

  const submitComment = async (formData) => {
    //request submit comment here
    const recaptchaToken = await handleReCaptchaVerify(CREATE_BLOG_COMMENT);
    if (!recaptchaToken) {
      dispatch(onError(RECAPTCHA_ERROR));
      setTimeout(() => dispatch(onErrorFinish(RECAPTCHA_ERROR)), 6000);
      return;
    }

    dispatch(
      makePostRequest({
        url: '/blog-comment/create',
        data: {
          comment: formData.comment,
          blog_id: router.query.id,
          name: auth.token ? undefined : formData.name,
          recaptcha_token: recaptchaToken,
        },
        action: CREATE_BLOG_COMMENT,
      })
    )
      .unwrap()
      .then((res) => {
        // handle result here if (res && res.data && res.data.status === 200) {
        if (res.status === 200) {
          commentRawDataDown.mutate();
          setSubmitted(res.data._id);
          if (
            commentRawDataDown.data[commentRawDataDown.data.length - 1].data
              ?.data.length >= perPage
          ) {
            commentRawDataDown.setMaxDataAchieved(false);
            setNewCommentData((state) => {
              return [...state, res.data.data[0]];
            });
          }
          dispatch(onSuccess(res.message));
          setTimeout(() => dispatch(onSuccessFinish(res.message)), 6000);
          dispatch(
            onBlogCommentChange({ blogId: router.query.id, comment: '' })
          );
        } else {
          dispatch(onError(res.message));
          setTimeout(() => dispatch(onErrorFinish(res.message)), 6000);
        }
      })
      .catch((errorRes) => {
        dispatch(onError(errorRes.message));
        setTimeout(() => dispatch(onErrorFinish(errorRes.message)), 6000);
      });
  };

  const deleteComment = async (commentId) => {
    const recaptchaToken = await handleReCaptchaVerify(DELETE_BLOG_COMMENT);
    if (!recaptchaToken) {
      dispatch(onError(RECAPTCHA_ERROR));
      setTimeout(() => dispatch(onErrorFinish(RECAPTCHA_ERROR)), 6000);
      return;
    }

    dispatch(
      makePostRequest({
        url: '/blog-comment/delete',
        data: {
          id: commentId,
          recaptcha_token: recaptchaToken,
        },
        action: DELETE_BLOG_COMMENT,
      })
    )
      .unwrap()
      .then((res) => {
        // handle result here if (res && res.data && res.data.status === 200) {
        if (res.status === 200) {
          setNewCommentData((state) => {
            return [...state].filter((commentData) => {
              return commentData._id !== selectedCommentId;
            });
          });
          commentRawDataDown.mutate();
          commentRawDataUp.mutate();
          setSelectedCommentId(-1);
          setOpenDeleteDialog(false);
          dispatch(onSuccess(res.message));
          setTimeout(() => dispatch(onSuccessFinish(res.message)), 6000);
        } else {
          dispatch(onError(res.message));
          setTimeout(() => dispatch(onErrorFinish(res.message)), 6000);
        }
      })
      .catch((errorRes) => {
        dispatch(onError(errorRes.message));
        setTimeout(() => dispatch(onErrorFinish(errorRes.message)), 6000);
      });
  };

  return (
    <>
      {!commentRawDataUp.maxDataAchieved && (
        <Grid item xs={10} container justifyContent="center">
          <IconButton
            onClick={() => {
              if (!commentRawDataUp.maxDataAchieved) {
                commentRawDataUp.setSize(commentRawDataUp.size + 1);
              }
            }}
          >
            <KeyboardArrowUpIcon
              className={classes.showNextCommentIcon}
              fontSize="large"
            />
          </IconButton>
        </Grid>
      )}
      {!commentRawDataUp.data || !commentRawDataDown.data ? (
        <></>
      ) : (
        <>
          <Grid
            item
            xs={10}
            container
            direction="column"
            className={classes.commentContainer}
          >
            {[...commentRawDataUp.data].reverse().map((commentData) => {
              return [...commentData.data.data]
                .reverse()
                .map((comment, idx) => {
                  if (
                    comment._id === commentId &&
                    !newCommentRef.current &&
                    !commentRawDataUp.isValidating
                  ) {
                    return (
                      <NewComment
                        key={commentId}
                        commentData={comment}
                        ref={newCommentRef}
                        selectedEdit={selectedEdit}
                        setSelectedEdit={setSelectedEdit}
                        onDeleteClick={(id) => {
                          setSelectedCommentId(id);
                          setOpenDeleteDialog(true);
                        }}
                      />
                    );
                  }
                  return (
                    <Comment
                      key={comment._id}
                      commentData={comment}
                      onDeleteClick={(id) => {
                        setSelectedCommentId(id);
                        setOpenDeleteDialog(true);
                      }}
                      selectedEdit={selectedEdit}
                      setSelectedEdit={setSelectedEdit}
                    />
                  );
                });
            })}
          </Grid>
          <Grid
            item
            xs={10}
            container
            direction="column"
            className={classes.commentContainer}
          >
            {commentRawDataDown.data.map((commentData) => {
              return commentData.data.data.map((comment) => {
                return (
                  <Comment
                    key={comment._id}
                    commentData={comment}
                    selectedEdit={selectedEdit}
                    setSelectedEdit={setSelectedEdit}
                    onDeleteClick={(id) => {
                      setSelectedCommentId(id);
                      setOpenDeleteDialog(true);
                    }}
                  />
                );
              });
            })}
          </Grid>
        </>
      )}
      {!commentRawDataDown.maxDataAchieved && (
        <Grid item xs={10} container justifyContent="center">
          <IconButton
            onClick={() => {
              if (!commentRawDataDown.maxDataAchieved) {
                commentRawDataDown.setSize(commentRawDataDown.size + 1);
              }
            }}
          >
            <KeyboardArrowDownIcon
              className={classes.showNextCommentIcon}
              fontSize="large"
            />
          </IconButton>
        </Grid>
      )}
      {newCommentData.length ? (
        <Grid item container xs={7} className={classes.newCommentContainer}>
          {newCommentData.map((commentData) => {
            return (
              <Comment
                key={`new-comment-${commentData._id}`}
                commentData={commentData}
                onDeleteClick={(id) => {
                  setSelectedCommentId(id);
                  setOpenDeleteDialog(true);
                }}
                selectedEdit={selectedEdit}
                setSelectedEdit={setSelectedEdit}
              />
            );
          })}
        </Grid>
      ) : (
        <></>
      )}
      <Grid item container xs={10}>
        <CommentOwn
          blogId={router.query.id}
          submitComment={(values) => {
            submitComment(values);
          }}
        />
      </Grid>
      <ValidationDialog
        open={openDeleteDialog}
        onClose={() => setOpenDeleteDialog(false)}
        title="Confirm delete this comment"
        description="delete selected comment?"
        onYes={() => {
          deleteComment(selectedCommentId);
        }}
        onNo={() => setOpenDeleteDialog(false)}
      />
    </>
  );
}
