import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import DialogTitle from '@material-ui/core/DialogTitle';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import { blue } from '@material-ui/core/colors';

const useStyles = makeStyles({
  avatar: {
    backgroundColor: blue[100],
    color: blue[600],
  },
});

interface IValidationDialogProps {
  title: string;
  description: string;
  open: boolean;
  noText: string | null;
  yesText: string | null;
  onClose(): void;
  onNo(): void;
  onYes(): void;
}

export default function ValidationDialog(props: IValidationDialogProps) {
  return (
    <Dialog open={props.open} onClose={props.onClose}>
      <DialogTitle id="simple-dialog-title">{props.title}</DialogTitle>
      <DialogContent>
        <DialogContentText>{props.description}</DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={props.onNo}>{props.noText || 'No'}</Button>
        <Button onClick={props.onYes} color="primary">
          {props.yesText || 'Yes'}
        </Button>
      </DialogActions>
    </Dialog>
  );
}
