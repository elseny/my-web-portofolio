import { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogActions from '@material-ui/core/DialogContentText';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Image from 'next/image';
import Grid from '@material-ui/core/Grid';
import {
  PATH_NOT_SUPPORTED,
  IMAGE_NOT_FOUND,
  IMAGEPATH_IS_EMPTY,
} from '../../../constants/uploadImageError';

const useStyles = makeStyles((theme) => ({
  errorText: {
    color: 'red',
  },
  srcMargin: {
    marginBottom: '15px',
  },
  submitButton: {
    backgroundColor: 'green',
    '&:hover': {
      backgroundColor: 'green',
    },
    color: 'white',
    margin: '0px 10px',
  },
  cancelButton: {
    color: 'red',
    border: '1px solid red',
    margin: '0px 10px',
  },
  applyButton: {
    margin: '0px 10px',
  },
  actionsGrid: {
    marginTop: '10px',
  },
}));

export default function ImageUploadDialog(props) {
  const classes = useStyles();
  const [imagepath, setImagepath] = useState('');
  const [imageApplied, setImageApplied] = useState('');
  const [imageError, setImageError] = useState([]);

  const checkImagepath = (imagepath) => {
    setImageError([]);
    let validImage = false;
    let finalImagepath = imagepath;

    const allowedUrls = process.env.NEXT_PUBLIC_IMAGE_DOMAIN;

    for (let i = 0; i < allowedUrls.length; i++) {
      if (finalImagepath.includes(allowedUrls[i])) {
        validImage = true;
      }
    }

    if (!validImage) {
      setImageError((state) => {
        if (!state.includes(PATH_NOT_SUPPORTED)) {
          const newState = [...state];
          newState.push(PATH_NOT_SUPPORTED);
          return newState;
        }
        return state;
      });
      setImageApplied(false);
      return;
    }

    if (!finalImagepath.startsWith('https://')) {
      finalImagepath = 'https://' + finalImagepath;
      setImagepath(finalImagepath);
    }

    setImageError([]);
    setImageApplied(finalImagepath);
  };

  return (
    <Dialog
      fullScreen
      open={props.open}
      onClose={() => {
        props.onClose();
      }}
    >
      <DialogTitle>Input image</DialogTitle>
      <DialogContent>
        <DialogContentText>
          Input the imagepath for the main image
        </DialogContentText>
        {imageError.length ? (
          imageError.map((errText) => {
            return (
              <div className={classes.errorText} key={errText}>
                {errText}
              </div>
            );
          })
        ) : (
          <></>
        )}
        <TextField
          value={imagepath}
          onChange={(e) => setImagepath(e.target.value)}
          variant="outlined"
          fullWidth
          placeholder="https://niceImagepathHere.com/yourOnlyImage.jpg"
          className={classes.srcMargin}
        />
        {imageApplied.length > 0 ? (
          <Image
            layout="responsive"
            width="90vw"
            height="100vh"
            alt=""
            src={imageApplied || '/'}
            onError={(e) => {
              setImageError((state) => {
                if (!state.includes(IMAGE_NOT_FOUND)) {
                  const newState = [...state];
                  newState.push(IMAGE_NOT_FOUND);
                  return newState;
                }
                return state;
              });
              setImageApplied('https://i.imgur.com/OHkfjIZ.jpeg');
            }}
          />
        ) : (
          <></>
        )}
      </DialogContent>
      <DialogActions component={'span'} className={classes.dialogActions}>
        <Grid container justifyContent="center" className={classes.actionsGrid}>
          <Button
            onClick={() => {
              checkImagepath(imagepath);
            }}
            className={classes.applyButton}
          >
            Apply
          </Button>
          <Button
            onClick={() => {
              if (imageApplied) {
                props.onImagepathChange(imageApplied);
                props.onClose();
              }
            }}
            variant="contained"
            className={classes.submitButton}
            disabled={!imageApplied}
          >
            Submit
          </Button>
          <Button
            onClick={() => {
              props.onClose();
            }}
            variant="outlined"
            className={classes.cancelButton}
          >
            Cancel
          </Button>
        </Grid>
      </DialogActions>
    </Dialog>
  );
}
