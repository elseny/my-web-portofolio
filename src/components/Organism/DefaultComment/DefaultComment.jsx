import { useDispatch } from 'react-redux';
import { fetchInfiniteData } from '../../../hooks/swr/fetchData';
import { makeStyles } from '@material-ui/core/styles';
import {
  RETRIEVE_BLOG_COMMENT,
  CREATE_BLOG_COMMENT,
  DELETE_BLOG_COMMENT,
} from '../../../constants/action';
import { RECAPTCHA_ERROR } from '../../../constants/error';
import { onSuccess, onSuccessFinish } from '../../../redux/slice/successSlice';
import { onError, onErrorFinish } from '../../../redux/slice/errorSlice';
import { Grid, IconButton } from '@material-ui/core';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import CommentOwn from '../../Organism/CommentOwn/CommentOwn';
import Comment from '../../Molecules/Comment/Comment';
import ValidationDialog from '../../Organism/ConfirmDialog/ConfirmDialog';
import NewComment from '../../Molecules/NewComment/NewComment';
import { makePostRequest } from '../../../redux/slice/loadingSlice';
import { useState, useCallback, useEffect } from 'react';
import { onBlogCommentChange } from '../../../redux/slice/blogCommentFormSlice';
import { useRouter } from 'next/router';
import { useGoogleReCaptcha } from 'react-google-recaptcha-v3';
import { useSelector } from 'react-redux';

const useStyles = makeStyles(() => ({
  commentContainer: {
    backgroundColor: '#9B9B9B',
    padding: '15px',
    margin: '10px',
    boxShadow: '30px 30px 50px 0px rgba(0,0,0,0.50)',
  },
  showNextCommentIcon: {
    color: 'white',
    width: '100px',
    fontWeight: 'bold',
  },
}));

export default function DefaultComment({ url, blogId, commentId, perPage }) {
  const classes = useStyles();
  const dispatch = useDispatch();
  const [submitted, setSubmitted] = useState(false);
  const [newCommentData, setNewCommentData] = useState([]);
  const [openDeleteDialog, setOpenDeleteDialog] = useState(false);
  const [selectedCommentId, setSelectedCommentId] = useState(-1);
  const [selectedEdit, setSelectedEdit] = useState(null);
  const router = useRouter();
  const auth = useSelector((state) => state.auth);

  const { executeRecaptcha } = useGoogleReCaptcha();

  const handleReCaptchaVerify = useCallback(
    async (action) => {
      if (!executeRecaptcha) {
        return null;
      }
      const token = await executeRecaptcha(action);
      return token;
    },
    [executeRecaptcha]
  );

  const commentRawDataDown = fetchInfiniteData(
    url,
    RETRIEVE_BLOG_COMMENT,
    {
      blog_id: blogId,
      last_page_id: commentId,
      per_page: perPage,
    },
    {
      maxReachedComment: 'No more newer comment!',
    }
  );

  const submitComment = async (formData) => {
    //request submit comment here
    const recaptchaToken = await handleReCaptchaVerify(CREATE_BLOG_COMMENT);
    if (!recaptchaToken) {
      dispatch(onError(RECAPTCHA_ERROR));
      setTimeout(() => dispatch(onErrorFinish(RECAPTCHA_ERROR)), 6000);
      return;
    }

    dispatch(
      makePostRequest({
        url: '/blog-comment/create',
        data: {
          comment: formData.comment,
          blog_id: router.query.id,
          name: auth.token ? undefined : formData.name,
          recaptcha_token: recaptchaToken,
        },
        action: CREATE_BLOG_COMMENT,
      })
    )
      .unwrap()
      .then((res) => {
        // handle result here if (res && res.data && res.data.status === 200) {
        if (res.status === 200) {
          commentRawDataDown.mutate();
          setSubmitted(res.data.data[0]._id);
          if (
            commentRawDataDown.data[commentRawDataDown.data.length - 1].data
              ?.data.length >= perPage
          ) {
            commentRawDataDown.setMaxDataAchieved(false);
            setNewCommentData((state) => {
              return [...state, res.data.data[0]];
            });
          }
          dispatch(onSuccess(res.message));
          setTimeout(() => dispatch(onSuccessFinish(res.message)), 6000);
          dispatch(
            onBlogCommentChange({ blogId: router.query.id, comment: '' })
          );
        } else {
          dispatch(onError(res.message));
          setTimeout(() => dispatch(onErrorFinish(res.message)), 6000);
        }
      })
      .catch((errorRes) => {
        dispatch(onError(errorRes.message));
        setTimeout(() => dispatch(onErrorFinish(errorRes.message)), 6000);
      });
  };

  const deleteComment = async (commentId) => {
    const recaptchaToken = await handleReCaptchaVerify(DELETE_BLOG_COMMENT);
    if (!recaptchaToken) {
      dispatch(onError(RECAPTCHA_ERROR));
      setTimeout(() => dispatch(onErrorFinish(RECAPTCHA_ERROR)), 6000);
      return;
    }

    dispatch(
      makePostRequest({
        url: '/blog-comment/delete',
        data: {
          id: commentId,
          recaptcha_token: recaptchaToken,
        },
        action: DELETE_BLOG_COMMENT,
      })
    )
      .unwrap()
      .then((res) => {
        // handle result here if (res && res.data && res.data.status === 200) {
        if (res.status === 200) {
          setNewCommentData((state) => {
            return [...state].filter((commentData) => {
              return commentData._id !== selectedCommentId;
            });
          });
          commentRawDataDown.mutate();
          setSelectedCommentId(-1);
          setOpenDeleteDialog(false);
          dispatch(onSuccess(res.message));
          setTimeout(() => dispatch(onSuccessFinish(res.message)), 6000);
        } else {
          dispatch(onError(res.message));
          setTimeout(() => dispatch(onErrorFinish(res.message)), 6000);
        }
      })
      .catch((errorRes) => {
        dispatch(onError(errorRes.message));
        setTimeout(() => dispatch(onErrorFinish(errorRes.message)), 6000);
      });
  };

  useEffect(() => {
    if (
      !commentRawDataDown ||
      !commentRawDataDown.data ||
      !commentRawDataDown.data.length
    ) {
      return;
    }
    let newState = [...newCommentData];
    const commentRawIds = commentRawDataDown.data[
      commentRawDataDown.data.length - 1
    ].data?.data.map((commentData) => {
      return commentData._id;
    });

    newState = newCommentData.filter(
      (commentData) => !commentRawIds.includes(commentData._id)
    );
    setNewCommentData(newState);

    if (newState.length > 0) {
      commentRawDataDown.setMaxDataAchieved(false);
    }
  }, [
    commentRawDataDown.data &&
      commentRawDataDown.data[commentRawDataDown.data.length - 1].data?.data,
  ]);

  return (
    <>
      {!commentRawDataDown.data ? (
        <></>
      ) : (
        <Grid
          item
          xs={10}
          container
          direction="column"
          className={classes.commentContainer}
        >
          {commentRawDataDown.data.map((commentData) => {
            return commentData.data.data.map((comment, idx) => {
              if (comment._id === submitted) {
                return (
                  <NewComment
                    key={comment._id}
                    commentData={comment}
                    selectedEdit={selectedEdit}
                    setSelectedEdit={setSelectedEdit}
                    onDeleteClick={(id) => {
                      setSelectedCommentId(id);
                      setOpenDeleteDialog(true);
                    }}
                  />
                );
              }
              return (
                <Comment
                  key={comment._id}
                  commentData={comment}
                  onDeleteClick={(id) => {
                    setSelectedCommentId(id);
                    setOpenDeleteDialog(true);
                  }}
                  selectedEdit={selectedEdit}
                  setSelectedEdit={setSelectedEdit}
                />
              );
            });
          })}
        </Grid>
      )}
      <Grid item xs={10} container justifyContent="center">
        <IconButton
          onClick={() => {
            if (!commentRawDataDown.maxDataAchieved) {
              commentRawDataDown.setSize(commentRawDataDown.size + 1);
            } else {
              dispatch(onError('No more blog!'));
              setTimeout(() => {
                dispatch(onErrorFinish('No more blog!'));
              }, 6000);
            }
          }}
        >
          <KeyboardArrowDownIcon
            className={classes.showNextCommentIcon}
            fontSize="large"
          />
        </IconButton>
      </Grid>
      {newCommentData.length ? (
        <Grid item container xs={7} className={classes.commentContainer}>
          {newCommentData.map((commentData) => {
            return (
              <Comment
                key={`new-comment-${commentData._id}`}
                commentData={commentData}
                onDeleteClick={(id) => {
                  setSelectedCommentId(id);
                  setOpenDeleteDialog(true);
                }}
                selectedEdit={selectedEdit}
                setSelectedEdit={setSelectedEdit}
              />
            );
          })}
        </Grid>
      ) : (
        <></>
      )}
      <Grid item container xs={10}>
        <CommentOwn submitComment={submitComment} />
      </Grid>
      <ValidationDialog
        open={openDeleteDialog}
        onClose={() => setOpenDeleteDialog(false)}
        title="Confirm delete this comment"
        description="delete selected comment?"
        onYes={() => {
          deleteComment(selectedCommentId);
        }}
        onNo={() => setOpenDeleteDialog(false)}
      />
    </>
  );
}
