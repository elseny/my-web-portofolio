import Button from '@material-ui/core/Button';
import Snackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import { useDispatch, useSelector } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import {
  onSuccess,
  onSuccessFinish,
  clearSuccess,
} from '../../../redux/slice/successSlice';

const useStyles = makeStyles((theme) => ({
  message: {
    color: '#2ad20f',
  },
}));

export default function SuccessBar() {
  const dispatch = useDispatch();
  const success = useSelector((state) => state.success);
  const classes = useStyles();

  const handleClose = () => {
    dispatch(clearSuccess());
  };

  return (
    <Snackbar
      anchorOrigin={{
        vertical: 'bottom',
        horizontal: 'right',
      }}
      open={success.isSuccess}
      autoHideDuration={6000}
      onClose={handleClose}
      message={
        <div className={classes.message}>
          {Object.keys(success.message).map((msg, idx) => (
            <div key={`${msg}-${idx}`}>{msg}</div>
          ))}
        </div>
      }
      action={
        <>
          <IconButton
            size="small"
            aria-label="close"
            color="inherit"
            onClick={handleClose}
          >
            <CloseIcon fontSize="small" />
          </IconButton>
        </>
      }
    />
  );
}
