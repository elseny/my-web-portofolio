import Button from '@material-ui/core/Button';
import Snackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import { useDispatch, useSelector } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import {
  onError,
  onErrorFinish,
  clearError,
} from '../../../redux/slice/errorSlice';

const useStyles = makeStyles((theme) => ({
  message: {
    color: '#df0000',
  },
}));

export default function ErrorBar() {
  const dispatch = useDispatch();
  const error = useSelector((state) => state.error);
  const classes = useStyles();
  const handleClose = () => {
    dispatch(clearError());
  };

  return (
    <Snackbar
      anchorOrigin={{
        vertical: 'bottom',
        horizontal: 'left',
      }}
      open={error.isError}
      autoHideDuration={6000}
      onClose={handleClose}
      message={
        <div className={classes.message}>
          {Object.keys(error.message).map((msg, idx) => (
            <div key={`${msg}-${idx}`}>{msg}</div>
          ))}
        </div>
      }
      action={
        <>
          <IconButton
            size="small"
            aria-label="close"
            color="inherit"
            onClick={handleClose}
          >
            <CloseIcon fontSize="small" />
          </IconButton>
        </>
      }
    />
  );
}
