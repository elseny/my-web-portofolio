import { useState, useEffect, useRef, forwardRef } from 'react';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Image from 'next/image';
import { makeStyles } from '@material-ui/core/styles';
import { ProfileImage } from '../../../constants/profile-image';
import classNames from 'classnames';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import { IconButton } from '@material-ui/core';
import { getFormattedDate } from '../../../helpers/dateHelper';
import { useSelector } from 'react-redux';

const useStyles = makeStyles((theme) => ({
  mainContainer: {
    margin: '15px',
  },
  fadeAnimation: {
    animation: '$fadingHightlight 3s',
  },
  '@keyframes fadingHightlight': {
    from: {
      background: 'red',
      opacity: 0,
    },
    to: {
      opacity: 1,
    },
  },
  commentTypo: {
    wordWrap: 'break-word',
  },
  splitLine: {
    border: '1px solid black',
    width: '100%',
    margin: '0px',
  },
  nameTypo: {
    color: 'black',
    fontFamily: 'Rokkitt',
    wordWrap: 'break-word',
    fontSize: '25px',
  },
  adminTypo: {
    color: '#d9e108',
    fontFamily: 'Rokkitt',
    fontSize: '25px',
  },
  dateTypo: {
    color: 'white',
    fontFamily: 'Rokkitt',
  },
}));

const NewComment = (props, ref) => {
  const [userData, setUserData] = useState(props.commentData);
  const classes = useStyles();
  const auth = useSelector((state) => state.auth);
  const gridRef = useRef('');

  useEffect(() => {
    gridRef.current &&
      gridRef.current.scrollIntoView({
        behavior: 'smooth',
        block: 'center',
        inline: 'nearest',
      });
    if (ref) {
      ref.current = true;
    }
  });

  return (
    <>
      <Grid
        item
        container
        className={classNames(classes.mainContainer, classes.fadeAnimation)}
        ref={gridRef}
      >
        <Grid item container justifyContent="center" alignItems="center" xs={3}>
          <Image
            src={
              userData && userData.role === 'admin'
                ? '/images/admin.png'
                : userData.imagepath
                ? ProfileImage[userData.imagepath - 1].imagepath
                : '/images/default-user-image.png'
            }
            alt="Elsen Yacub"
            width={150}
            height={150}
            // layout="responsive"
          />
        </Grid>
        <Grid item container xs={6} md={7}>
          <Grid item container xs={12} justifyContent="space-between">
            <Grid item xs={12}>
              <Typography className={classes.nameTypo}>
                {userData && userData.name}
                {userData && userData.role === 'admin' ? (
                  <span className={classes.adminTypo}>&nbsp;(Admin)</span>
                ) : (
                  <></>
                )}
              </Typography>
            </Grid>
            <Typography className={classes.dateTypo}>
              {getFormattedDate(
                new Date(props.commentData.created_at),
                'dd MM yyyy hh:ii'
              )}
            </Typography>
          </Grid>
          <Grid item container>
            <Grid item xs={12}>
              <Typography className={classes.commentTypo}>
                {props.commentData.comment}
              </Typography>
            </Grid>
          </Grid>
        </Grid>
        {((auth.isLoggedIn && auth.userId === props.commentData.user_id) ||
          auth.role === 'admin') && (
          <Grid item container xs={2} md={1}>
            <Grid item xs={6}>
              <IconButton
                onClick={() => props.onDeleteClick(props.commentData._id)}
              >
                <DeleteIcon />
              </IconButton>
            </Grid>
            <Grid item xs={6}>
              <IconButton
                onClick={() => {
                  props.setSelectedEdit(props.commentData._id);
                }}
              >
                <EditIcon />
              </IconButton>
            </Grid>
          </Grid>
        )}
      </Grid>
      <hr className={classes.splitLine} />
    </>
  );
};

export default forwardRef(NewComment);
