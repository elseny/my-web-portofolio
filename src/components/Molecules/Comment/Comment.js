import { useState, useCallback } from 'react';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Image from 'next/image';
import { makeStyles } from '@material-ui/core/styles';
import { ProfileImage } from '../../../constants/profile-image';
import { getFormattedDate } from '../../../helpers/dateHelper';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import { IconButton, TextareaAutosize } from '@material-ui/core';
import CheckIcon from '@material-ui/icons/Check';
import CloseIcon from '@material-ui/icons/Close';
import { makePostRequest } from '../../../redux/slice/loadingSlice';
import { useDispatch } from 'react-redux';
import { onSuccess, onSuccessFinish } from '../../../redux/slice/successSlice';
import { onError, onErrorFinish } from '../../../redux/slice/errorSlice';
import { EDIT_BLOG_COMMENT } from '../../../constants/action';
import { useSelector } from 'react-redux';
import { RECAPTCHA_ERROR } from '../../../constants/error';
import { useGoogleReCaptcha } from 'react-google-recaptcha-v3';

const useStyles = makeStyles((theme) => ({
  checkIcon: {
    color: 'green',
  },
  closeIcon: {
    color: '#d60e24',
  },
  mainContainer: {
    margin: '15px',
  },
  commentTypo: {
    wordWrap: 'break-word',
  },
  splitLine: {
    border: '1px solid black',
    width: '100%',
    margin: '0px',
  },
  nameTypo: {
    color: 'black',
    fontFamily: 'Rokkitt',
    fontSize: '25px',
    wordWrap: 'break-word',
  },
  adminTypo: {
    color: '#d9e108',
    fontFamily: 'Rokkitt',
    fontSize: '25px',
  },
  dateTypo: {
    color: 'white',
    fontFamily: 'Rokkitt',
  },
}));

export default function Comment(props) {
  const classes = useStyles();
  const dispatch = useDispatch();
  const [comment, setComment] = useState(props.commentData.comment);
  const [editedComment, setEditedComment] = useState(
    props.commentData?.comment
  );
  const auth = useSelector((state) => state.auth);
  const { executeRecaptcha } = useGoogleReCaptcha();

  const handleReCaptchaVerify = useCallback(
    async (action) => {
      if (!executeRecaptcha) {
        return null;
      }
      const token = await executeRecaptcha(action);
      return token;
    },
    [executeRecaptcha]
  );

  const submitEditComment = async (formData) => {
    const recaptchaToken = await handleReCaptchaVerify(EDIT_BLOG_COMMENT);
    if (!recaptchaToken) {
      dispatch(onError(RECAPTCHA_ERROR));
      setTimeout(() => dispatch(onErrorFinish(RECAPTCHA_ERROR)), 6000);
      return;
    }

    dispatch(
      makePostRequest({
        url: '/blog-comment/edit',
        data: {
          comment: formData.comment,
          id: formData.id,
          recaptcha_token: recaptchaToken,
        },
        action: EDIT_BLOG_COMMENT,
      })
    )
      .unwrap()
      .then((res) => {
        // handle result here if (res && res.data && res.data.status === 200) {
        if (res.status === 200) {
          setComment(editedComment);
          props.setSelectedEdit(null);
          dispatch(onSuccess(res.message));
          setTimeout(() => dispatch(onSuccessFinish(res.message)), 6000);
        } else {
          dispatch(onError(res.message));
          setTimeout(() => dispatch(onErrorFinish(res.message)), 6000);
        }
      })
      .catch((errorRes) => {
        dispatch(onError(errorRes.message));
        setTimeout(() => dispatch(onErrorFinish(errorRes.message)), 6000);
      });
  };

  return (
    <>
      <Grid item container className={classes.mainContainer}>
        <Grid item container justifyContent="center" alignItems="center" xs={3}>
          <Image
            src={
              props.commentData.role === 'admin'
                ? '/images/admin.png'
                : props.commentData.imagepath
                ? ProfileImage[props.commentData.imagepath - 1].imagepath
                : '/images/default-user-image.png'
            }
            alt="Elsen Yacub"
            width={150}
            height={150}
            // layout="responsive"
          />
        </Grid>
        <Grid item container xs={6} md={7}>
          <Grid item container xs={12} justifyContent="space-between">
            <Grid item xs={12}>
              <Typography className={classes.nameTypo}>
                {props.commentData && props.commentData.name}
                {props.commentData.role === 'admin' ? (
                  <span className={classes.adminTypo}>&nbsp;(Admin)</span>
                ) : (
                  <></>
                )}
              </Typography>
            </Grid>
            <Typography className={classes.dateTypo}>
              {getFormattedDate(
                new Date(props.commentData.created_at),
                'dd MM yyyy hh:ii'
              )}
            </Typography>
          </Grid>
          <Grid item container alignItems="flex-start">
            {props.selectedEdit === props.commentData._id ? (
              <Grid item container xs={12}>
                <Grid item xs={8}>
                  <TextareaAutosize
                    value={editedComment}
                    onChange={(e) => {
                      setEditedComment(e.target.value);
                    }}
                  />
                </Grid>
                <Grid item container xs={4}>
                  <Grid item xs={6}>
                    <IconButton
                      onClick={() => {
                        submitEditComment({
                          id: props.commentData._id,
                          comment: editedComment,
                        });
                      }}
                    >
                      <CheckIcon className={classes.checkIcon} />
                    </IconButton>
                  </Grid>
                  <Grid item xs={6}>
                    <IconButton
                      onClick={() => {
                        props.setSelectedEdit(null);
                      }}
                    >
                      <CloseIcon className={classes.closeIcon} />
                    </IconButton>
                  </Grid>
                </Grid>
              </Grid>
            ) : (
              <Grid item xs={12}>
                <Typography className={classes.commentTypo}>
                  {comment}
                </Typography>
              </Grid>
            )}
          </Grid>
        </Grid>
        {((auth.isLoggedIn && auth.userId === props.commentData.user_id) ||
          auth.role === 'admin') && (
          <Grid item container xs={2} md={1}>
            <Grid item xs={6}>
              <IconButton
                onClick={() => props.onDeleteClick(props.commentData._id)}
              >
                <DeleteIcon />
              </IconButton>
            </Grid>
            <Grid item xs={6}>
              <IconButton
                onClick={() => {
                  props.setSelectedEdit(props.commentData._id);
                }}
              >
                <EditIcon />
              </IconButton>
            </Grid>
          </Grid>
        )}
      </Grid>
      <hr className={classes.splitLine} />
    </>
  );
}
