import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Image from 'next/image';
import Link from 'next/link';
import DOMPurify from 'isomorphic-dompurify';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '90%',
    borderRadius: '5%',
    boxShadow: '30px 30px 50px 0px rgba(0,0,0,0.50)',
  },
  bodyTypo: {
    textAlign: 'justify',
  },
  cardContent: {
    backgroundColor: '#8f8f8f',
    height: '100px',
    maxHeight: '100px',
  },
}));

export default function CustomCard(props) {
  const classes = useStyles();

  DOMPurify.addHook('afterSanitizeAttributes', function (node) {
    // set all elements owning target to target=_blank
    if ('target' in node) {
      node.setAttribute('target', '_blank');
      node.setAttribute('rel', 'noopener');
    }
  });

  const sanitizedBlogContent = DOMPurify.sanitize(props.blogData.content);

  return (
    <Card className={classes.root}>
      <Link href={`/blog/${props.blogData._id}`}>
        <CardActionArea>
          <Image
            src={props.blogData.imagepath || '/'}
            alt="Image supposed to be here"
            height={100}
            width={200}
            layout="responsive"
          />
          <CardContent className={classes.cardContent}>
            <Grid container>
              <Grid item container justifyContent="center">
                <Typography gutterBottom variant="h5" component="h2">
                  {props.blogData.title}
                </Typography>
              </Grid>
              <Typography
                variant="body2"
                color="textSecondary"
                className={classes.bodyTypo}
              >
                <span
                  dangerouslySetInnerHTML={{
                    __html: sanitizedBlogContent,
                  }}
                />
              </Typography>
            </Grid>
          </CardContent>
        </CardActionArea>
      </Link>
    </Card>
  );
}
