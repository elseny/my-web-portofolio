import { PersistGate } from 'redux-persist/integration/react';
import React from 'react';
export function SSRPersistGate(props) {
  const { children, persistor } = props;

  return (
    <PersistGate loading={<div>{children}</div>} persistor={persistor}>
      <div>{children}</div>
    </PersistGate>
  );
}
