import { makeStyles } from '@material-ui/core/styles';
import { useSelector } from 'react-redux';

const useStyles = makeStyles({
  ldsDualRing: {
    display: 'inline-block',
    width: '80px',
    height: '80px',
    position: 'fixed',
    left: '45%',
    zIndex: 10000,
    '&:after': {
      content: '""',
      display: 'block',
      width: '64px',
      height: '64px',
      margin: '8px',
      borderRadius: '50%',
      border: '6px solid #fff',
      borderColor: '#fff transparent #fff transparent',
    },
    animation: '$ldsDualRing 0.7s linear infinite',
  },
  topDownAnim: {
    animation: '$topDown 0.2s linear',
    animationFillMode: 'forwards',
    position: 'absolute',
  },
  '@keyframes ldsDualRing': {
    '0%': {
      transform: 'rotate(0deg);',
    },
    '100%': {
      transform: 'rotate(360deg);',
    },
  },
  '@keyframes topDown': {
    '0%': {
      top: '0px',
    },
    '100%': {
      top: '100px',
    },
  },
});

export default function Spinner() {
  const classes = useStyles();
  const loading = useSelector((state) => state.loading);
  return loading.isLoading ? (
    <div className={classes.topDownAnim}>
      <div className={classes.ldsDualRing}></div>
    </div>
  ) : (
    <></>
  );
}
