// user
export const EDIT_USER = 'EDIT_USER';
export const EDIT_USER_IMAGE = 'EDIT_USER_IMAGE';
export const RETRIEVE_USER = 'RETRIEVE_USER';
export const USER_LOGIN = 'USER_LOGIN';
export const USER_REGISTER = 'USER_REGISTER';

// blog comment
export const CREATE_BLOG_COMMENT = 'CREATE_BLOG_COMMENT';
export const RETRIEVE_BLOG_COMMENT = 'CREATE_BLOG_COMMENT';
export const DELETE_BLOG_COMMENT = 'DELETE_BLOG_COMMENT';
export const EDIT_BLOG_COMMENT = 'EDIT_BLOG_COMMENT';

// blog
export const CREATE_BLOG = 'CREATE_BLOG';
export const RETRIEVE_BLOG = 'RETRIEVE_BLOG';
export const DELETE_BLOG = 'DELETE_BLOG';
export const EDIT_BLOG = 'EDIT_BLOG';

// project
export const RETRIEVE_PROJECT = 'RETRIEVE_BLOG';
export const CREATE_PROJECT = 'CREATE_PROJECT';
export const DELETE_PROJECT = 'DELETE_PROJECT';
export const EDIT_PROJECT = 'EDIT_PROJECT';

// mail
export const SEND_EMAIL_TO_DEV = 'SEND_EMAIL_TO_DEV';

// check login
export const CHECK_LOGIN = 'CHECK_LOGIN';
