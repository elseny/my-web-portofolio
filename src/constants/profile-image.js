export const ProfileImage = [
  {
    imagepath: '/images/profile-1.png',
    author: 'Darius Dan',
  },
  {
    imagepath: '/images/profile-2.png',
    author: 'Darius Dan',
  },
  {
    imagepath: '/images/profile-3.png',
    author: 'Darius Dan',
  },
  {
    imagepath: '/images/profile-4.png',
    author: 'Darius Dan',
  },
  {
    imagepath: '/images/profile-5.png',
    author: 'Darius Dan',
  },
  {
    imagepath: '/images/profile-6.png',
    author: 'Darius Dan',
  },
  {
    imagepath: '/images/profile-7.png',
    author: 'Darius Dan',
  },
  {
    imagepath: '/images/profile-8.png',
    author: 'Darius Dan',
  },
  {
    imagepath: '/images/profile-9.png',
    author: 'Darius Dan',
  },
];
