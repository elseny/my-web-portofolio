import Button from '@material-ui/core/Button';
import { CheckPermission } from '../../../../src/helpers/permissions';

export default function AuthorizedButton(props) {
  const isAllowed = CheckPermission(props.permissions);
  return (
    isAllowed && (
      <Button variant={props.variant} color="default" onClick={props.onClick}>
        {props.text}
      </Button>
    )
  );
}
