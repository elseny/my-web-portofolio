module.exports = (phase, { defaultConfig }) => {
  return {
    trailingSlash: true,
    env: {},
    images: {
      domains: ['i.imgur.com'],
    },
  };
};
